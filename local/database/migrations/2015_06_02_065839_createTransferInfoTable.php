<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransferInfoTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('transfer_info', function(Blueprint $table)
        {
            $table->increments('id');
            $table->integer('order_id');
            $table->integer('patient_id');
            $table->string('pharmacy_name');
            $table->timestamp('transfer_date');
            $table->string('transfer_by');
            $table->timestamps();
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::drop('transfer_info');
	}

}
