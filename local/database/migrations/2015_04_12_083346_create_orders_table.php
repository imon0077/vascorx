<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrdersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('orders', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('order_name');
			$table->integer('doctor_id');
			$table->integer('patient_id');
			$table->string('prescription_filename');
			$table->string('note_filename');
			$table->string('additional_filename1');
			$table->string('additional_filename2');
			$table->string('additional_filename3');
			$table->string('additional_filename4');
			$table->enum('active_status', array('1', '0'));
			$table->enum('order_completed', array('1', '0'));
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('orders');
	}

}
