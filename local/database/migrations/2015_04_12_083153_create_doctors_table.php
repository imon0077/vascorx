<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDoctorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('doctors', function(Blueprint $table)
		{
			$table->integer('user_id');
			$table->string('first_name');
			$table->string('last_name');
			$table->string('contact_name');
			$table->string('fax_number');
			$table->string('email')->unique();
			$table->string('phone_number');
			$table->integer('npi_number');
			$table->integer('dea_number');
			$table->integer('updated_by');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('doctors');
	}

}
