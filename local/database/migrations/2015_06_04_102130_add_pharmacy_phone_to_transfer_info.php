<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPharmacyPhoneToTransferInfo extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('transfer_info', function(Blueprint $table)
		{
            $table->longText('pharmacy_phone');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('transfer_info', function(Blueprint $table)
		{
            $table->dropColumn('pharmacy_phone');
		});
	}

}
