<?php namespace App\Helpers;


use Auth;
use DB;
use App\Order;
use App\Patient;
class Helper {


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public static function notification()
    {

        if(Auth::user()->hasRole('admin')) {
            $allOrders = Order::whereraw('patient_id != 0 and active_status = 1 and status = 0')
                ->leftjoin('patients', function($join)
                {
                    $join->on('orders.patient_id', '=', 'patients.id');
                })
                ->orderBy('orders.updated_at', 'orders.id', 'DESC')
                ->get();
        }
        else{
            $doctor_id = Auth::user()->getkey();
            $allOrders = DB::table('orders')
                ->join('status', function($join)
                {
                    $join->on('orders.status', '=', 'status.id');
                })
                ->join('patients', function($join)
                {
                    $join->on('orders.patient_id', '=', 'patients.id');
                })
                ->where('orders.patient_id', '!=', 0)
                ->where('orders.doctor_id', $doctor_id)
                ->where('orders.active_status', '=', 1)
                ->where('orders.view_status', '=', 0)
                ->whereRaw('orders.status IN (4, 6, 11, 12)')
                ->select('orders.id', 'orders.order_name', 'orders.created_at', 'orders.updated_at', 'patients.first_name', 'patients.last_name', 'status.status')
                ->orderBy('orders.updated_at', 'orders.id', 'DESC')
                ->get();

        }

        return $allOrders;
    }

} 