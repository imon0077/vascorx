<?php namespace App\Http\Controllers;

use App\Admin;
use App\Doctor;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\User;
use App\Http\Requests\signupDoctorsRequest;
use App\Http\Requests\editprofilesaveRequest;
use App\Http\Requests\editdoctorsaveRequest;

class DoctorsController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $doctor_id = Auth::user()->getkey();
        $doctor_status = User::where('id',$doctor_id)->first();
        if($doctor_status->status == 2 || $doctor_status->status == 0){
            return redirect('profile');
        }else{

            return view('vascorx.pages.dashboard-doctor');
        }
	}

 /** Create Doctors **/
    public function CreateDoctors()
    {
        return view('vascorx.pages.CreateDoctors');
    }

    public function CreateDoctorsSave(signupDoctorsRequest $request)
    {
        $first_name = $request->get('first_name');
        $last_name = $request->get('last_name');
        $email = $request->get('email');
        $phn_number = $request->get('phn_number');
        $user_name = $request->get('user_name');
        $password = $request->get('password');
        $npi_number = $request->get('npi_number');
        $dea_number = $request->get('dea_number');
        $fax_number = $request->get('fax_number');
        $contact_name = $request->get('contact_name');
        $role= "doctor";
        $passwordhash = bcrypt($password);
        $status = 1;
        if ($user_name and $password and $email)
        {
            $this->savedoctors($first_name, $last_name, $user_name, $passwordhash, $status, $email, $role, $phn_number,$npi_number,$dea_number,$fax_number,$contact_name);
        }
        flash()->success('Created Doctor Successfully !!');

        return redirect('CreateDoctors');
    }

    private function savedoctors($first_name, $last_name, $user_name, $passwordhash, $status, $email, $role, $phn_number,$npi_number,$dea_number,$fax_number,$contact_name)
    {
        $user = new User();
        $id = $user::insertGetId(array('username'=>$user_name, 'password'=>$passwordhash, 'status'=>$status, 'user_role'=>$role));

        $doctor = new Doctor();
        $doctor->user_id = $id;
        $doctor->first_name = $first_name;
        $doctor->last_name = $last_name;
        $doctor->phone_number = $phn_number;
        $doctor->email = $email;
        $doctor->npi_number = $npi_number;
        $doctor->dea_number = $dea_number;
        $doctor->fax_number = $fax_number;
        $doctor->contact_name = $contact_name;
        $doctor->save();
    }

 /** /Create Doctors **/

    /**show profile**/
    public function profile()
    {
        if(Auth::user()->hasRole('admin')) {

            $doctor_id = Auth::user()->getkey();

            $profile = DB::table('users')
                ->join('admins', function($join)
                {
                    $join->on('users.id', '=', 'admins.user_id');
                })
                ->where('users.status', '=', 1)
                ->where('users.id', '=', $doctor_id)
                ->first();
        }
        else{
            $doctor_id = Auth::user()->getkey();

            $profile = DB::table('users')
                ->join('doctors', function($join)
                {
                    $join->on('users.id', '=', 'doctors.user_id');
                })
                ->where('users.id', '=', $doctor_id)
                ->first();
            //print_r($profile); die;
        }

        return view('vascorx.pages.profile', compact('profile'));
    }

    /** /show profile **/

    /** edit profile **/
    public function editprofile($user_id){
        if(Auth::user()->hasRole('admin')) {
            $user = new Admin();
            $profiledetails = $user->where('user_id', $user_id)->first();
        }
        else{
            $user = new Doctor();
            $profiledetails = $user->where('user_id', $user_id)->first();
        }
        return view('vascorx.pages.editprofile', compact('profiledetails'));
    }

    public function editprofilesave(editprofilesaveRequest $request)
    {
        if(Auth::user()->hasRole('admin')) {
            $user_id = $request->get('id');
            $first_name = $request->get('first_name');
            $last_name = $request->get('last_name');
            $pharmacy_name = $request->get('pharmacy_name');
            $phone_number = $request->get('phn_number');
            $email = $request->get('email');

            Admin::where('user_id', $user_id)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "pharmacy_name" => $pharmacy_name,
                        "phone_number" => $phone_number,
                        "email" => $email
                    )
                );
        }
        else{
            $user_id = $request->get('id');
            $first_name = $request->get('first_name');
            $last_name = $request->get('last_name');
            $contact_name = $request->get('contact_name');
            $phone_number = $request->get('phn_number');
            $email = $request->get('email');
            $fax_number = $request->get('fax_number');
            $npi_number = $request->get('npi_number');
            $dea_number = $request->get('dea_number');

            Doctor::where('user_id', $user_id)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "contact_name" => $contact_name,
                        "phone_number" => $phone_number,
                        "email" => $email,
                        "fax_number" => $fax_number,
                        "npi_number" => $npi_number,
                        "dea_number" => $dea_number,
                    )
                );
        }

        flash()->success('Profile updated successfully !!');

        return redirect('profile');
    }

    /** /edit profile **/


    /** edit profile **/
    public function editdoctor($user_id){
            $user = new Doctor();
            $profiledetails = $user->where('user_id', $user_id)->first();
        return view('vascorx.pages.editdoctor', compact('profiledetails'));
    }

    public function editdoctorsave(editdoctorsaveRequest $request)
    {
            $user_id = $request->get('id');
            $first_name = $request->get('first_name');
            $last_name = $request->get('last_name');
            $contact_name = $request->get('contact_name');
            $phone_number = $request->get('phn_number');
            $email = $request->get('email');
            $fax_number = $request->get('fax_number');
            $npi_number = $request->get('npi_number');
            $dea_number = $request->get('dea_number');

            Doctor::where('user_id', $user_id)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "contact_name" => $contact_name,
                        "phone_number" => $phone_number,
                        "email" => $email,
                        "fax_number" => $fax_number,
                        "npi_number" => $npi_number,
                        "dea_number" => $dea_number,
                    )
                );


        flash()->success('Doctors updated successfully !!');

        return redirect('Doctors');
    }

    /** /edit doctor **/

    /** Delete a doctor **/
    public function deletedoctor($doctorID){
        DB::table('doctors')
            ->where('user_id', $doctorID)
            ->delete();

        DB::table('users')
            ->where('id', $doctorID)
            ->delete();

        flash()->success('One Doctor has been deleted !!');
        return redirect('Doctors');
    }
    /** /Delete a doctor **/

    /**show contact details**/
    public function contact()
    {
        $doctor_id = Auth::user()->getkey();
        $doctorstatus = User::where('id',$doctor_id)->first();
        return view('vascorx.pages.contact', compact('doctorstatus'));
    }

    /** /show contact details **/

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
