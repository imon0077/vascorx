<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use Auth;
use Illuminate\Support\Facades\DB;
use App\Order;
use App\Patient;
use App\Admin;
use App\Role;
use App\Status;
use App\TransferInfo;
use App\Doctor;
use App\User;

class AdminsController extends Controller {


    public function __construct()
    {
        $this->middleware('auth');
    }


    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index()
    {

        if(Auth::user()->hasRole('admin')) {

            //$allOrders = Order::whereraw('patient_id != 0 and active_status = 1 and status = 0')->get();
            $allOrders = DB::table('orders')
                ->leftjoin('status', function($join)
                {
                    $join->on('orders.status', '=', 'status.id');
                })
                ->leftjoin('patients', function($join)
                {
                    $join->on('orders.patient_id', '=', 'patients.id');
                })

                ->join('doctors', function($join)
                {
                    $join->on('orders.doctor_id', '=', 'doctors.user_id');
                })

                ->where('orders.patient_id', '!=', 0)
                ->where('orders.active_status', '=', 1)
                ->where('orders.status', '=', 0)
                ->select('orders.id', 'orders.order_name','orders.memo', 'orders.created_at', 'orders.updated_at',
                    'patients.first_name', 'patients.last_name', 'doctors.first_name AS d_fname', 'doctors.last_name AS d_lname','orders.patient_id', 'patients.date_of_birth', 'status.status')
                ->orderBy('orders.updated_at', 'orders.id', 'DESC')
                ->get();
            $noOfNewOrders = count($allOrders);

            //$statusLists = Status::lists('status', 'id');
            $statusLists = DB::table('status')
                        ->whereNotIn('id', [4])
                        ->get();

            return view('vascorx.pages.dashboard-admin', compact('allOrders','statusLists','noOfNewOrders'));
        }
        else{
            //$allOrders = Order::whereraw('patient_id != 0 and active_status = 1 and status in (2, 4, 6, 7, 11)')->get();
            $doctor_id = Auth::user()->getkey();
            $doctor_status = User::where('id',$doctor_id)->first();
            if($doctor_status->status == 2 || $doctor_status->status == 0){
                return redirect('profile');
            }else{
                $allOrders = DB::table('orders')
                    ->join('status', function($join)
                    {
                        $join->on('orders.status', '=', 'status.id');
                    })
                    ->join('patients', function($join)
                    {
                        $join->on('orders.patient_id', '=', 'patients.id');
                    })
                    ->where('orders.patient_id', '!=', 0)
                    ->where('orders.doctor_id', $doctor_id)
                    ->where('orders.active_status', '=', 1)
                    ->where('orders.view_status', '=', 0)
                    ->whereRaw('orders.status IN (4, 6, 11,12)')
                    ->select('orders.id', 'orders.order_name', 'orders.created_at', 'orders.updated_at', 'patients.first_name', 'patients.last_name', 'status.status')
                    ->orderBy('orders.updated_at', 'orders.id', 'DESC')
                    ->get();

                $noOfNewOrders = count($allOrders);

                $orderLists = Order::all()
                    ->where('active_status', '1')
                    ->where('doctor_id', $doctor_id);

                $patientLists = Patient::all()->where('doctor_id', $doctor_id);

                return view('vascorx.pages.dashboard-doctor', compact('allOrders', 'noOfNewOrders', 'orderLists', 'patientLists'));
            }
        }

    }

    public function notification()
    {
        if(Auth::user()->hasRole('admin')) {
            $notifications = Order::whereraw('patient_id != 0 and active_status = 1 and status = 0')
                ->leftjoin('patients', function($join)
                {
                    $join->on('orders.patient_id', '=', 'patients.id');
                })
                ->orderBy('orders.updated_at', 'orders.id', 'DESC')
                ->get();
        }
        else{
            $doctor_id = Auth::user()->getkey();
            $notifications = DB::table('orders')
                ->join('status', function($join)
                {
                    $join->on('orders.status', '=', 'status.id');
                })
                ->join('patients', function($join)
                {
                    $join->on('orders.patient_id', '=', 'patients.id');
                })
                ->where('orders.patient_id', '!=', 0)
                ->where('orders.doctor_id', $doctor_id)
                ->where('orders.active_status', '=', 1)
                ->where('orders.view_status', '=', 0)
                ->whereRaw('orders.status IN (4, 6, 11, 12)')
                ->select('orders.id', 'orders.order_name', 'orders.created_at', 'orders.updated_at', 'patients.first_name', 'patients.last_name', 'status.status')
                ->orderBy('orders.updated_at', 'orders.id', 'DESC')
                ->get();


        }

        return $notifications;

    }

    /**show patients**/
    public function patientlist()
    {
        $doctor_id = Auth::user()->getkey();
        $patientLists = Patient::all()->where('doctor_id', $doctor_id);

        return view('vascorx.pages.patientlist', compact('patientLists'));
    }
    /** /show patients **/

    /**show registered doctors**/
    public function newDoctors()
    {
        if(Auth::user()->hasRole('admin')) {
            //$newDoctors = User::where('status','>=', 0)->where('user_role', 'doctor')->get();
            $newDoctors = DB::table('users')
                ->join('doctors', function($join)
                {
                    $join->on('users.id', '=', 'doctors.user_id');
                })
                ->where('users.status', '>=', 0)->where('users.user_role', 'doctor')->get();



            return view('vascorx.pages.newDoctors', compact('newDoctors'));

        }

    }
    /** /show registered doctors **/

    /**show doctors list **/
    public function Doctors()
    {
        if(Auth::user()->hasRole('admin')) {
            //$newDoctors = User::where('status','>=', 0)->where('user_role', 'doctor')->get();
            $Doctors = DB::table('users')
                ->join('doctors', function($join)
                {
                    $join->on('users.id', '=', 'doctors.user_id');
                })
                ->where('users.status', '>=', 0)->where('users.user_role', 'doctor')->get();

            return view('vascorx.pages.Doctors', compact('Doctors'));

        }

    }
    /** /show doctors list **/

    /** Decline a doctors **/
    public function decline($doctorID){
        DB::table('users')
            ->where('id', $doctorID)
            ->update(['status' => 2]);
        flash()->success('Doctors request has been declined !!');
        return redirect('newDoctors');
    }
    /** /Decline a doctors **/

    /** Decline a doctors **/
    public function accept($doctorID){
        DB::table('users')
            ->where('id', $doctorID)
            ->update(['status' => 1]);
        flash()->success('Doctors request has been accepted !!');
        return redirect('newDoctors');
    }
    /** /Decline a doctors **/

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update(Request $id)
    {
        //
        $order_id = $id->get('order_id');
        $status = $id->get('status');
        $memo = $id->get('memo');

        $affectedRows = Order::where('id', '=', $order_id)->update(['status' => $status,'memo' => $memo]);


        //$this->sendCustomMail($order_id,$doctor_email,$body);
        return redirect('dashboard');
    }

    public function orderTransfer(Request $id)
    {
        $doctor_id = Auth::user()->getkey();
        $order_id = $id->get('t_order_id');
        $status = 11;

        $affectedRows = Order::where('id', '=', $order_id)->update(['status' => $status]);
        if($affectedRows){
            $transfer_order = new TransferInfo();
            $transfer_order->order_id = $order_id;
            $transfer_order->patient_id = $id->get('t_patient_id');
            $transfer_order->pharmacy_name = $id->get('t_pharmacy_name');
            $transfer_order->pharmacy_phone = $id->get('t_phone');
            $transfer_order->transfer_date = $id->get('t_date');
            $transfer_order->transfer_by= $doctor_id;
            $transfer_order->note= $id->get('t_note');

            $transfer_order->save();

            //$this->sendCustomMail($order_id,$doctor_email,$body);
            return redirect('dashboard');
        }
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
    public function sendCustomMail($order_id,$doctor_email,$body)
    {
        $headers = "From: VascoRX <" . strip_tags("info@vascorx.com") . ">\r\n";
        $headers .= "Reply-To: VascoRX <" . strip_tags("info@vascorx.com") . ">\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";


        $email_temp = '<html>
            <head>
            <title>VascoRX</title>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
            </head>
            <body bgcolor="#d4d2d2" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
            <table id="Table_01" width="600" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">
              <tr>
                <td colspan="3" style="border-bottom:3px solid #730606;"><img src="'.url().'/public/assets/images/logo.png" width="300" height="47" alt=""></td>
              </tr>
              <tr>

              <td width="23" height="2" bgcolor="#ffffff"></td>
              <td rowspan="2" width="540" height="100%" bgcolor="#ffffff" valign="top">

              <table cellpadding="0" cellspacing="0">
                <tr>
                  <td style="padding-top:30px; padding-bottom:30px;" style="font-family:Arial, sans-serif;">
                  Dear User,<br>
                  <br>

                 '.$body.'

                  <br>
                  <br>
                  <strong>Regards</strong><br>
                  VascoRX Team<br>
                </td>

                </tr>

              </table>
              </td>

              <td width="37" height="2" bgcolor="#ffffff"></td>
              </tr>
              <tr>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td colspan="3" width="600" height="175" bgcolor="#730606" align="center" style="color:#fff;font-family:Arial, sans-serif"><strong>Contact Information</strong><br>
                  Phoenix, Arizona<br>
                  <a href="mailto:info@vascorx.com" style="color:#ffffff;">info@vascorx.com</a><br></td>
              </tr>
            </table>
            </body>
            </html>';

        $subDoctor = "VascoRX: Order submitted: ".$order_id;
        $subAdmin = "VascoRx: New Order: ".$order_id;


        /*This is email Body Content for sending an email after user registration End*/

        mail($doctor_email, $subDoctor, $email_temp, $headers);

        /*For All Admin Email Send*/
        $allAdminRoleID = User::where('user_role','admin')->get();
        foreach($allAdminRoleID as $adminID){
            $adminEmail = Admin::where('user_id',$adminID->id)->first();

            mail($adminEmail->email, $subAdmin, $email_temp, $headers);
        }

    }

}
