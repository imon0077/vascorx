<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\User;
use App\Doctor;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\signupDoctorsRequest;
use Illuminate\Support\Facades\Storage;

use Carbon\Carbon;

class SignupController extends Controller {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{

		return view('vascorx.pages.signup');
	}

    /** Signup-Doctors **/
    public function signupdoctors(signupDoctorsRequest $request)
    {

        $first_name = $request->get('first_name');
        $last_name = $request->get('last_name');
        $email = $request->get('email');
        $phn_number = $request->get('phn_number');
        $user_name = $request->get('user_name');
        $password = $request->get('password');
        $role= "doctor";
        $passwordhash = bcrypt($password);

        if ($user_name and $password and $email)
        {
            $this->savedoctors($first_name, $last_name, $user_name, $passwordhash, $email, $role, $phn_number);
        }
        flash()->success('Registered successfully, wait for admin approval !!');

        return redirect('signup');
    }

    private function savedoctors($first_name, $last_name, $user_name, $passwordhash, $email, $role, $phn_number)
    {
        $user = new User();
        $id = $user::insertGetId(array('username'=>$user_name, 'password'=>$passwordhash, 'user_role'=>$role));

        $doctor = new Doctor();
        $doctor->user_id = $id;
        $doctor->first_name = $first_name;
        $doctor->last_name = $last_name;
        $doctor->phone_number = $phn_number;
        $doctor->email = $email;
        $doctor->save();
    }

    /** /Signup-Doctors **/

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		//
	}

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

}
