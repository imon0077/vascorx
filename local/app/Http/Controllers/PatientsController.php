<?php namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use Auth;
use App\Http\Requests\CreatePatientRequest;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Validator;
use App\Patient;
use App\Order;
use Carbon\Carbon;

use App\Http\Controllers\Route;

class PatientsController extends Controller {


	public function __construct()
	{
		$this->middleware('auth');
	}


	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
        $doctor_id = Auth::user()->getkey();
		$orderLists = Order::where('active_status', '1')
            ->where('doctor_id', $doctor_id)->get();

		return view('vascorx.pages.create-patient', compact('orderLists'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(CreatePatientRequest $request)
	{
		
		$prescription_file = $request->file('prescription_filename');
		$note_file = $request->file('note_filename');

		$random_patient = 'PATIENT.'.Carbon::now()->format('dmY').'_'.mt_rand(0, 999);

		if ($prescription_file or $note_file)
		{
            $validator = Validator::make($request->all(), [
                'prescription_filename' => 'required',
                'note_filename' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('create-patient')
                    ->withErrors($validator)
                    ->withInput();
            }
			$this->savePatientAndOrder($request, $prescription_file, $note_file, $random_patient);
		}

		else{
			
			$this->saveOnlyPatient($request, $random_patient);
		}

		flash()->success('Patient has been created successfully !!');

		return redirect('patientlist');
	}



	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	private function savePatientAndOrder($request, $prescription_file, $note_file, $random_patient)
	{
		$doctor_id = Auth::user()->getkey();
		$patient = new Patient();

		$patient->patient_name = $random_patient;
		$patient->first_name = ucwords($request->get('first_name'));
		$patient->last_name = ucwords($request->get('last_name'));
		//$patient->date_of_birth = $request->get('date_of_birth');
        $dd = $request->get('dd');
        $mm = $request->get('mm');
        $yyyy = $request->get('yyyy');
        $date_of_birth = strtotime($dd.'-'.$mm.'-'.$yyyy);
        $patient->date_of_birth = $date_of_birth;
		$patient->doctor_id = $doctor_id;

		$patient->save();

		$patient_id = $patient->id;

        $p_extension = $prescription_file->getClientOriginalExtension();
        $extension = $note_file->getClientOriginalExtension();

		$order = new Order();

        $path_to_move = storage_path().'/app/presc_note/';
        $short_directory = 'app/presc_note/';

        $random_order = 'ORDER.'.Carbon::now()->format('dmY').'.'.mt_rand(0, 999);

        $prescription_file->move($path_to_move, 'PRESC.'.$random_order.'.'.$p_extension);
        $note_file->move($path_to_move, 'NOTE1.'.$random_order.'.'.$extension);

        $order->prescription_filename = $short_directory.'PRESC.'.$random_order.'.'.$p_extension;
        $order->note_filename = $short_directory.'NOTE1.'.$random_order.'.'.$extension;

		$order->doctor_id = $doctor_id;
		$order->patient_id = $patient_id;
		$order->order_name = $random_order;
 
		$order->save();
	}

	private function saveOnlyPatient($request, $random_patient)
	{
		$doctor_id = Auth::user()->getkey();;
		$order_id = $request->get('order_id');
		$patient = new Patient();

		$patient->patient_name = $random_patient;
		$patient->first_name = ucwords($request->get('first_name'));
		$patient->last_name = ucwords($request->get('last_name'));
        $dd = $request->get('dd');
        $mm = $request->get('mm');
        $yyyy = $request->get('yyyy');

		$patient->date_of_birth = strtotime($dd.'-'.$mm.'-'.$yyyy);

		$patient->doctor_id = $doctor_id;

		$patient->save();

		$patient_id = $patient->id;

		$order = new Order; 
		$order->where('id', '=', $order_id)->update(array('patient_id' => $patient_id));

	}

    /** edit a patient **/
    public function edit_patient($patientID){
      $patientss = new Patient();
        $patients = $patientss->where('id', $patientID)->first();

        return view('vascorx.pages.edit-patient', compact('patients'));
    }
    /** /edit a patient **/

    /** del a patient **/
    public function del_patient($patientID){
//        DB::table('patients')
//            ->where('id', $patientID)
//            ->delete();
        Patient::where('id', '=', $patientID)->delete();
        Order::where('patient_id', '=', $patientID)->delete();
        flash()->success('Patient has been deleted successfully !!');
        return redirect('patientlist');
    }
    /** /del a patient **/


    /** edit patient save  **/
    public function editSave(CreatePatientRequest $request)
    {
        $PatientID = $request->get('id');
        $first_name = ucwords($request->get('first_name'));
        $last_name = ucwords($request->get('last_name'));
        $dd = $request->get('dd');
        $mm = $request->get('mm');
        $yyyy = $request->get('yyyy');
        $date_of_birth = strtotime($dd.'-'.$mm.'-'.$yyyy);

        Patient::where('id', $PatientID)
            ->update(
                array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                )
            );

        flash()->success('Patient updated successfully !!');

        return redirect('patientlist');
    }
    /** /edit patient save  **/



}
