<?php namespace App\Http\Controllers;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
//use Illuminate\Support\Facades\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Requests\CreateOrderRequest;
use Validator;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

use App\Order;
use App\User;
use App\Patient;
use App\Doctor;
use App\Admin;
use Carbon\Carbon;

use App\Status;
use App\Http\Controllers\Route;


class OrdersController extends Controller {

	public function __construct()
	{
		$this->middleware('auth');
	}

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$doctor_id = Auth::user()->getkey();

		$patientLists = Patient::all()->where('doctor_id', $doctor_id);

		return view('vascorx.pages.create-order', compact('patientLists'));
	}

	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		//
	}

	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store(Request $request)
	{
		$doctor_id = Auth::user()->getkey();

		$first_name = $request->get('first_name');
		$last_name = $request->get('last_name');
        $dd = $request->get('dd');
        $mm = $request->get('mm');
        $yyyy = $request->get('yyyy');

		$date_of_birth = strtotime($dd.'-'.$mm.'-'.$yyyy);

		if ($first_name or $last_name or $date_of_birth)
		{
            $validator = Validator::make($request->all(), [
                'first_name' => 'required',
                'last_name' => 'required',
                'dd' => 'required',
                'mm' => 'required',
                'yyyy' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('create-order')
                    ->withErrors($validator)
                    ->withInput();
            }
			$this->saveOrderAndPatient($request,$first_name,$last_name,$date_of_birth,$doctor_id);
		}

		else{
            $validator = Validator::make($request->all(), [
                'patient_id' => 'required',
            ]);

            if ($validator->fails()) {
                return redirect('create-order')
                    ->withErrors($validator)
                    ->withInput();
            }
			$this->saveOnlyOrder($request,$doctor_id);
		}

		flash()->success('Order has been created successfully !!');

		return redirect('orderlist');

	}
    public function OrderUpdate(Request $request)
	{
		$doctor_id = Auth::user()->getkey();

		$order_id = $request->get('order_id');
		$first_name = $request->get('first_name');
		$last_name = $request->get('last_name');
        $dd = $request->get('dd');
        $mm = $request->get('mm');
        $yyyy = $request->get('yyyy');

		//$date_of_birth = mktime(0, 0, 0, $mm, $dd, $yyyy);
		$date_of_birth = strtotime($mm.'-'.$dd.'-'.$yyyy);
//echo $first_name.'-'.$last_name.'-'.$dd.'-'.$mm.'-'.$yyyy; die;
       $patient_id = $request->get('patient_id');
		if ($first_name and $last_name and $date_of_birth)
		{
			$this->updateOrderAndPatient($order_id,$first_name,$last_name,$date_of_birth,$doctor_id);
		}

		else{

			$this->updateOnlyOrder($patient_id,$order_id);
		}

		flash()->success('Order has been created successfully !!');

		return redirect('create-order');

	}
public function OrderUpdateView($orderID)
	{
        $doctor_id = Auth::user()->getkey();
        $orderLists = Order::where('id', $orderID)->first();
        $patientLists = Patient::all()->where('doctor_id', $doctor_id);
        return view('vascorx.pages.update-order', compact('orderID','patientLists','orderLists'));

	}

    /**show Orders**/
    public function OrderLists()
    {
        if(Auth::user()->hasRole('admin')) {
            $allOrders = DB::table('orders')
                ->leftjoin('status', function($join)
                {
                    $join->on('orders.status', '=', 'status.id');
                })
                ->leftjoin('patients', function($join)
                {
                    $join->on('orders.patient_id', '=', 'patients.id');
                })
                ->leftjoin('doctors', function($join)
                {
                    $join->on('orders.doctor_id', '=', 'doctors.user_id');
                })

                ->where('orders.active_status', '=', 1)
                ->where('orders.patient_id', '>', 0)
                ->select('orders.id','orders.patient_id', 'orders.order_name', 'orders.created_at', 'orders.updated_at', 'patients.first_name', 'patients.last_name', 'patients.date_of_birth', 'doctors.first_name AS dfname', 'doctors.last_name AS dlname', 'status.status')
                ->orderBy('orders.updated_at', 'orders.id', 'DESC')
                ->get();
            //$noOfNewOrders = count($allOrders);
            //$orderLists = Order::all()->where('active_status', '1')
                //->orderBy('orders.id', 'DESC');
            $statusLists = Status::lists('status', 'id');
        }
        else{
            $doctor_id = Auth::user()->getkey();
            $allOrders = DB::table('orders')
                ->leftjoin('status', function($join)
                {
                    $join->on('orders.status', '=', 'status.id');
                })
                ->leftjoin('patients', function($join)
                {
                    $join->on('orders.patient_id', '=', 'patients.id');
                })
                ->leftjoin('doctors', function($join)
                {
                    $join->on('orders.doctor_id', '=', 'doctors.user_id');
                })

                ->where('orders.active_status', '=', 1)
                ->where('orders.doctor_id', $doctor_id)
                ->select('orders.id','orders.patient_id', 'orders.order_name', 'orders.created_at', 'orders.updated_at', 'patients.first_name', 'patients.last_name', 'patients.date_of_birth', 'doctors.first_name AS dfname', 'doctors.last_name AS dlname', 'status.status')
                ->orderBy('orders.updated_at', 'orders.id', 'DESC')
                ->get();
            //$noOfNewOrders = count($allOrders);
            //$orderLists = Order::all()->where('active_status', '1');
        }

        return view('vascorx.pages.orderlist', compact('allOrders', 'noOfNewOrders', 'orderLists','statusLists'));
    }

    /** /show orders **/

 /**show Order Details**/
    public function OrderDetails($orderID)
    {
        $doctor_id = Auth::user()->getkey();
        $orderDetails = DB::table('orders')
            ->leftjoin('status', function($join)
            {
                $join->on('orders.status', '=', 'status.id');
            })
            ->leftjoin('patients', function($join)
            {
                $join->on('orders.patient_id', '=', 'patients.id');
            })

            ->leftjoin('doctors', function($join)
            {
                $join->on('orders.doctor_id', '=', 'doctors.user_id');
            })

            ->leftjoin('transfer_info', function($join)
            {
                $join->on('orders.id', '=', 'transfer_info.order_id');
            })

            ->where('orders.active_status', '=', 1)
            ->where('orders.id', $orderID)
            ->select('orders.id','orders.patient_id', 'orders.status AS sid', 'orders.order_name', 'orders.notes', 'orders.memo','prescription_filename','note_filename','additional_filename1','additional_filename2','additional_filename3', 'orders.created_at', 'orders.updated_at', 'patients.first_name', 'patients.last_name', 'patients.date_of_birth', 'doctors.first_name AS d_fname', 'doctors.last_name AS d_lname', 'status.status', 'transfer_info.pharmacy_name', 'transfer_info.pharmacy_phone', 'transfer_info.note', 'transfer_info.transfer_date')
            ->get();
        $statusLists = Status::all();
        return view('vascorx.pages.orderDetails', compact('orderDetails','statusLists'));
    }
    public function changeStatusFromAdmin(Request $getvalue){
        $order_id = $getvalue->get('order_id');
        $status_id = $getvalue->get('status_change');

        $order = Order::find($order_id);
        $order->status = $status_id;
        $order->view_status = 0;

        $order->save();
        return redirect('orderlist');
    }

    public function changeMemoFromAdmin(Request $getvalue){
        $order_id = $getvalue->get('order_id');
        $status_id = $getvalue->get('memo');

        $order = Order::find($order_id);
        $order->memo = $status_id;

        $order->save();
        return redirect('orderlist');
    }


    public function NotesUpdate(Request $getvalue){
        $order_id = $getvalue->get('order_id');
        $status_id = $getvalue->get('notes');

        $order = Order::find($order_id);
        $order->notes = $status_id;

        $order->save();
        return redirect('orderDetails/'.$order_id);
    }


    /** /End show order Details **/

	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
		//
	}

	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}

	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}

	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}

	private function saveOrderAndPatient($request,$first_name,$last_name,$date_of_birth,$doctor_id)
	{
		
		$random_patient = 'patient_'.Carbon::now()->format('d-m-Y').'_'.mt_rand(0, 999);
		
		$patient = new Patient();

		$patient->patient_name = $random_patient;
		$patient->first_name = $first_name;
		$patient->last_name = $last_name;
		$patient->date_of_birth = $date_of_birth;
		$patient->doctor_id = $doctor_id;

		$patient->save();

		$patient_id = $patient->id;

		$prescription_file = $request->file('prescription_filename');
		$note_file = $request->file('note_filename');

        if($prescription_file AND $note_file){
            $p_extension = $prescription_file->getClientOriginalExtension();

            $extension = $note_file->getClientOriginalExtension();

            $order = new Order();

            $path_to_move = storage_path().'/app/presc_note/';
            $short_directory = 'app/presc_note/';

            $random_order = 'ORDER.'.Carbon::now()->format('dmY').'.'.mt_rand(0, 999);

            $prescription_file->move($path_to_move, 'PRESC.'.$random_order.'.'.$p_extension);
            $note_file->move($path_to_move, 'NOTE1.'.$random_order.'.'.$extension);

            $order->prescription_filename = $short_directory.'PRESC.'.$random_order.'.'.$p_extension;
            $order->note_filename = $short_directory.'NOTE1.'.$random_order.'.'.$extension;
            $order->doctor_id = $doctor_id;
            $order->patient_id = $patient_id;
            $order->order_name = $random_order;

            $order->save();

            $order_id = $order->id;
            $this->sendCustomMail($order_id,$random_order,$patient_id,$doctor_id);
        }
        elseif($prescription_file){
            $p_extension = $prescription_file->getClientOriginalExtension();
            $order = new Order();

            $path_to_move = storage_path().'/app/presc_note/';
            $short_directory = 'app/presc_note/';

            $random_order = 'ORDER.'.Carbon::now()->format('dmY').'.'.mt_rand(0, 999);

            $prescription_file->move($path_to_move, 'PRESC.'.$random_order.'.'.$p_extension);

            $order->prescription_filename = $short_directory.'PRESC.'.$random_order.'.'.$p_extension;
            $order->doctor_id = $doctor_id;
            $order->patient_id = $patient_id;
            $order->order_name = $random_order;

            $order->save();

            $order_id = $order->id;
            $this->sendCustomMail($order_id,$random_order,$patient_id,$doctor_id);
        }
        elseif($note_file){
            $extension = $note_file->getClientOriginalExtension();

            $order = new Order();

            $path_to_move = storage_path().'/app/presc_note/';
            $short_directory = 'app/presc_note/';

            $random_order = 'ORDER.'.Carbon::now()->format('dmY').'.'.mt_rand(0, 999);

            $note_file->move($path_to_move, 'NOTE1.'.$random_order.'.'.$extension);

            $order->note_filename = $short_directory.'NOTE1.'.$random_order.'.'.$extension;
            $order->doctor_id = $doctor_id;
            $order->patient_id = $patient_id;
            $order->order_name = $random_order;

            $order->save();

            $order_id = $order->id;
            $this->sendCustomMail($order_id,$random_order,$patient_id,$doctor_id);
        }

    }
private function updateOrderAndPatient($order_id,$first_name,$last_name,$date_of_birth,$doctor_id)
	{

		$random_patient = 'patient_'.Carbon::now()->format('d-m-Y').'_'.mt_rand(0, 999);

		$patient = new Patient();

		$patient->patient_name = $random_patient;
		$patient->first_name = $first_name;
		$patient->last_name = $last_name;
		$patient->date_of_birth = $date_of_birth;
		$patient->doctor_id = $doctor_id;

		$patient->save();

		$patient_id = $patient->id;

		$order = Order::find($order_id);
        $order->patient_id = $patient_id;

		$order->save();
	}

	private function saveOnlyOrder($request, $doctor_id)
	{

		$prescription_file = $request->file('prescription_filename');
		$note_file = $request->file('note_filename');
		$patient_id = $request->get('patient_id');


        if($prescription_file AND $note_file){
            $p_extension = $prescription_file->getClientOriginalExtension();

            $extension = $note_file->getClientOriginalExtension();

            $order = new Order();

            $path_to_move = storage_path().'/app/presc_note/';
            $short_directory = 'app/presc_note/';

            $random_order = 'ORDER.'.Carbon::now()->format('dmY').'.'.mt_rand(0, 999);

            $prescription_file->move($path_to_move, 'PRESC.'.$random_order.'.'.$p_extension);
            $note_file->move($path_to_move, 'NOTE1.'.$random_order.'.'.$extension);

            $order->prescription_filename = $short_directory.'PRESC.'.$random_order.'.'.$p_extension;
            $order->note_filename = $short_directory.'NOTE1.'.$random_order.'.'.$extension;
            $order->doctor_id = $doctor_id;
            $order->patient_id = $patient_id;
            $order->order_name = $random_order;

            $order->save();
            $order_id = $order->id;
            $this->sendCustomMail($order_id,$random_order,$patient_id,$doctor_id);
        }
        elseif($prescription_file){
            $p_extension = $prescription_file->getClientOriginalExtension();

            $order = new Order();

            $path_to_move = storage_path().'/app/presc_note/';
            $short_directory = 'app/presc_note/';

            $random_order = 'ORDER.'.Carbon::now()->format('dmY').'.'.mt_rand(0, 999);

            $prescription_file->move($path_to_move, 'PRESC.'.$random_order.'.'.$p_extension);

            $order->prescription_filename = $short_directory.'PRESC.'.$random_order.'.'.$p_extension;
            $order->doctor_id = $doctor_id;
            $order->patient_id = $patient_id;
            $order->order_name = $random_order;

            $order->save();
            $order_id = $order->id;
            $this->sendCustomMail($order_id,$random_order,$patient_id,$doctor_id);
        }
        elseif($note_file){
            $extension = $note_file->getClientOriginalExtension();

            $order = new Order();

            $path_to_move = storage_path().'/app/presc_note/';
            $short_directory = 'app/presc_note/';

            $random_order = 'ORDER.'.Carbon::now()->format('dmY').'.'.mt_rand(0, 999);

            $note_file->move($path_to_move, 'NOTE1.'.$random_order.'.'.$extension);

            $order->note_filename = $short_directory.'NOTE1.'.$random_order.'.'.$extension;
            $order->doctor_id = $doctor_id;
            $order->patient_id = $patient_id;
            $order->order_name = $random_order;

            $order->save();
            $order_id = $order->id;
            $this->sendCustomMail($order_id,$random_order,$patient_id,$doctor_id);
        }
        elseif($patient_id){
            $order = new Order();
            $random_order = 'ORDER.'.Carbon::now()->format('dmY').'.'.mt_rand(0, 999);
            $order->doctor_id = $doctor_id;
            $order->patient_id = $patient_id;
            $order->order_name = $random_order;

            $order->save();
            $order_id = $order->id;
            $this->sendCustomMail($order_id,$random_order,$patient_id,$doctor_id);
        }

	}
    private function updateOnlyOrder($patient_id,$order_id)
	{
$order_info = Order::where('id',$order_id)->first();
		$order = Order::find($order_id);

		$order->patient_id = $patient_id;

		$order->save();

        $this->sendCustomMail($order_id,$order_info->order_name,$patient_id,$order_info->doctor_id);

	}


    /** edit an order **/
    public function edit_order($orderID){
        //$order = new Order();
        //$orders = $order->where('id', $orderID)->first();

        $doctor_id = Auth::user()->getkey();
        $orders = DB::table('orders')
            ->leftjoin('patients', function($join)
            {
                $join->on('orders.patient_id', '=', 'patients.id');
            })

            ->leftjoin('doctors', function($join)
            {
                $join->on('orders.doctor_id', '=', 'doctors.user_id');
            })

            ->where('orders.active_status', '=', 1)
            ->where('orders.id', $orderID)
            ->select('orders.id','orders.patient_id', 'orders.status', 'orders.order_name', 'orders.notes', 'prescription_filename','note_filename', 'additional_filename1', 'additional_filename2', 'additional_filename3', 'orders.created_at', 'orders.updated_at', 'patients.id AS pid', 'patients.first_name', 'patients.last_name', 'patients.date_of_birth', 'doctors.first_name AS d_fname', 'doctors.last_name AS d_lname')
            ->first();

        return view('vascorx.pages.edit-order', compact('orders'));
    }
    /** /edit an order **/


    /** edit an order save  **/
    public function edit_order_save(Request $request)
    {
        $OrderID = $request->get('id');
        $random_order = $request->get('order_number');
        $PatientID = $request->get('pid');
        $sid = $request->get('sid');
        $first_name = $request->get('first_name');
        $last_name = $request->get('last_name');
        $dd = $request->get('dd');
        $mm = $request->get('mm');
        $yyyy = $request->get('yyyy');
        $date_of_birth = strtotime($dd.'-'.$mm.'-'.$yyyy);

        //$note = $request->get('notes');
        $prescription_filename = $request->file('prescription_filename');
        $note_filename = $request->file('note_filename');

        $additional_file1 = $request->file('additional_file1');
        $additional_file2 = $request->file('additional_file2');
        $additional_file3 = $request->file('additional_file3');

        $order = new Order();

        $path_to_move = storage_path().'/app/presc_note/';
        $short_directory = 'app/presc_note/';

//        if($sid == 0){
//            Patient::where('id', $PatientID)
//                ->update(
//                    array("first_name" => $first_name,
//                        "last_name" => $last_name,
//                        "date_of_birth" => $date_of_birth
//                    )
//                );
//            flash()->success('Order updated successfully !!');
//            return redirect('orderlist');
//        }

//        if($note){
//            Order::where('id', $OrderID)
//                ->update(
//                    array(
//                        "notes" => $note
//                    )
//                );
//        }

        if($prescription_filename AND $note_filename){
            $pfile_extension = $prescription_filename->getClientOriginalExtension();
            $nfile_extension = $note_filename->getClientOriginalExtension();

            $prescription_filename->move($path_to_move, 'PRESC.'.$random_order.'.'.$pfile_extension);
            $note_filename->move($path_to_move, 'NOTE1.'.$random_order.'.'.$nfile_extension);

            $pfile_path = $short_directory.'PRESC.'.$random_order.'.'.$pfile_extension;
            $nfile_path = $short_directory.'NOTE1.'.$random_order.'.'.$nfile_extension;


            Order::where('id', $OrderID)
                ->update(
                    array("prescription_filename" => $pfile_path,
                        "note_filename" => $nfile_path,
                        "note" => $note,
                        "status" => 0
                    )
                );

            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );
            flash()->success('Files updated successfully !!');
            return redirect('orderlist');
        }

//        elseif($prescription_filename AND $note_filename AND $first_name OR $last_name OR $dd OR $mm OR $yyyy){
//            $pfile_extension = $prescription_filename->getClientOriginalExtension();
//            $nfile_extension = $note_filename->getClientOriginalExtension();
//
//            $prescription_filename->move($path_to_move, 'PRESC.'.$random_order.'.'.$pfile_extension);
//            $note_filename->move($path_to_move, 'NOTE1.'.$random_order.'.'.$nfile_extension);
//
//            $pfile_path = $short_directory.'PRESC.'.$random_order.'.'.$pfile_extension;
//            $nfile_path = $short_directory.'NOTE1.'.$random_order.'.'.$nfile_extension;
//
//
//            Order::where('id', $OrderID)
//                ->update(
//                    array("prescription_filename" => $pfile_path,
//                        "note_filename" => $nfile_path,
//                        "first_name" => $first_name,
//                        "last_name" => $last_name,
//                        "date_of_birth" => $date_of_birth,
//                        "status" => 0
//                    )
//                );
//            flash()->success('Files updated with Patient details !!');
//            return redirect('orderlist');
//        }

//        elseif($prescription_filename AND $first_name OR $last_name OR $dd OR $mm OR $yyyy){
//            $pfile_extension = $prescription_filename->getClientOriginalExtension();
//
//            $prescription_filename->move($path_to_move, 'PRESC.'.$random_order.'.'.$pfile_extension);
//
//            $pfile_path = $short_directory.'PRESC.'.$random_order.'.'.$pfile_extension;
//
//            Order::where('id', $OrderID)
//                ->update(
//                    array("prescription_filename" => $pfile_path,
//                        "first_name" => $first_name,
//                        "last_name" => $last_name,
//                        "date_of_birth" => $date_of_birth,
//                        "status" => 0
//                    )
//                );
//            flash()->success('Order updated with Prescription file !!');
//            return redirect('orderlist');
//        }

//        elseif($note_filename AND $first_name OR $last_name OR $dd OR $mm OR $yyyy){
//
//            $nfile_extension = $note_filename->getClientOriginalExtension();
//            $note_filename->move($path_to_move, 'NOTE1.'.$random_order.'.'.$nfile_extension);
//            $nfile_path = $short_directory.'NOTE1.'.$random_order.'.'.$nfile_extension;
//
//            Order::where('id', $OrderID)
//                ->update(
//                    array("note_filename" => $nfile_path,
//                        "first_name" => $first_name,
//                        "last_name" => $last_name,
//                        "date_of_birth" => $date_of_birth,
//                        "status" => 0
//                    )
//                );
//            flash()->success('Order updated with Note file !!');
//            return redirect('orderlist');
//        }

        elseif($prescription_filename){
            $pfile_extension = $prescription_filename->getClientOriginalExtension();

            $prescription_filename->move($path_to_move, 'PRESC.'.$random_order.'.'.$pfile_extension);

            $pfile_path = $short_directory.'PRESC.'.$random_order.'.'.$pfile_extension;
            Order::where('id', $OrderID)
                ->update(
                    array("prescription_filename" => $pfile_path,
                        "status" => 0
                    )
                );

            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );

            flash()->success('Files updated successfully !!');
            return redirect('orderlist');
        }

        elseif($note_filename){
            $nfile_extension = $note_filename->getClientOriginalExtension();

            $note_filename->move($path_to_move, 'NOTE1.'.$random_order.'.'.$nfile_extension);
            $nfile_path = $short_directory.'NOTE1.'.$random_order.'.'.$nfile_extension;


            Order::where('id', $OrderID)
                ->update(
                    array("note_filename" => $nfile_path,
                        "status" => 0
                    )
                );

            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );

            flash()->success('Files updated successfully !!');
            return redirect('orderlist');
        }

        elseif($additional_file1 AND $additional_file2 AND $additional_file3){
            $additional_file1_extension = $additional_file1->getClientOriginalExtension();
            $additional_file2_extension = $additional_file2->getClientOriginalExtension();
            $additional_file3_extension = $additional_file3->getClientOriginalExtension();

            $additional_file1->move($path_to_move, 'NOTE2.'.$random_order.'.'.$additional_file1_extension);
            $additional_file2->move($path_to_move, 'NOTE3.'.$random_order.'.'.$additional_file2_extension);
            $additional_file3->move($path_to_move, 'NOTE4.'.$random_order.'.'.$additional_file3_extension);

            $additional_file1_path = $short_directory.'NOTE2.'.$random_order.'.'.$additional_file1_extension;
            $additional_file2_path = $short_directory.'NOTE3.'.$random_order.'.'.$additional_file2_extension;
            $additional_file3_path = $short_directory.'NOTE4.'.$random_order.'.'.$additional_file3_extension;

            Order::where('id', $OrderID)
                ->update(
                    array("additional_filename1" => $additional_file1_path,
                        "additional_filename2" => $additional_file2_path,
                        "additional_filename3" => $additional_file3_path,
                        "status" => 0
                    )
                );

            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );

            flash()->success('Files updated successfully !!');
            return redirect('orderlist');
        }

        elseif($additional_file1 AND $additional_file2 AND $additional_file3 AND $prescription_filename AND $note_filename){
            $additional_file1_extension = $additional_file1->getClientOriginalExtension();
            $additional_file2_extension = $additional_file2->getClientOriginalExtension();
            $additional_file3_extension = $additional_file3->getClientOriginalExtension();

            $pfile_extension = $prescription_filename->getClientOriginalExtension();
            $nfile_extension = $note_filename->getClientOriginalExtension();

            $additional_file1->move($path_to_move, 'NOTE2.'.$random_order.'.'.$additional_file1_extension);
            $additional_file2->move($path_to_move, 'NOTE3.'.$random_order.'.'.$additional_file2_extension);
            $additional_file3->move($path_to_move, 'NOTE4.'.$random_order.'.'.$additional_file3_extension);

            $prescription_filename->move($path_to_move, 'PRESC.'.$random_order.'.'.$pfile_extension);
            $note_filename->move($path_to_move, 'NOTE1.'.$random_order.'.'.$nfile_extension);

            $additional_file1_path = $short_directory.'NOTE2.'.$random_order.'.'.$additional_file1_extension;
            $additional_file2_path = $short_directory.'NOTE3.'.$random_order.'.'.$additional_file2_extension;
            $additional_file3_path = $short_directory.'NOTE4.'.$random_order.'.'.$additional_file3_extension;

            $pfile_path = $short_directory.'PRESC.'.$random_order.'.'.$pfile_extension;
            $nfile_path = $short_directory.'NOTE1.'.$random_order.'.'.$nfile_extension;

            Order::where('id', $OrderID)
                ->update(
                    array("prescription_filename" => $pfile_path,
                        "note_filename" => $nfile_path,
                        "additional_filename1" => $additional_file1_path,
                        "additional_filename2" => $additional_file2_path,
                        "additional_filename3" => $additional_file3_path,
                        "status" => 0
                    )
                );

            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );

            flash()->success('Order updated successfully !!');
            return redirect('orderlist');
        }

        elseif($additional_file1 AND $additional_file2 AND $additional_file3 AND $prescription_filename){
            $additional_file1_extension = $additional_file1->getClientOriginalExtension();
            $additional_file2_extension = $additional_file2->getClientOriginalExtension();
            $additional_file3_extension = $additional_file3->getClientOriginalExtension();

            $pfile_extension = $prescription_filename->getClientOriginalExtension();

            $additional_file1->move($path_to_move, 'NOTE2.'.$random_order.'.'.$additional_file1_extension);
            $additional_file2->move($path_to_move, 'NOTE3.'.$random_order.'.'.$additional_file2_extension);
            $additional_file3->move($path_to_move, 'NOTE4.'.$random_order.'.'.$additional_file3_extension);

            $prescription_filename->move($path_to_move, 'PRESC.'.$random_order.'.'.$pfile_extension);

            $additional_file1_path = $short_directory.'NOTE2.'.$random_order.'.'.$additional_file1_extension;
            $additional_file2_path = $short_directory.'NOTE3.'.$random_order.'.'.$additional_file2_extension;
            $additional_file3_path = $short_directory.'NOTE4.'.$random_order.'.'.$additional_file3_extension;

            $pfile_path = $short_directory.'PRESC.'.$random_order.'.'.$pfile_extension;

            Order::where('id', $OrderID)
                ->update(
                    array("prescription_filename" => $pfile_path,
                        "additional_filename1" => $additional_file1_path,
                        "additional_filename2" => $additional_file2_path,
                        "additional_filename3" => $additional_file3_path,
                        "status" => 0
                    )
                );

            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );

            flash()->success('Order updated successfully !!');
            return redirect('orderlist');
        }

        elseif($additional_file1 AND $additional_file2 AND $additional_file3 AND $note_filename){
            $additional_file1_extension = $additional_file1->getClientOriginalExtension();
            $additional_file2_extension = $additional_file2->getClientOriginalExtension();
            $additional_file3_extension = $additional_file3->getClientOriginalExtension();

            $nfile_extension = $note_filename->getClientOriginalExtension();

            $additional_file1->move($path_to_move, 'NOTE2.'.$random_order.'.'.$additional_file1_extension);
            $additional_file2->move($path_to_move, 'NOTE3.'.$random_order.'.'.$additional_file2_extension);
            $additional_file3->move($path_to_move, 'NOTE4.'.$random_order.'.'.$additional_file3_extension);

            $note_filename->move($path_to_move, 'NOTE1.'.$random_order.'.'.$nfile_extension);

            $additional_file1_path = $short_directory.'NOTE2.'.$random_order.'.'.$additional_file1_extension;
            $additional_file2_path = $short_directory.'NOTE3.'.$random_order.'.'.$additional_file2_extension;
            $additional_file3_path = $short_directory.'NOTE4.'.$random_order.'.'.$additional_file3_extension;

            $nfile_path = $short_directory.'NOTE1.'.$random_order.'.'.$nfile_extension;

            Order::where('id', $OrderID)
                ->update(
                    array("note_filename" => $nfile_path,
                        "additional_filename1" => $additional_file1_path,
                        "additional_filename2" => $additional_file2_path,
                        "additional_filename3" => $additional_file3_path,
                        "status" => 0
                    )
                );

            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );

            flash()->success('Order updated successfully !!');
            return redirect('orderlist');
        }

        elseif($additional_file1 AND $additional_file2 AND $prescription_filename AND $note_filename){
            $additional_file1_extension = $additional_file1->getClientOriginalExtension();
            $additional_file2_extension = $additional_file2->getClientOriginalExtension();
            $pfile_extension = $prescription_filename->getClientOriginalExtension();
            $nfile_extension = $note_filename->getClientOriginalExtension();

            $additional_file1->move($path_to_move, 'NOTE2.'.$random_order.'.'.$additional_file1_extension);
            $additional_file2->move($path_to_move, 'NOTE3.'.$random_order.'.'.$additional_file2_extension);
            $prescription_filename->move($path_to_move, 'PRESC.'.$random_order.'.'.$pfile_extension);
            $note_filename->move($path_to_move, 'NOTE1.'.$random_order.'.'.$nfile_extension);

            $additional_file1_path = $short_directory.'NOTE2.'.$random_order.'.'.$additional_file1_extension;
            $additional_file2_path = $short_directory.'NOTE3.'.$random_order.'.'.$additional_file2_extension;
            $pfile_path = $short_directory.'PRESC.'.$random_order.'.'.$pfile_extension;
            $nfile_path = $short_directory.'NOTE1.'.$random_order.'.'.$nfile_extension;

            Order::where('id', $OrderID)
                ->update(
                    array("prescription_filename" => $pfile_path,
                        "note_filename" => $nfile_path,
                        "additional_filename1" => $additional_file1_path,
                        "additional_filename2" => $additional_file2_path,
                        "status" => 0
                    )
                );

            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );

            flash()->success('Order updated successfully !!');
            return redirect('orderlist');
        }

        elseif($additional_file1 AND $additional_file2 AND $prescription_filename){
            $additional_file1_extension = $additional_file1->getClientOriginalExtension();
            $additional_file2_extension = $additional_file2->getClientOriginalExtension();
            $pfile_extension = $prescription_filename->getClientOriginalExtension();


            $additional_file1->move($path_to_move, 'NOTE2.'.$random_order.'.'.$additional_file1_extension);
            $additional_file2->move($path_to_move, 'NOTE3.'.$random_order.'.'.$additional_file2_extension);
            $prescription_filename->move($path_to_move, 'PRESC.'.$random_order.'.'.$pfile_extension);


            $additional_file1_path = $short_directory.'NOTE2.'.$random_order.'.'.$additional_file1_extension;
            $additional_file2_path = $short_directory.'NOTE3.'.$random_order.'.'.$additional_file2_extension;
            $pfile_path = $short_directory.'PRESC.'.$random_order.'.'.$pfile_extension;


            Order::where('id', $OrderID)
                ->update(
                    array("prescription_filename" => $pfile_path,
                        "additional_filename1" => $additional_file1_path,
                        "additional_filename2" => $additional_file2_path,
                        "status" => 0
                    )
                );

            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );

            flash()->success('Order updated successfully !!');
            return redirect('orderlist');
        }

        elseif($additional_file1 AND $additional_file2 AND $note_filename){
            $additional_file1_extension = $additional_file1->getClientOriginalExtension();
            $additional_file2_extension = $additional_file2->getClientOriginalExtension();

            $nfile_extension = $note_filename->getClientOriginalExtension();

            $additional_file1->move($path_to_move, 'NOTE2.'.$random_order.'.'.$additional_file1_extension);
            $additional_file2->move($path_to_move, 'NOTE3.'.$random_order.'.'.$additional_file2_extension);

            $note_filename->move($path_to_move, 'NOTE1.'.$random_order.'.'.$nfile_extension);

            $additional_file1_path = $short_directory.'NOTE2.'.$random_order.'.'.$additional_file1_extension;
            $additional_file2_path = $short_directory.'NOTE3.'.$random_order.'.'.$additional_file2_extension;

            $nfile_path = $short_directory.'NOTE1.'.$random_order.'.'.$nfile_extension;

            Order::where('id', $OrderID)
                ->update(
                    array("note_filename" => $nfile_path,
                        "additional_filename1" => $additional_file1_path,
                        "additional_filename2" => $additional_file2_path,
                        "status" => 0
                    )
                );

            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );

            flash()->success('Order updated successfully !!');
            return redirect('orderlist');
        }

        elseif($additional_file1 AND $additional_file2){
            $additional_file1_extension = $additional_file1->getClientOriginalExtension();
            $additional_file2_extension = $additional_file2->getClientOriginalExtension();

            $additional_file1->move($path_to_move, 'NOTE2.'.$random_order.'.'.$additional_file1_extension);
            $additional_file2->move($path_to_move, 'NOTE3.'.$random_order.'.'.$additional_file2_extension);

            $additional_file1_path = $short_directory.'NOTE2.'.$random_order.'.'.$additional_file1_extension;
            $additional_file2_path = $short_directory.'NOTE3.'.$random_order.'.'.$additional_file2_extension;

            Order::where('id', $OrderID)
                ->update(
                    array("additional_filename1" => $additional_file1_path,
                        "additional_filename2" => $additional_file2_path,
                        "status" => 0
                    )
                );
            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );

            flash()->success('Order updated successfully !!');
            return redirect('orderlist');
        }

        elseif($additional_file2 AND $additional_file3){
            $additional_file3_extension = $additional_file3->getClientOriginalExtension();
            $additional_file2_extension = $additional_file2->getClientOriginalExtension();

            $additional_file3->move($path_to_move, 'NOTE4.'.$random_order.'.'.$additional_file3_extension);
            $additional_file2->move($path_to_move, 'NOTE3.'.$random_order.'.'.$additional_file2_extension);

            $additional_file3_path = $short_directory.'NOTE4.'.$random_order.'.'.$additional_file3_extension;
            $additional_file2_path = $short_directory.'NOTE3.'.$random_order.'.'.$additional_file2_extension;

            Order::where('id', $OrderID)
                ->update(
                    array("additional_filename2" => $additional_file2_path,
                        "additional_filename3" => $additional_file3_path,
                        "status" => 0
                    )
                );

            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );

            flash()->success('Order updated successfully !!');
            return redirect('orderlist');
        }

        elseif($additional_file1 AND $prescription_filename AND $note_filename){
            $additional_file1_extension = $additional_file1->getClientOriginalExtension();

            $pfile_extension = $prescription_filename->getClientOriginalExtension();
            $nfile_extension = $note_filename->getClientOriginalExtension();

            $additional_file1->move($path_to_move, 'NOTE2.'.$random_order.'.'.$additional_file1_extension);

            $prescription_filename->move($path_to_move, 'PRESC.'.$random_order.'.'.$pfile_extension);
            $note_filename->move($path_to_move, 'NOTE1.'.$random_order.'.'.$nfile_extension);

            $additional_file1_path = $short_directory.'NOTE2.'.$random_order.'.'.$additional_file1_extension;

            $pfile_path = $short_directory.'PRESC.'.$random_order.'.'.$pfile_extension;
            $nfile_path = $short_directory.'NOTE1.'.$random_order.'.'.$nfile_extension;

            Order::where('id', $OrderID)
                ->update(
                    array("prescription_filename" => $pfile_path,
                        "note_filename" => $nfile_path,
                        "additional_filename1" => $additional_file1_path,
                        "status" => 0
                    )
                );

            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );

            flash()->success('Order updated successfully !!');
            return redirect('orderlist');
        }

        elseif($additional_file1 AND $prescription_filename){
            $additional_file1_extension = $additional_file1->getClientOriginalExtension();

            $pfile_extension = $prescription_filename->getClientOriginalExtension();

            $additional_file1->move($path_to_move, 'NOTE2.'.$random_order.'.'.$additional_file1_extension);

            $prescription_filename->move($path_to_move, 'PRESC.'.$random_order.'.'.$pfile_extension);

            $additional_file1_path = $short_directory.'NOTE2.'.$random_order.'.'.$additional_file1_extension;

            $pfile_path = $short_directory.'PRESC.'.$random_order.'.'.$pfile_extension;

            Order::where('id', $OrderID)
                ->update(
                    array("prescription_filename" => $pfile_path,
                        "additional_filename1" => $additional_file1_path,
                        "status" => 0
                    )
                );

            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );

            flash()->success('Order updated successfully !!');
            return redirect('orderlist');
        }

        elseif($additional_file1 AND $note_filename){
            $additional_file1_extension = $additional_file1->getClientOriginalExtension();

            $nfile_extension = $note_filename->getClientOriginalExtension();

            $additional_file1->move($path_to_move, 'NOTE2.'.$random_order.'.'.$additional_file1_extension);

            $note_filename->move($path_to_move, 'NOTE1.'.$random_order.'.'.$nfile_extension);

            $additional_file1_path = $short_directory.'NOTE2.'.$random_order.'.'.$additional_file1_extension;

            $nfile_path = $short_directory.'NOTE1.'.$random_order.'.'.$nfile_extension;

            Order::where('id', $OrderID)
                ->update(
                    array("note_filename" => $nfile_path,
                        "additional_filename1" => $additional_file1_path,
                        "status" => 0
                    )
                );

            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );

            flash()->success('Order updated successfully !!');
            return redirect('orderlist');
        }

        elseif($additional_file1){
            $additional_file1_extension = $additional_file1->getClientOriginalExtension();
            $additional_file1->move($path_to_move, 'NOTE2.'.$random_order.'.'.$additional_file1_extension);

            $additional_file1_path = $short_directory.'NOTE2.'.$random_order.'.'.$additional_file1_extension;

            Order::where('id', $OrderID)
                ->update(
                    array("additional_filename1" => $additional_file1_path,
                        "status" => 0
                    )
                );

            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );

            flash()->success('Order updated successfully !!');
            return redirect('orderlist');
        }

        elseif($additional_file2){
            $additional_file2_extension = $additional_file2->getClientOriginalExtension();
            $additional_file2->move($path_to_move, 'NOTE3.'.$random_order.'.'.$additional_file2_extension);

            $additional_file2_path = $short_directory.'NOTE3.'.$random_order.'.'.$additional_file2_extension;

            Order::where('id', $OrderID)
                ->update(
                    array("additional_filename2" => $additional_file2_path,
                        "status" => 0
                    )
                );

            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );

            flash()->success('Order updated successfully !!');
            return redirect('orderlist');
        }

        elseif($first_name OR $last_name OR $dd OR $mm OR $yyyy){
            Patient::where('id', $PatientID)
                ->update(
                    array("first_name" => $first_name,
                        "last_name" => $last_name,
                        "date_of_birth" => $date_of_birth
                    )
                );
            flash()->success('Order updated successfully !!');
            return redirect('orderlist');
        }

        else{
            flash()->error('Please upload atleast one file !!');
            return redirect('edit_order/'.$OrderID);
        }

    }
    /** /edit an order save  **/

    /** Clear Notification  **/
    public function ClearNotification()
    {
        $id = $_REQUEST['orderid'];
        Order::where('id', $id)
            ->update(
                array("view_status" => 1)
            );

        //flash()->success('One notification cleared');
        //$str = "ok";

        //return redirect('dashboard');
    }
    /** /Clear Notification  **/


    public function sendCustomMail($order_id,$order_name,$patient_id='',$doctor_id)
    {
$doctor_info = Doctor::where('user_id',$doctor_id)->first();

        $headers = "From: VascoRX <" . strip_tags("info@vascorx.com") . ">\r\n";
        $headers .= "Reply-To: VascoRX <" . strip_tags("info@vascorx.com") . ">\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";

        $body = '<p>Order has been created.  </p>

<strong>Order information:</strong><br>
<strong>Order Created By</strong>: '.$doctor_info->first_name.' '.$doctor_info->last_name.'<br>
<strong>Doctor ID</strong>: '.$doctor_id.'<br>
<strong>Patient ID</strong>: '.$patient_id.'<br>
<strong>Order ID</strong>: '.$order_id.'<br>
<strong>Order Name</strong>: '.$order_name.'<br>';
if($patient_id == ''){
    $body.='<br><strong>Patient ID</strong>: <span style="color:red;font-weight:bold;">No Patient Select for this Order</span><br>';
}
        $email_temp = '<html>
<head>
<title>VascoRX</title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
</head>
<body bgcolor="#d4d2d2" leftmargin="0" topmargin="0" marginwidth="0" marginheight="0">
<table id="Table_01" width="600" height="100%" border="0" cellpadding="0" cellspacing="0" align="center">
  <tr>
    <td colspan="3" style="border-bottom:3px solid #730606;"><img src="http://vascorx.tyschell.com/public/assets/images/logo.png" width="300" height="47" alt=""></td>
  </tr>
  <tr>

  <td width="23" height="2" bgcolor="#ffffff"></td>
  <td rowspan="2" width="540" height="100%" bgcolor="#ffffff" valign="top">

  <table cellpadding="0" cellspacing="0">
    <tr>
      <td style="padding-top:30px; padding-bottom:30px;" style="font-family:Arial, sans-serif;">
      Dear User,<br>
      <br>

	 '.$body.'

	  <br>
      <br>
      <strong>Regards</strong><br>
      VascoRX Team<br>
    </td>

    </tr>

  </table>
  </td>

  <td width="37" height="2" bgcolor="#ffffff"></td>
  </tr>
  <tr>
    <td></td>
    <td></td>
  </tr>
  <tr>
    <td colspan="3" width="600" height="175" bgcolor="#730606" align="center" style="color:#fff;font-family:Arial, sans-serif"><strong>Contact Information</strong><br>
      Phoenix, Arizona<br>
      <a href="mailto:info@vascorx.com" style="color:#ffffff;">info@vascorx.com</a><br></td>
  </tr>
</table>
</body>
</html>';

        $subDoctor = "VascoRX: Order submitted: ".$order_id;
        $subAdmin = "VascoRx: New Order: ".$order_id;


        /*This is email Body Content for sending an email after user registration End*/

        mail($doctor_info->email, $subDoctor, $email_temp, $headers);

        /*For All Admin Email Send*/
        $allAdminRoleID = User::where('user_role','admin')->get();
        foreach($allAdminRoleID as $adminID){
            $adminEmail = Admin::where('user_id',$adminID->id)->first();

            mail($adminEmail->email, $subAdmin, $email_temp, $headers);
        }

    }




}
