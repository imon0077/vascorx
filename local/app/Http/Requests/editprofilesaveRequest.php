<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

use Illuminate\Foundation\Http\FormRequest;
use Response;
use Auth;

class editprofilesaveRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
        if(Auth::user()->hasRole('admin')) {
         return [
             'pharmacy_name' => 'required',
             'first_name' => 'required',
             'last_name' => 'required',
             'email' => 'required',
             'phn_number' => 'required'
         ];
        }
        else{
         return[
             'contact_name' => 'required',
             'first_name' => 'required',
             'last_name' => 'required',
             'email' => 'required',
             'phn_number' => 'required'
         ];
        }
	}

}
