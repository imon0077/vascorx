<?php namespace App\Http\Requests;

use App\Http\Requests\Request;

class signupDoctorsRequest extends Request {

	/**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize()
	{
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules()
	{
		return [
			'first_name' => 'required',
			'last_name' => 'required',
			'email' => 'required|email|unique:doctors,email',
			'user_name' => 'required|unique:users,username|max:255',
			'password' => 'required|between:6,20|confirmed'
		];
	}

}
