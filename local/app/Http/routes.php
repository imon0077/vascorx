<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function(){
	return view('vascorx.pages.login');
});

Route::controllers([
	'auth' => 'Auth\AuthController',
	'password' => 'Auth\PasswordController',
]);
// Password reset link request routes...
//Route::get('password/email', 'PasswordController@getEmail');
Route::post('password/email', 'PasswordController@postEmail');
// Password reset routes...
Route::get('password/reset/{token}', 'PasswordController@getReset');
Route::post('password/reset', 'PasswordController@postReset');

Route::get('login', function(){
	return view('vascorx.pages.login');
});
Route::get('recover-password', function(){
	return view('vascorx.pages.recover-password');
});

Route::get('dashboard', 'AdminsController@index');
Route::post('dashboard', 'AdminsController@update');

Route::get('signup', 'SignupController@index');
Route::post('signup', 'SignupController@signupdoctors');

Route::get('create-patient', 'PatientsController@index');
Route::post('create-patient', 'PatientsController@store');
Route::get('patientlist', 'AdminsController@patientlist');
Route::get('create-order', 'OrdersController@index');
Route::get('orderlist', 'OrdersController@OrderLists');
Route::get('orderDetails/{orderID}', 'OrdersController@OrderDetails');
Route::post('changeStatusFromAdmin', 'OrdersController@changeStatusFromAdmin');
Route::post('changeMemoFromAdmin', 'OrdersController@changeMemoFromAdmin');
Route::post('NotesUpdate', 'OrdersController@NotesUpdate');
Route::get('orderUpdate/{orderID}', 'OrdersController@OrderUpdateView');
Route::post('create-order', 'OrdersController@store');
Route::post('update-order', 'OrdersController@OrderUpdate');
Route::post('ordertransfer', 'AdminsController@orderTransfer');

Route::get('newDoctors', 'AdminsController@newDoctors');
Route::get('Doctors', 'AdminsController@Doctors');
Route::get('admin', 'AdminsController@index');

//Route::post('decline', 'AdminsController@decline');

Route::get('editdoctor/{user_id}', [
    'as' => 'editdoctor',
    'uses' => 'DoctorsController@editdoctor'
]);

Route::post('editdoctorsave', [
    'as' => 'editdoctorsave',
    'uses' => 'DoctorsController@editdoctorsave'
]);

Route::get('deletedoctor/{user_id}', [
    'as' => 'deletedoctor',
    'uses' => 'DoctorsController@deletedoctor'
]);

Route::get('profile', [
    'as' => 'profile',
    'uses' => 'DoctorsController@profile'
]);

Route::get('editprofile/{user_id}', [
    'as' => 'editprofile',
    'uses' => 'DoctorsController@editprofile'
]);

Route::post('editprofilesave', [
    'as' => 'editprofilesave',
    'uses' => 'DoctorsController@editprofilesave'
]);

Route::get('CreateDoctors', [
    'as' => 'CreateDoctors',
    'uses' => 'DoctorsController@CreateDoctors'
]);

Route::post('CreateDoctorsSave', [
    'as' => 'CreateDoctorsSave',
    'uses' => 'DoctorsController@CreateDoctorsSave'
]);

Route::get('accept/{doctorID}', [
    'as' => 'accept_doctors',
    'uses' => 'AdminsController@accept'
]);

Route::get('decline/{doctorID}', [
    'as' => 'decline_doctors',
    'uses' => 'AdminsController@decline'
]);

Route::get('edit_patient/{patientID}', [
    'as' => 'edit_patient',
    'uses' => 'PatientsController@edit_patient'
]);

Route::get('del_patient/{patientID}', [
    'as' => 'edit_patient',
    'uses' => 'PatientsController@del_patient'
]);

Route::post('editSave', [
    'as' => 'editSave',
    'uses' => 'PatientsController@editSave'
]);

Route::get('edit_order/{orderID}', [
    'as' => 'edit_order',
    'uses' => 'OrdersController@edit_order'
]);

Route::post('edit_order_save', [
    'as' => 'edit_order_save',
    'uses' => 'OrdersController@edit_order_save'
]);

Route::get('ClearNotification', [
    'as' => 'ClearNotification',
    'uses' => 'OrdersController@ClearNotification'
]);

Route::post('notification', [
    'as' => 'notification',
    'uses' => 'AdminsController@notification'
]);

Route::get('contact', [
    'as' => 'contact',
    'uses' => 'DoctorsController@contact'
]);
