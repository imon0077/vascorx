<?php namespace App;

use Illuminate\Database\Eloquent\Model;

class Doctor extends Model {

	protected $guarded = ['user_id'];

	protected $table = 'doctors';
}
