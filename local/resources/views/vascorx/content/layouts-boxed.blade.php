@extends('porto.layout.master')



@section('content')
<header class="page-header">
    <h2>Boxed Layout</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Layouts</span></li>
            <li><span>Boxed</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>

<!-- start: page -->

<!-- end: page -->
@stop