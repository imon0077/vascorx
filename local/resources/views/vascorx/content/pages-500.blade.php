@extends('porto.layout.master-nobar')

@section('content')
<!-- start: page -->
<section class="body-error error-outside">
    <div class="center-error">

        <div class="row">
            <div class="col-sm-8">
                <div class="main-error mb-xlg">
                    <h2 class="error-code text-dark text-center text-semibold m-none">500 <i class="fa fa-file"></i></h2>
                    <p class="error-explanation text-center">We're sorry, something went wrong.</p>
                </div>
            </div>
            <div class="col-sm-4">
                <h4 class="text">Here are some useful links</h4>
                <ul class="nav nav-list primary">
                    <li><a href="#"><i class="fa fa-caret-right text-dark"></i> Dashboard</a></li>
                    <li><a href="#"><i class="fa fa-caret-right text-dark"></i> User Profile</a></li>
                    <li><a href="#"><i class="fa fa-caret-right text-dark"></i> FAQ's</a></li>
                </ul>
            </div>
        </div>
    </div>
</section>
<!-- end: page -->
@stop