@extends('porto.layout.master')



@section('content')
<header class="page-header">
    <h2>Ajax Tables</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Tables</span></li>
            <li><span>Ajax</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
        </div>

        <h2 class="panel-title">Ajax</h2>
    </header>
    <div class="panel-body">
        <table class="table table-bordered table-striped" id="datatable-ajax" data-url="assets/ajax/ajax-datatables-sample.json">
            <thead>
            <tr>
                <th width="20%">Rendering engine</th>
                <th width="25%">Browser</th>
                <th width="25%">Platform(s)</th>
                <th width="15%">Engine version</th>
                <th width="15%">CSS grade</th>
            </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
</section>
<!-- end: page -->
@stop