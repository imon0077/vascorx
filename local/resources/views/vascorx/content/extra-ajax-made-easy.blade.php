@extends('porto.layout.master')



@section('content')
<header class="page-header">
    <h2>Ajax Made Easy</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Extra</span></li>
            <li><span>Ajax Made Easy</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>

<!-- start: page -->
<div class="row">
    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">Get Content on Page load</h2>
            </header>
            <div class="panel-body" data-loading-overlay data-loading-overlay-options='{ "startShowing": true }' style="min-height: 150px;" ic-get-from="/example-lazy-load" ic-trigger-on="load">
            </div>
        </section>
    </div>

    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a id="ListenOnExample" href="#" class="panel-action fa fa-refresh" ic-get-from="/example-refresh" ic-target="#ExampleRefreshTarget"></a>
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">Refresh Content</h2>
            </header>
            <div id="ExampleRefreshTarget" class="panel-body" data-loading-overlay data-loading-overlay-options='{ "listenOn": "#ListenOnExample" }' style="min-height: 150px;">
                <p>This is an initial content. Click on refresh icon to update content.</p>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action fa fa-refresh" ic-trigger-on="click" ic-prepend-from="/example-load-more" ic-target="#ExampleLoadMorePrependTarget"></a>
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">Prepend Content</h2>
            </header>
            <div id="ExampleLoadMorePrependTarget" class="panel-body" style="min-height: 150px;">
                <p>This is an initial content. Click on refresh icon to preppend content.</p>
            </div>
        </section>
    </div>

    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action fa fa-refresh" ic-trigger-on="click" ic-append-from="/example-load-more" ic-target="#ExampleLoadMoreAppendTarget"></a>
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">Append Content</h2>
            </header>
            <div id="ExampleLoadMoreAppendTarget" class="panel-body" style="min-height: 150px;">
                <p>This is an initial content. Click on refresh icon to append content.</p>
            </div>
        </section>
    </div>
</div>

<div class="row">
    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">Polling Content</h2>
            </header>
            <div class="panel-body" style="min-height: 150px;">
                <ul ic-prepend-from="/example-polling" ic-poll="2s" ic-limit-children="5">
                    <li>First Content</li>
                </ul>
            </div>
        </section>
    </div>

    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">Scrollable - End scroll</h2>
            </header>
            <div class="panel-body">
                <div class="scrollable" ic-trigger-on="scrollend" ic-append-from="/example-scroll" ic-target="#ScrollableTarget" data-plugin-scrollable style="height: 120px;">
                    <div id="ScrollableTarget" class="scrollable-content">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In nec egestas felis, ut posuere est. Etiam eleifend, lacus in pretium placerat, augue nulla pretium ex, non accumsan est sapien et arcu. In facilisis euismod justo, id ultricies purus efficitur eu. Nullam a commodo turpis. Nam vitae neque tellus. Sed luctus, ante tincidunt placerat vestibulum, mi dui placerat sapien, consectetur posuere nisl sem non odio. Proin eget metus lobortis, tristique diam bibendum, aliquet nisi. Aliquam sed finibus quam, sed lobortis justo. In cursus magna id scelerisque accumsan. Cras eleifend accumsan ligula, in elementum libero bibendum ut. Maecenas tempor faucibus vulputate.</p>
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>
<!-- end: page -->
@stop