@extends('porto.layout.master')



@section('content')
<header class="page-header">
    <h2>Session Timeout</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>Pages</span></li>
            <li><span>Session Timeout</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>

<!-- start: page -->
If you don't move, after 10 seconds lock screen will be shown.
<br />
After 15 seconds after document ready, a warning will appear asking if user wants to renew a session.
<br />
If the user doesn't want to, then after 5 more seconds he will be redirected to login page.

<br />
<br />

Note: The code is for demo purposes, you can configure it like you want to.
If you don't want a session timeout you can just make a keep alive call from time to time. :)
<br />
You can find more information looking at source code or at our extensive documentation.

<!-- end: page -->
@stop