@extends('porto.layout.master')



@section('content')
<header class="page-header">
    <h2>Carousels</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>UI Elements</span></li>
            <li><span>Carousels</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>

<!-- start: page -->
<div class="owl-carousel" data-plugin-carousel data-plugin-options='{ "autoPlay": 3000, "items": 6, "itemsDesktop": [1199,4], "itemsDesktopSmall": [979,3], "itemsTablet": [768,2], "itemsMobile": [479,1] }'>
    <div class="item spaced"><img class="img-thumbnail" src="assets/images/projects/project-1.jpg" alt=""></div>
    <div class="item spaced"><img class="img-thumbnail" src="assets/images/projects/project-2.jpg" alt=""></div>
    <div class="item spaced"><img class="img-thumbnail" src="assets/images/projects/project-3.jpg" alt=""></div>
    <div class="item spaced"><img class="img-thumbnail" src="assets/images/projects/project-4.jpg" alt=""></div>
    <div class="item spaced"><img class="img-thumbnail" src="assets/images/projects/project-5.jpg" alt=""></div>
    <div class="item spaced"><img class="img-thumbnail" src="assets/images/projects/project-6.jpg" alt=""></div>
    <div class="item spaced"><img class="img-thumbnail" src="assets/images/projects/project-7.jpg" alt=""></div>
    <div class="item spaced"><img class="img-thumbnail" src="assets/images/projects/project-1.jpg" alt=""></div>
    <div class="item spaced"><img class="img-thumbnail" src="assets/images/projects/project-2.jpg" alt=""></div>
    <div class="item spaced"><img class="img-thumbnail" src="assets/images/projects/project-3.jpg" alt=""></div>
    <div class="item spaced"><img class="img-thumbnail" src="assets/images/projects/project-4.jpg" alt=""></div>
    <div class="item spaced"><img class="img-thumbnail" src="assets/images/projects/project-5.jpg" alt=""></div>
    <div class="item spaced"><img class="img-thumbnail" src="assets/images/projects/project-6.jpg" alt=""></div>
    <div class="item spaced"><img class="img-thumbnail" src="assets/images/projects/project-7.jpg" alt=""></div>
</div>

<hr>

<div class="row mt-lg pt-lg">
    <div class="col-md-3">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">Single Item</h2>
            </header>
            <div class="panel-body">
                <div class="owl-carousel" data-plugin-carousel data-plugin-options='{ "items": 1, "autoHeight": true }'>
                    <div class="item">
                        <img alt="" class="img-responsive" src="assets/images/projects/project-1.jpg">
                    </div>
                    <div class="item">
                        <img alt="" class="img-responsive" src="assets/images/projects/project-2.jpg">
                    </div>
                    <div class="item">
                        <img alt="" class="img-responsive" src="assets/images/projects/project-4.jpg">
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-3">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">Navigation</h2>
            </header>
            <div class="panel-body">
                <div class="owl-carousel" data-plugin-carousel data-plugin-options='{ "items": 1,  "navigation": true, "pagination": false }'>
                    <div class="item">
                        <img alt="" class="img-responsive" src="assets/images/projects/project-2.jpg">
                    </div>
                    <div class="item">
                        <img alt="" class="img-responsive" src="assets/images/projects/project-4.jpg">
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-3">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">Autoplay</h2>
            </header>
            <div class="panel-body">
                <div class="owl-carousel" data-plugin-carousel data-plugin-options='{ "autoPlay": 5000, "items": 1,  "navigation": false, "pagination": true }'>
                    <div class="item">
                        <img alt="" class="img-responsive" src="assets/images/projects/project-7.jpg">
                    </div>
                    <div class="item">
                        <img alt="" class="img-responsive" src="assets/images/projects/project-6.jpg">
                    </div>
                    <div class="item">
                        <img alt="" class="img-responsive" src="assets/images/projects/project-5.jpg">
                    </div>
                </div>
            </div>
        </section>
    </div>
    <div class="col-md-3">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                    <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
                </div>

                <h2 class="panel-title">Controls Disabled</h2>
            </header>
            <div class="panel-body">
                <div class="owl-carousel" data-plugin-carousel data-plugin-options='{ "autoPlay": 2000, "items": 1,  "navigation": false, "pagination": false }'>
                    <div class="item">
                        <img alt="" class="img-responsive" src="assets/images/projects/project-6.jpg">
                    </div>
                    <div class="item">
                        <img alt="" class="img-responsive" src="assets/images/projects/project-2.jpg">
                    </div>
                    <div class="item">
                        <img alt="" class="img-responsive" src="assets/images/projects/project-4.jpg">
                    </div>
                    <div class="item">
                        <img alt="" class="img-responsive" src="assets/images/projects/project-2.jpg">
                    </div>
                    <div class="item">
                        <img alt="" class="img-responsive" src="assets/images/projects/project-1.jpg">
                    </div>
                    <div class="item">
                        <img alt="" class="img-responsive" src="assets/images/projects/project-7.jpg">
                    </div>
                </div>
            </div>
        </section>
    </div>
</div>

<!-- end: page -->
@stop