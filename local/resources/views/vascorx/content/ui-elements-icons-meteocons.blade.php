@extends('porto.layout.master')



@section('content')
<header class="page-header">
    <h2>Meteocons</h2>

    <div class="right-wrapper pull-right">
        <ol class="breadcrumbs">
            <li>
                <a href="index">
                    <i class="fa fa-home"></i>
                </a>
            </li>
            <li><span>UI Elements</span></li>
            <li><span>Icons</span></li>
            <li><span>Meteocons</span></li>
        </ol>

        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>
    </div>
</header>

<!-- start: page -->
<section class="panel">
    <header class="panel-heading">
        <div class="panel-actions">
            <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
            <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
        </div>

        <h2 class="panel-title">47 icons</h2>
    </header>
    <div class="panel-body">
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-sunrise"></i> wi-sunrise</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-sun"></i> wi-sun</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-moon"></i> wi-moon</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-sun2"></i> wi-sun2</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-windy"></i> wi-windy</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-wind"></i> wi-wind</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-snowflake"></i> wi-snowflake</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-cloudy"></i> wi-cloudy</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-cloud"></i> wi-cloud</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-weather"></i> wi-weather</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-weather2"></i> wi-weather2</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-weather3"></i> wi-weather3</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-lines"></i> wi-lines</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-cloud2"></i> wi-cloud2</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-lightning"></i> wi-lightning</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-lightning2"></i> wi-lightning2</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-rainy"></i> wi-rainy</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-rainy2"></i> wi-rainy2</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-windy2"></i> wi-windy2</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-windy3"></i> wi-windy3</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-snowy"></i> wi-snowy</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-snowy2"></i> wi-snowy2</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-snowy3"></i> wi-snowy3</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-weather4"></i> wi-weather4</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-cloudy2"></i> wi-cloudy2</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-cloud3"></i> wi-cloud3</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-lightning3"></i> wi-lightning3</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-sun3"></i> wi-sun3</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-moon2"></i> wi-moon2</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-cloudy3"></i> wi-cloudy3</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-cloud4"></i> wi-cloud4</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-cloud5"></i> wi-cloud5</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-lightning4"></i> wi-lightning4</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-rainy3"></i> wi-rainy3</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-rainy4"></i> wi-rainy4</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-windy4"></i> wi-windy4</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-windy5"></i> wi-windy5</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-snowy4"></i> wi-snowy4</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-snowy5"></i> wi-snowy5</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-weather5"></i> wi-weather5</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-cloudy4"></i> wi-cloudy4</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-lightning5"></i> wi-lightning5</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-thermometer"></i> wi-thermometer</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-compass"></i> wi-compass</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-none"></i> wi-none</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-celsius"></i> wi-celsius</div>
        <div class="demo-icon-hover mb-sm mt-sm col-md-6 col-lg-4 col-xl-3"><i class="wi-fahrenheit"></i> wi-fahrenheit</span>
        </div>
</section>
<!-- end: page -->
@stop