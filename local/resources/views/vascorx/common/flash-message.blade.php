<div class="flash-message">
  @foreach (['danger', 'warning', 'success', 'info'] as $msg)
    @if(Session::has('alert-' . $msg))
    <p class="alert alert-{{ $msg }} alert-dismissable">
        <button class='close' type='button' data-dismiss="alert" aria-hidden="true">
            &times;
        </button>
        {{ Session::get('alert-' . $msg) }}
    </p>
    @endif
  @endforeach
</div>