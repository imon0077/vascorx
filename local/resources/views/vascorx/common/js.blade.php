<!-- Vendor -->
<script src="{{ asset('assets/vendor/jquery/jquery.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-browser-mobile/jquery.browser.mobile.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.js') }}"></script>
<script src="{{ asset('assets/vendor/nanoscroller/nanoscroller.js') }}"></script>
<script src="{{ asset('assets/vendor/bootstrap-datepicker/js/bootstrap-datepicker.js') }}"></script>
<script src="{{ asset('assets/vendor/magnific-popup/magnific-popup.js') }}"></script>
<script src="{{ asset('assets/vendor/jquery-placeholder/jquery.placeholder.js') }}"></script>
<script src="{{ asset('assets/vendor/intercooler/intercooler-0.4.8.js') }}"></script>
<script>
    $(document).ready(function(){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
    })
</script>

<!-- Specific Page Vendor -->
<?php if(Request::segment(1) == 'extra-ajax-made-easy'){?>
    <script src="{{ asset('assets/vendor/jquery-mockjax/jquery.mockjax.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'create-patient' or Request::segment(1) == 'create-order' or Request::segment(1) == 'CreateDoctors' or Request::segment(1) == 'dashboard' or Request::segment(1) == 'edit_order'){
    ?>
    <script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-appear/jquery.appear.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-maskedinput/jquery.maskedinput.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-colorpicker/js/bootstrap-colorpicker.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-timepicker/js/bootstrap-timepicker.js') }}"></script>
    <script src="{{ asset('assets/vendor/fuelux/js/spinner.js') }}"></script>
    <script src="{{ asset('assets/vendor/dropzone/dropzone.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-markdown/js/markdown.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-markdown/js/to-markdown.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-markdown/js/bootstrap-markdown.js') }}"></script>
    <script src="{{ asset('assets/vendor/codemirror/lib/codemirror.js') }}"></script>
    <script src="{{ asset('assets/vendor/codemirror/addon/selection/active-line.js') }}"></script>
    <script src="{{ asset('assets/vendor/codemirror/addon/edit/matchbrackets.js') }}"></script>
    <script src="{{ asset('assets/vendor/codemirror/mode/javascript/javascript.js') }}"></script>
    <script src="{{ asset('assets/vendor/codemirror/mode/xml/xml.js') }}"></script>
    <script src="{{ asset('assets/vendor/codemirror/mode/htmlmixed/htmlmixed.js') }}"></script>
    <script src="{{ asset('assets/vendor/codemirror/mode/css/css.js') }}"></script>
    <script src="{{ asset('assets/vendor/summernote/summernote.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-maxlength/bootstrap-maxlength.js') }}"></script>
    <script src="{{ asset('assets/vendor/ios7-switch/ios7-switch.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-confirmation/bootstrap-confirmation.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-autosize/jquery.autosize.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'forms-basic'){
?>
    <script src="{{ asset('assets/vendor/jquery-autosize/jquery.autosize.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-fileupload/bootstrap-fileupload.min.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'patientlist' or Request::segment(1) == 'orderlist'or Request::segment(1) == 'Doctors'  or Request::segment(1) == 'newDoctors'){
    ?>
    <script src="{{ asset('assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>
    <script src="{{ asset('assets/javascripts/tables/examples.datatables.default.js') }}"></script>
<!--    <script src="{{ asset('assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>-->
<!--    <script src="{{ asset('assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>-->
    <script src="{{ asset('assets/javascripts/theme.js') }}"></script>

    <script src="{{ asset('assets/javascripts/tables/examples.datatables.editable.js') }}"></script>

<?php
}
elseif(Request::segment(1) == 'forms-code-editor'){
    ?>
    <script src="{{ asset('assets/vendor/codemirror/lib/codemirror.js') }}"></script>
    <script src="{{ asset('assets/vendor/codemirror/addon/selection/active-line.js') }}"></script>
    <script src="{{ asset('assets/vendor/codemirror/addon/edit/matchbrackets.js') }}"></script>
    <script src="{{ asset('assets/vendor/codemirror/mode/javascript/javascript.js') }}"></script>
    <script src="{{ asset('assets/vendor/codemirror/mode/xml/xml.js') }}"></script>
    <script src="{{ asset('assets/vendor/codemirror/mode/htmlmixed/htmlmixed.js') }}"></script>
    <script src="{{ asset('assets/vendor/codemirror/mode/css/css.js') }}"></script>

<?php
}elseif(Request::segment(1) == 'forms-validation'){
    ?>
    <script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'forms-wizard'){
    ?>
    <script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-wizard/jquery.bootstrap.wizard.js') }}"></script>
    <script src="{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}"></script>

<?php
}elseif(Request::segment(1) == 'mailbox-compose' OR Request::segment(1) == 'mailbox-email' OR Request::segment(1) == 'mailbox-folder'){
    ?>
    <script src="{{ asset('assets/vendor/bootstrap-tagsinput/bootstrap-tagsinput.js') }}"></script>
    <script src="{{ asset('assets/vendor/summernote/summernote.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'contact'){
    ?>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="{{ asset('assets/vendor/gmaps/gmaps.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'contacts'){
    ?>
    <script src="{{ asset('assets/vendor/select2/select2.js') }}"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="{{ asset('assets/javascripts/maps/snazzy.themes.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'maps-vector'){
    ?>
    <script src="{{ asset('assets/vendor/jqvmap/jquery.vmap.js') }}"></script>
    <script src="{{ asset('assets/vendor/jqvmap/data/jquery.vmap.sampledata.js') }}"></script>
    <script src="{{ asset('assets/vendor/jqvmap/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js') }}"></script>
    <script src="{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js') }}"></script>
    <script src="{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js') }}"></script>
    <script src="{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js') }}"></script>
    <script src="{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js') }}"></script>
    <script src="{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-calendar'){
    ?>
    <script src="{{ asset('assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js') }}"></script>
    <script src="{{ asset('assets/vendor/fullcalendar/lib/moment.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/fullcalendar/fullcalendar.js') }}"></script>

<?php
}elseif(Request::segment(1) == 'pages-lock-screen-example'){
    ?>
    <script src="{{ asset('assets/vendor/jquery-idletimer/dist/idle-timer.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-session-timeout'){
    ?>
    <script src="{{ asset('assets/vendor/jquery-idletimer/dist/idle-timer.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-media-gallery'){
    ?>
    <script src="{{ asset('assets/vendor/isotope/jquery.isotope.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-timeline'){
    ?>
    <script src="{{ asset('assets/vendor/jquery-appear/jquery.appear.js') }}"></script>
    <script src="http://maps.google.com/maps/api/js?sensor=false"></script>
    <script src="{{ asset('assets/vendor/gmaps/gmaps.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-user-profile'){
    ?>
    <script src="{{ asset('assets/vendor/jquery-autosize/jquery.autosize.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'tables-advanced'){
    ?>
    <script src="{{ asset('assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatables/extras/TableTools/js/dataTables.tableTools.min.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

<?php
}elseif(Request::segment(1) == 'tables-editable' OR Request::segment(1) == 'tables-ajax' ){
    ?>
    <script src="{{ asset('assets/vendor/select2/select2.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatables/media/js/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-datatables-bs3/assets/js/datatables.js') }}"></script>

<?php
}elseif(Request::segment(1) == 'ui-elements-animations-appear' OR Request::segment(1) == 'ui-elements-animations-hover'){
    ?>
    <script src="{{ asset('assets/vendor/jquery-appear/jquery.appear.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-blocks' OR Request::segment(1) == 'ui-elements-widgets'){
    ?>
    <script src="{{ asset('assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-appear/jquery.appear.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-easypiechart/jquery.easypiechart.js') }}"></script>
    <script src="{{ asset('assets/vendor/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('assets/vendor/flot-tooltip/jquery.flot.tooltip.js') }}"></script>
    <script src="{{ asset('assets/vendor/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('assets/vendor/flot/jquery.flot.categories.js') }}"></script>
    <script src="{{ asset('assets/vendor/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-sparkline/jquery.sparkline.js') }}"></script>
    <script src="{{ asset('assets/vendor/raphael/raphael.js') }}"></script>
    <script src="{{ asset('assets/vendor/morris/morris.js') }}"></script>
    <script src="{{ asset('assets/vendor/owl-carousel/owl.carousel.js') }}"></script>

<?php
}elseif(Request::segment(1) == 'ui-elements-carousels'){
    ?>
    <script src="{{ asset('assets/vendor/owl-carousel/owl.carousel.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-charts'){
    ?>
    <script src="{{ asset('assets/vendor/jquery-appear/jquery.appear.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-easypiechart/jquery.easypiechart.js') }}"></script>
    <script src="{{ asset('assets/vendor/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('assets/vendor/flot-tooltip/jquery.flot.tooltip.js') }}"></script>
    <script src="{{ asset('assets/vendor/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('assets/vendor/flot/jquery.flot.categories.js') }}"></script>
    <script src="{{ asset('assets/vendor/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-sparkline/jquery.sparkline.js') }}"></script>
    <script src="{{ asset('assets/vendor/raphael/raphael.js') }}"></script>
    <script src="{{ asset('assets/vendor/morris/morris.js') }}"></script>
    <script src="{{ asset('assets/vendor/gauge/gauge.js') }}"></script>
    <script src="{{ asset('assets/vendor/snap-svg/snap.svg.js') }}"></script>
    <script src="{{ asset('assets/vendor/liquid-meter/liquid.meter.js') }}"></script>
    <script src="{{ asset('assets/vendor/chartist/chartist.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-lightbox'){
    ?>
    <script src="{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-loading-progress'){
    ?>
    <script src="{{ asset('assets/vendor/nprogress/nprogress.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-modals'){
    ?>
    <script src="{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-nestable'){
    ?>
    <script src="{{ asset('assets/vendor/jquery-nestable/jquery.nestable.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-notifications'){
    ?>
    <script src="{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-portlets'){
    ?>
    <script src="{{ asset('assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js') }}"></script>
    <script src="{{ asset('assets/vendor/store-js/store.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-sliders'){
    ?>
    <script src="{{ asset('assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-tree-view'){
    ?>
    <script src="{{ asset('assets/vendor/jstree/jstree.js') }}"></script>
<?php
}else{
    ?>
    <script src="{{ asset('assets/vendor/jquery-ui/js/jquery-ui-1.10.4.custom.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-ui-touch-punch/jquery.ui.touch-punch.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-appear/jquery.appear.js') }}"></script>
    <script src="{{ asset('assets/vendor/bootstrap-multiselect/bootstrap-multiselect.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-easypiechart/jquery.easypiechart.js') }}"></script>
    <script src="{{ asset('assets/vendor/flot/jquery.flot.js') }}"></script>
    <script src="{{ asset('assets/vendor/flot-tooltip/jquery.flot.tooltip.js') }}"></script>
    <script src="{{ asset('assets/vendor/flot/jquery.flot.pie.js') }}"></script>
    <script src="{{ asset('assets/vendor/flot/jquery.flot.categories.js') }}"></script>
    <script src="{{ asset('assets/vendor/flot/jquery.flot.resize.js') }}"></script>
    <script src="{{ asset('assets/vendor/jquery-sparkline/jquery.sparkline.js') }}"></script>
    <script src="{{ asset('assets/vendor/raphael/raphael.js') }}"></script>
    <script src="{{ asset('assets/vendor/morris/morris.js') }}"></script>
    <script src="{{ asset('assets/vendor/gauge/gauge.js') }}"></script>
    <script src="{{ asset('assets/vendor/snap-svg/snap.svg.js') }}"></script>
    <script src="{{ asset('assets/vendor/liquid-meter/liquid.meter.js') }}"></script>
    <script src="{{ asset('assets/vendor/jqvmap/jquery.vmap.js') }}"></script>
    <script src="{{ asset('assets/vendor/jqvmap/data/jquery.vmap.sampledata.js') }}"></script>
    <script src="{{ asset('assets/vendor/jqvmap/maps/jquery.vmap.world.js') }}"></script>
    <script src="{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.africa.js') }}"></script>
    <script src="{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.asia.js') }}"></script>
    <script src="{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.australia.js') }}"></script>
    <script src="{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.europe.js') }}"></script>
    <script src="{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.north-america.js') }}"></script>
    <script src="{{ asset('assets/vendor/jqvmap/maps/continents/jquery.vmap.south-america.js') }}"></script>
<?php } ?>



<!-- Theme Base, Components and Settings -->
<script src="{{ asset('assets/javascripts/theme.js') }}"></script>

<!-- Theme Custom -->
<script src="{{ asset('assets/javascripts/theme.custom.js') }}"></script>

<!-- Theme Initialization Files -->
<script src="{{ asset('assets/javascripts/theme.init.js') }}"></script>

<!--
        Examples - Actually there's just ajax mockups in the file below
        All mockups are for demo purposes only. You don't need to include this in your application.
    -->

<?php if(Request::segment(1) == 'extra-ajax-made-easy'){?>
    <script src="{{ asset('assets/javascripts/extra/examples.ajax.made.easy.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'forms-validation'){
?>
    <script src="{{ asset('assets/javascripts/forms/examples.validation.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'forms-wizard'){
?>
    <script src="{{ asset('assets/javascripts/forms/examples.wizard.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'create-patient'){
?>
    <script src="{{ asset('assets/javascripts/forms/examples.advanced.form.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'contact'){
?>
    <script src="{{ asset('assets/javascripts/maps/examples.gmap.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'contact'){
?>
    <script src="{{ asset('assets/javascripts/maps/examples.map.builder.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'maps-vector'){
?>
    <script src="{{ asset('assets/javascripts/maps/examples.vector.maps.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-calendar'){
?>
    <script src="{{ asset('assets/javascripts/pages/examples.calendar.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-lock-screen-example'){
?>
    <script src="{{ asset('assets/javascripts/pages/examples.lockscreen.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-session-timeout'){
?>
    <script src="{{ asset('assets/javascripts/pages/examples.lockscreen.js') }}"></script>
    <script src="{{ asset('assets/javascripts/pages/examples.session.timeout.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-media-gallery'){
?>
    <script src="{{ asset('assets/javascripts/pages/examples.mediagallery.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'pages-timeline'){
?>
    <script src="{{ asset('assets/javascripts/pages/examples.timeline.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'tables-advanced'){
?>
    <script src="{{ asset('assets/javascripts/tables/examples.datatables.default.js') }}"></script>
    <script src="{{ asset('assets/javascripts/tables/examples.datatables.row.with.details.js') }}"></script>
    <script src="{{ asset('assets/javascripts/tables/examples.datatables.tabletools.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'tables-editable'){
?>
    <script src="{{ asset('assets/javascripts/tables/examples.datatables.editable.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'tables-ajax'){
?>
    <script src="{{ asset('assets/javascripts/tables/examples.datatables.ajax.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-blocks' OR Request::segment(1) == 'ui-elements-widgets'){
?>
    <script src="{{ asset('assets/javascripts/ui-elements/examples.widgets.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-charts'){
?>
    <style>
        #ChartistCSSAnimation .ct-series.ct-series-a .ct-line {
            fill: none;
            stroke-width: 4px;
            stroke-dasharray: 5px;
            -webkit-animation: dashoffset 1s linear infinite;
            -moz-animation: dashoffset 1s linear infinite;
            animation: dashoffset 1s linear infinite;
        }

        #ChartistCSSAnimation .ct-series.ct-series-b .ct-point {
            -webkit-animation: bouncing-stroke 0.5s ease infinite;
            -moz-animation: bouncing-stroke 0.5s ease infinite;
            animation: bouncing-stroke 0.5s ease infinite;
        }

        #ChartistCSSAnimation .ct-series.ct-series-b .ct-line {
            fill: none;
            stroke-width: 3px;
        }

        #ChartistCSSAnimation .ct-series.ct-series-c .ct-point {
            -webkit-animation: exploding-stroke 1s ease-out infinite;
            -moz-animation: exploding-stroke 1s ease-out infinite;
            animation: exploding-stroke 1s ease-out infinite;
        }

        #ChartistCSSAnimation .ct-series.ct-series-c .ct-line {
            fill: none;
            stroke-width: 2px;
            stroke-dasharray: 40px 3px;
        }

        @-webkit-keyframes dashoffset {
            0% {
                stroke-dashoffset: 0px;
            }

            100% {
                stroke-dashoffset: -20px;
            };
        }

        @-moz-keyframes dashoffset {
            0% {
                stroke-dashoffset: 0px;
            }

            100% {
                stroke-dashoffset: -20px;
            };
        }

        @keyframes dashoffset {
            0% {
                stroke-dashoffset: 0px;
            }

            100% {
                stroke-dashoffset: -20px;
            };
        }

        @-webkit-keyframes bouncing-stroke {
            0% {
                stroke-width: 5px;
            }

            50% {
                stroke-width: 10px;
            }

            100% {
                stroke-width: 5px;
            };
        }

        @-moz-keyframes bouncing-stroke {
            0% {
                stroke-width: 5px;
            }

            50% {
                stroke-width: 10px;
            }

            100% {
                stroke-width: 5px;
            };
        }

        @keyframes bouncing-stroke {
            0% {
                stroke-width: 5px;
            }

            50% {
                stroke-width: 10px;
            }

            100% {
                stroke-width: 5px;
            };
        }

        @-webkit-keyframes exploding-stroke {
            0% {
                stroke-width: 2px;
                opacity: 1;
            }

            100% {
                stroke-width: 20px;
                opacity: 0;
            };
        }

        @-moz-keyframes exploding-stroke {
            0% {
                stroke-width: 2px;
                opacity: 1;
            }

            100% {
                stroke-width: 20px;
                opacity: 0;
            };
        }

        @keyframes exploding-stroke {
            0% {
                stroke-width: 2px;
                opacity: 1;
            }

            100% {
                stroke-width: 20px;
                opacity: 0;
            };
        }
    </style>
    <script src="{{ asset('assets/javascripts/ui-elements/examples.charts.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-grid-system'){
?>
    <style>
        .show-grid-block {
            background-color: #EEE;
            border: 1px solid #FFF;
            display: block;
            line-height: 40px;
            min-height: 40px;
            text-align: center;
        }

        html.dark .show-grid-block {
            background-color: #282d36;
            border-color: #282d36;
        }
    </style>
<?php
}elseif(Request::segment(1) == 'ui-elements-icons-elusive' OR Request::segment(1) == 'ui-elements-icons-font-awesome' OR Request::segment(1) == 'ui-elements-icons-glyphicons' OR Request::segment(1) == 'ui-elements-icons-line-icons' OR Request::segment(1) == 'ui-elements-icons-meteocons'){
?>
    <style>
        .icons-demo-page .demo-icon-hover {
            cursor: pointer;
            font-size: 15px;
        }

        .icons-demo-page .demo-icon-hover:hover {
            color: #111;
        }

        .icons-demo-page .demo-icon-hover i {
            min-width: 40px;
            padding-right: 15px;
        }

        html.dark .icons-demo-page .demo-icon-hover:hover {
            color: #FEFEFE;
        }
    </style>
<?php
}elseif(Request::segment(1) == 'ui-elements-lightbox'){
?>
    <script src="{{ asset('assets/javascripts/ui-elements/examples.lightbox.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-loading-overlay'){
?>
    <script src="{{ asset('assets/javascripts/ui-elements/examples.loading.overlay.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-loading-progress'){
?>
    <script src="{{ asset('assets/javascripts/ui-elements/examples.loading.progress.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-modals'){
?>
    <script src="{{ asset('assets/javascripts/ui-elements/examples.modals.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-nestable'){
?>
    <script src="{{ asset('assets/javascripts/ui-elements/examples.nestable.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-notifications'){
?>
    <script src="{{ asset('assets/javascripts/ui-elements/examples.notifications.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-portlets'){
?>
    <script src="{{ asset('assets/javascripts/ui-elements/examples.portlets.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-tree-view'){
?>
    <script src="{{ asset('assets/javascripts/ui-elements/examples.treeview.js') }}"></script>
<?php
}elseif(Request::segment(1) == 'ui-elements-sliders'){
?>
    <script>
        (function() {
            $('#listenSlider').change(function() {
                $('.output b').text( this.value );
            });

            $('#listenSlider2').change(function() {
                var min = parseInt(this.value.split('/')[0], 10);
                var max = parseInt(this.value.split('/')[1], 10);

                $('.output2 b.min').text( min );
                $('.output2 b.max').text( max );
            });
        })();
    </script>
<?php
}else{
?>
    <!--script src="{{ asset('assets/javascripts/dashboard/examples.dashboard.js') }}"></script-->
<?php } ?>
@if(Request::segment(1)=='editprofile' OR Request::segment(1)=='login' OR Request::segment(1)=='orderDetails' OR Request::segment(1)=='edit_patient' OR Request::segment(1)=='editdoctor' OR Request::segment(1)=='' OR Request::segment(1)=='signup')
<script src="{{ asset('assets/vendor/jquery-validation/jquery.validate.js') }}"></script>
@endif
@if(Request::segment(1)=='dashboard')
<script src="{{ asset('assets/vendor/pnotify/pnotify.custom.js') }}"></script>
@endif
