
<header class="header">
    <div class="logo-container">
        <a href="../" class="logo">
            <img src="{{asset('assets/images/logo.png')}}" height="35" alt="VascoRx" />
        </a>
        <div class="visible-xs toggle-sidebar-left" data-toggle-class="sidebar-left-opened" data-target="html" data-fire-event="sidebar-left-opened">
            <i class="fa fa-bars" aria-label="Toggle sidebar"></i>
        </div>
    </div>

    <!-- start: search & user box -->
    <div class="header-right load_nt">

        <span class="separator"></span>
        <?php $notifications = Helpers::notification() ?>
        <ul class="notifications">

            @if(Auth::user()->hasRole('admin'))
            <li>
                <a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                    <i class="fa fa-tasks"></i>
                    <span class="badge">{!! count($notifications) !!}</span>
                </a>

                <div class="dropdown-menu notification-menu large">
                    <div class="notification-title">
                        <span class="pull-right label label-default">{!! count($notifications) !!}</span>
                        Notifications
                    </div>

                    <div class="content">
                        <ul>
                            @foreach($notifications as $notification)
                            <li>
                                <p class="clearfix mb-xs">
                                    <span class="title">{!! $notification->first_name !!} {!! $notification->last_name !!}  {!! $notification->order_name !!}</span>
                                    <!--<span class="message">{!! $notification->status !!}</span>-->
                                </p>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

            </li>
            @endif


            @if(!Auth::user()->hasRole('admin'))
            <li>
                <a href="#" class="dropdown-toggle notification-icon" data-toggle="dropdown">
                    <i class="fa fa-tasks"></i>
                    <span class="badge">{!! count($notifications) !!}</span>
                </a>

                <div class="dropdown-menu notification-menu large">
                    <div class="notification-title">
                        <span class="pull-right label label-default">{!! count($notifications) !!}</span>
                        Notifications
                    </div>

                    <div class="content">
                        <ul class="nt-ul">
                            @foreach($notifications as $notification)
                            <li class="notification-bar">
                                <p class="clearfix mb-xs hvr-fade" style="padding: 5px; width: 100%;">

                                    <a href="#" onclick="clear_notification({!! $notification->id !!})" class="panel-action-dismiss text-right nt_hide"></a>
                                    <a href="{!!url()!!}/orderDetails/{!! $notification->id !!}">
                                        <span class="title">{!! $notification->first_name !!} {!! $notification->last_name !!} - {!! $notification->status !!} </span>
                                        <span class="message">{!! $notification->order_name !!}</span>
                                    </a>


                                </p>

                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>

            </li>
            @endif


        </ul>

        <span class="separator"></span>

        <div id="userbox" class="userbox">
            <a href="#" data-toggle="dropdown">
                <figure class="profile-picture">
                    <img src="{{asset('assets/images/!logged-user.jpg')}}" alt="Joseph Doe" class="img-circle" data-lock-picture="assets/images/!logged-user.jpg" />
                </figure>
                <div class="profile-info" data-lock-name="John Doe" data-lock-email="johndoe@okler.com">
            <span class="name">
                {!! Auth::user()->username !!}
            </span>
            <span class="role">
                {!! Auth::user()->user_role !!}
            </span>
                </div>

                <i class="fa custom-caret"></i>
            </a>

            <div class="dropdown-menu dropdown-menu-logout">
                <ul class="list-unstyled">
                    <li class="divider"></li>
                    <!--            <li>-->
                    <!--                <a role="menuitem" tabindex="-1" href="pages-user-profile.html"><i class="fa fa-user"></i> My Profile</a>-->
                    <!--            </li>-->
                    <!--            <li>-->
                    <!--                <a role="menuitem" tabindex="-1" href="#" data-lock-screen="true"><i class="fa fa-lock"></i> Lock Screen</a>-->
                    <!--            </li>-->
                    <li>
                        <a role="menuitem" tabindex="-1" href="{{ url('/auth/logout') }}"><i class="fa fa-power-off"></i> Logout</a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
    <!-- end: search & user box -->
</header>