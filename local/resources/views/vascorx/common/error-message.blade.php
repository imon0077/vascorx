@if ($errors->has())
    <div class='alert alert-danger alert-dismissable'>
        <button class='close' type='button' data-dismiss="alert" aria-hidden="true">
            &times;
        </button>
        <ul>
	        @foreach ($errors->all() as $error)
	            <li>{{ $error }}</li>
	        @endforeach
	    </ul>
    </div>
@endif
