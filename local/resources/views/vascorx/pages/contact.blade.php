@extends('vascorx.layout.master')

@section('content')
<header class="page-header">
    <h2>Contact Details</h2>

</header>
@include('flash::message')
@include('vascorx.common.error-message')
<!-- start: page -->
<div class="row">
    <div class="col-md-12">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>

                </div>

                <h2 class="panel-title">Map</h2>
            </header>

                                <div class="panel-body">

                                    <div id="gmap-basic" style="height: 300px; width: 100%;"></div>
                                </div>


        </section>
    </div>
</div>


<div class="row">
    <div class="col-md-6">


      <!--  <section class="panel panel-featured-left panel-featured-primary">
            <div class="panel-body">
                <div class="widget-summary widget-summary-xs">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-primary">
                            <i class="fa fa-life-ring"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title">Support Questions</h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class="panel panel-featured-left panel-featured-primary">
            <div class="panel-body">
                <div class="widget-summary widget-summary-xs">
                    <div class="widget-summary-col widget-summary-col-icon">
                        <div class="summary-icon bg-primary">
                            <i class="fa fa-life-ring"></i>
                        </div>
                    </div>
                    <div class="widget-summary-col">
                        <div class="summary">
                            <h4 class="title">Support Questions</h4>
                        </div>
                    </div>
                </div>
            </div>
        </section>-->


        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>

                </div>

                <h2 class="panel-title">Contact Info</h2>
            </header>





            <div class="panel-body">
                <div class="dt-sc-appointment">
                <p class="dt-sc-contact-info address">
                    <i class="fa fa-home"></i>
                    <span>4045 E.Bell Rd, Suite 163 Phoenix,AZ 85032</span>
                </p>
                <p class="dt-sc-contact-info">
                    <i class="fa fa-phone"></i>
                    Toll Free : <span>877-971-3001</span>
                </p>
                <p class="dt-sc-contact-info">
                    <i class="fa fa-phone"></i>
                    Local : <span>602-971-6950</span>
                </p>
                <p class="dt-sc-contact-info">
                    <i class="fa fa-file-text"></i>
                    Fax : <span>602-404-2504</span>
                </p>
                <p class="dt-sc-contact-info">
                    <i class="fa fa-envelope-o"></i>
                    Email : <a href="mailto:admin@vascorx.com">admin@vascorx.com</a>
                </p>

                 </div>
            </div>


        </section>
    </div>

    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>

                </div>

                <h2 class="panel-title">Business Hours</h2>
            </header>

            <div class="panel-body">
                <div class="dt-sc-appointment">
                    <ul class='dt-sc-fancy-list  grey  time'>
                        <li><strong>Mon</strong> 7:30AM &#8211; 5:30PM</li>
                        <li><strong> Tue </strong> 7:30AM &#8211; 5:30PM</li>
                        <li><strong> Wed </strong> 7:30AM &#8211; 5:30PM</li>
                        <li><strong> Thu</strong> 7:30AM &#8211; 5:30PM</li>
                        <li><strong>Fri </strong> 7:30AM &#8211; 5:30PM</li>
                        <li><strong>Sat</strong> 9:00AM &#8211; 1:00PM</li>
                    </ul>

                </div>
            </div>


        </section>
    </div>


    <div class="col-md-6">
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>

                </div>

                <h2 class="panel-title">Instant Message VascoRX Securely</h2>
            </header>

            <div class="panel-body">
                <div class="dt-sc-appointment">
                    <a href="https://www.jabze.com/org/signin" class="btn btn-danger" target="_blank">Click here to chat</a>
                </div>
            </div>


        </section>
    </div>



</div>

<!-- end: page -->
@stop

