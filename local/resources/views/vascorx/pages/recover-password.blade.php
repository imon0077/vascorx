@extends('vascorx.layout.master-nobar')



@section('content')
<!-- start: page -->
<section class="body-sign">
    <div class="center-sign">
        <a href="../" class="logo">
            <img src="{{asset('assets/images/logo.png')}}" height="35" alt="VascoRx" />
        </a>

        <div class="panel panel-sign">
            <div class="panel-title-sign mt-xl text-right">
                <h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Recover Password</h2>
            </div>
            <div class="panel-body">
                @include('vascorx.common.error-message')
                <div class="alert alert-info">
                    <p class="m-none text-semibold h6">Enter your e-mail below and we will send you reset instructions!</p>
                </div>

                <form method="post" action="password/email">
                    <div class="form-group mb-none">
                        <div class="input-group">
                            <input name="email" type="email" placeholder="E-mail" class="form-control input-lg" />
                            <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
									<span class="input-group-btn">
										<button class="btn btn-primary btn-lg" type="submit">Reset!</button>
									</span>
                        </div>
                    </div>

                    <p class="text-center mt-lg">Remembered? <a href="login">Sign In!</a>
                </form>
            </div>
        </div>

        <p class="text-center text-muted mt-md mb-md">&copy; Copyright 2014. All Rights Reserved.</p>
    </div>
</section>
<!-- end: page -->

@stop