@extends('vascorx.layout.master')

@section('content')
<header class="page-header">
    <h2>Order list</h2>

</header>
@include('flash::message')
@include('vascorx.common.error-message')
<!-- start: page -->
<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-transparent">
            <div class="panel-body">
                <section class="panel panel-group">
                    <div id="accordion">
                        <div class="panel panel-accordion panel-accordion-first">

                            <div id="collapse1One" class="accordion-body collapse in">

                                <!-- -->
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped mb-none" id="datatable-default" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
                                        <thead>
                                        <tr>
                                            <th style="display: none">testing column(dont remove)</th>

                                            <th>Patient Name</th>
                                            <th>Order ID</th>
                                            <th>Doctor Name</th>
                                            <th>Patient Birth Date</th>
                                            <th>Order Date</th>
                                            <th>Return Date</th>
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($allOrders as $OrderLists)

                                        <tr class="gradeX">
                                            <td style="display: none">testing column(dont remove)</td>

                                            <td>
                                                @if($OrderLists->patient_id == 0)
                                                    @if(!Auth::user()->hasRole('admin'))
                                                        <a href="orderUpdate/{!! $OrderLists->id !!}" class="btn btn-warning text-xs btn-xs">Assign Patient</a>
                                                    @endif
                                                @else
                                                    {!! $OrderLists->first_name !!} {!! $OrderLists->last_name !!}
                                                @endif
                                            </td>
                                            <td>{!! $OrderLists->id !!}</td>
                                            <td>{!! $OrderLists->dfname !!} {!! $OrderLists->dlname !!} </td>
                                            <td>
                                                @if($OrderLists->patient_id == 0)

                                                @else
                                                {!! date('m-d-Y', $OrderLists->date_of_birth) !!}</td>
                                                @endif
                                            <td>{!! $OrderLists->created_at !!}</td>
                                            <td>
                                                @if($OrderLists->patient_id == 0)

                                                @else
                                                {!! $OrderLists->updated_at !!}
                                                @endif
                                            </td>

                                            <td class="hidden-phone">
                                                @if( $OrderLists->status == '' )
                                                <span class="text-sm text-danger">Status Pending</span>
                                                @else
                                                {!! $OrderLists->status !!}
                                                @endif
                                            </td>
                                            <!--<td class="hidden-phone">
                                                @if(Auth::user()->hasRole('admin'))
                                                    @if( $OrderLists->status == '' )
                                                        <span class="btn-info text-sm btn-xs">Status Pending</span>
                                                    @elseif($OrderLists->status != "Order Completed" && $OrderLists->status != "Order Transferred" )
                                                        {!! Form::open(array('action'=>'AdminsController@update', 'class'=>'form-bordered')); !!}
                                                        {!! Form::hidden('order_id', $OrderLists->id) !!} -->


                                <!--{!!  Form::select('status', ['draft' => 'Draft', 'live' => 'Live'], null, ['class' => 'form-control']) !!}-->
                            <!--   {!! Form::select('statusList', array('default' => 'Please Select') + $statusLists, null, array('class' => 'form-control pull-right input-sm col-lg-5 col-md-5', 'name' => 'status'),'@if($OrderLists->status == $statusLists->status)$statusLists->status @endif') !!}-->
                                <!-- {!! Form::select('size', array('L' => 'Large', 'S' => 'Small'), 'S') !!} -->



                                               <!-- {!! Form::select('Status', $statusLists, Input::old("status")) !!}
                                                        {!! Form::submit('Update', array('class' => 'btn btn-primary')) !!}
                                                        {!! Form::close() !!}

                                                    @else
                                                        {!! $OrderLists->status !!}
                                                    @endif
                                                @else
                                                    @if( $OrderLists->status == '' )
                                                        <span class="btn-info text-sm btn-xs">Status Pending</span>
                                                    @else
                                                        {!! $OrderLists->status !!}
                                                    @endif
                                                @endif
                                            </td>-->
                                            <td class="actions">
                                                <!--<a href="orderDetails/{!! $OrderLists->id !!}" class="btn btn-warning text-xs btn-xs">View Details</a>-->
                                                <a href="orderDetails/{!! $OrderLists->id !!}">
                                                    <span class="btn btn-xs btn btn-success text-xs">
                                                    <i class="fa fa-list"></i> View Details
                                                    </span>
                                                </a>
                                                @if(!Auth::user()->hasRole('admin'))
                                                    @if( $OrderLists->status != 'Order Completed' AND $OrderLists->status != 'Order Transferred'  AND $OrderLists->status != 'Cancelled')
                                                    <a href="edit_order/{!! $OrderLists->id !!}">
                                                        <span class="btn text-xs btn-xs btn btn-success">
                                                        <i class="fa fa-pencil"></i> Edit Order
                                                        </span>
                                                    </a>
                                                    @endif
                                                @endif
                                            </td>
                                        </tr>

                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </section>
    </div>
</div>
<!-- end: page -->
@stop

@section('javascript')

$('.showField').click(function(){
$('.field').toggle().removeClass('hide');
$('.patientSelect').toggleClass('hide');
$(this).html($(this).html() == 'Create New Patient' ? 'Cancel' : 'Create New Patient');
})


$('.showFile').click(function(){
$('.create-order').toggle().removeClass('hide');
$('.orderSelect').toggleClass('hide');
$(this).html($(this).html() == 'Create New Order' ? 'Cancel' : 'Create New Order');
})

@stop