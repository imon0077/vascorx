@extends('vascorx.layout.master-nobar')

@section('content')
<!-- start: page -->
<section class="body-sign">
    <div class="center-sign">
        <a href="/" class="logo pull-left">
            <img src="assets/images/logo.png" height="54" alt="Porto Admin" />
        </a>

        <div class="panel panel-sign">
            <div class="panel-title-sign mt-xl text-right">
                <h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Sign Up</h2>
            </div>
            <div class="panel-body">

                @include('flash::message')
                @include('vascorx.common.error-message')

                {!! Form::open(['url' => 'signup', 'class' => 'form-horizontal form-bordered','id'=>'userSignup']) !!}

                    <div class="form-group mb-none">
                        <div class="row">
                            <div class="col-sm-6 mb-lg">
                                <label>First Name</label>
                                <input name="first_name" type="text" class="form-control input-lg" />
                            </div>
                            <div class="col-sm-6 mb-lg">
                                <label>Last Name</label>
                                <input name="last_name" type="text" class="form-control input-lg" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group mb-none">
                        <div class="row">
                            <div class="col-sm-6 mb-lg">
                                <label>User Name</label>
                                <input name="user_name" type="text" class="form-control input-lg" />
                            </div>
                            <div class="col-sm-6 mb-lg">
                                <label>Phone Number</label>
                                <input name="phn_number" type="text" class="form-control input-lg" />
                            </div>
                        </div>
                    </div>

                    <div class="form-group mb-lg">
                        <label>E-mail Address</label>
                        <input name="email" type="email" class="form-control input-lg" />
                    </div>

                    <div class="form-group mb-none">
                        <div class="row">
                            <div class="col-sm-6 mb-lg">
                                <label>Password</label>
                                <input name="password" type="password" id="password" class="form-control input-lg" />
                            </div>
                            <div class="col-sm-6 mb-lg">
                                <label>Password Confirmation</label>
                                <input name="password_confirmation" type="password" class="form-control input-lg" />
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="checkbox-custom checkbox-default">
                                <input id="AgreeTerms" name="agreeterms" type="checkbox"/>
                                <label for="AgreeTerms">I agree with <a href="#">terms of use</a></label>
                            </div>
                        </div>
                        <div class="col-sm-4 text-right">
                            <button type="submit" class="btn btn-primary hidden-xs">Sign Up</button>
                            <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign Up</button>
                        </div>
                    </div>

                    <!--	<span class="mt-lg mb-lg line-thru text-center text-uppercase">
                           <span>or</span>
                       </span>

              <div class="mb-xs text-center">
                   <a class="btn btn-facebook mb-md ml-xs mr-xs">Connect with <i class="fa fa-facebook"></i></a>
                   <a class="btn btn-twitter mb-md ml-xs mr-xs">Connect with <i class="fa fa-twitter"></i></a>
               </div>-->

                    <p class="text-center">Already have an account? <a href="login">Sign In!</a>

             {!! Form::close() !!}
            </div>
        </div>

        <p class="text-center text-muted mt-md mb-md">&copy; Copyright 2014. All Rights Reserved.</p>
    </div>
</section>
<!-- end: page -->
@stop

@section('javascript')

$("#userSignup").validate({
highlight: function( label ) {
$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
},
success: function( label ) {
$(label).closest('.form-group').removeClass('has-error');
label.remove();
},
rules: {

first_name: {
required: true
},
last_name: {
required: true
},
user_name: {
required: true
},
phn_number: {
number: true
},
email: {
required: true,
email:true
},
password: {
required: true,
rangelength: [6, 32]
},
password_confirmation: {
required: true,
equalTo: "#password"
},
agreeterms: {
required: true
}
},
messages:{
first_name: {
required:"Dont forget to input your First Name."
},
last_name: {
required:"Dont forget to input your Last Name."
},
user_name: {
required:"Dont forget to input your User Name."
},
email: {
required:"Please input your Email address.",
email:"Your Email does not seems to valid"
},
password: {
required:"Password field could not remain blank."

},
password_confirmation: {
required:"Password Confirmation field is required.",
equalTo:"Password does not matched"
},
agreeterms: {
required:"Please read & check our Agreements"
}
}
});

$('input[name="first_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});
$('input[name="last_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});




@stop