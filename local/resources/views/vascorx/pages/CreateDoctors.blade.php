@extends('vascorx.layout.master')

@section('content')
<header class="page-header">
    <h2>Create Doctors</h2>
</header>

@include('flash::message')
@include('vascorx.common.error-message')

<!-- start: page -->
<div class="row">
<div class="col-xs-12">
<section class="panel">

<div class="panel-body">

    {!! Form::open(['url' => 'CreateDoctorsSave', 'class' => 'form-horizontal form-bordered','id' => 'createDoc' ] ) !!}

    <div class="form-group field">
        <label class="col-md-2 control-label" for="">First Name</label>
        <div class="col-md-3">
            <input class="form-control" name='first_name'  id="first_name" type="text"  >
        </div>

        <label class="col-md-3 control-label" for="">Last Name</label>
        <div class="col-md-3">
            <input class="form-control" name='last_name' id="last_name" type="text"  >
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-2 control-label" for="">User Name</label>
        <div class="col-md-3">
            <input class="form-control" name='user_name' id="user_name" type="text" >
        </div>

        <label class="col-md-3 control-label" for="">Phone Number</label>
        <div class="col-md-3">
            <input class="form-control" name='phn_number' id="phn_number" type="text" >
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-2 control-label" for="">Password</label>
        <div class="col-md-3">
            <input class="form-control" name='password' id="password" type="password" >
        </div>

        <label class="col-md-3 control-label" for="">Password Confirmation</label>
        <div class="col-md-3">
            <input class="form-control" name='password_confirmation' id="password_confirmation" type="password" >
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-2 control-label" for="">Email Address</label>
        <div class="col-md-3">
            <input class="form-control" name='email' id="email" type="email" required >
        </div>
        <label class="col-md-3 control-label" for="inputDefault">Contact Name</label>
        <div class="col-md-3">
            <input class="form-control" name='contact_name' id="contact_name" type="text" >
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-2 control-label" for="inputDefault">NPI Number</label>
        <div class="col-md-3">
            <input class="form-control" name='npi_number' id="npi_number" type="text">
        </div>

        <label class="col-md-3 control-label" for="inputDefault">DEA Number</label>
        <div class="col-md-3">
            <input class="form-control" name='dea_number' id="dea_number" type="text">
        </div>
    </div>
    <div class="form-group field">
        <label class="col-md-2 control-label" for="inputDefault">FAX Number</label>
        <div class="col-md-3">
            <input class="form-control" name='fax_number' id="fax_number" type="text">
        </div>

    </div>

    <div class="form-group">
        <label class="col-md-3 control-label">&nbsp;</label>
        <div class="col-md-6">
            <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary">Create doctors</button>

        </div>
    </div>

    <!--  end  -->



     {!! Form::close() !!}
</section>
</div>
</div>

<!-- end: page -->
@stop
@section('javascript')
$(document).ready(function(){

$("#editDoctorProfileSave").validate({
highlight: function( label ) {
$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
},
success: function( label ) {
$(label).closest('.form-group').removeClass('has-error');
label.remove();
},
rules: {
first_name: "required",
last_name: "required",
contact_name: {
required: true
},
phn_number: {
required: true
},
email: {
required: true,
email: true
},
npi_number: {

rangelength: [10, 20]
},
dea_number: {

rangelength: [1, 9]
}
},
messages:{
first_name: {
required:"First Name field could not remain blank"
},
last_name: {
required:"Last Name field could not remain blank"
},
contact_name: {
required:"Contact Name field could not remain blank"
},
email: {
required:"The email is required",
email:"Email does not seems to valid"
},
npi_number: {
rangelength:"The NPI Number should between 10 and 20 characters long."
},
dea_number: {
rangelength:"The DEA Number should between 1 and 9 characters long."
}
}
});

$('input[name="first_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});
$('input[name="last_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});

$('input[name="contact_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});
$('input[name="fax_number"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^0-9,.\ \-\+]/g,'') );
});
$('input[name="phn_number"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^0-9,.\ \-\+]/g,'') );
});
$('input[name="npi_number"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^0-9,.\ \-\+]/g,'') );
});


});
@stop
