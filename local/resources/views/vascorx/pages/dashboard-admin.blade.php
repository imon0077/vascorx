@extends('vascorx.layout.master')

@section('content')
<header class="page-header">
    <h2>Dashboard</h2>
</header>

@include('flash::message')
@include('vascorx.common.error-message')

<!-- start: page -->

<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-transparent">
            <div class="panel-body">
                <section class="panel panel-group">
                    <div id="accordion">
                        <div class="panel panel-accordion panel-accordion-first">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1One">
                                        <i class="fa fa-check"></i> New Orders List
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse1One" class="accordion-body collapse in">
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                                        <thead>
                                        <tr>
                                            <th>Patient Name</th>
                                            <th>Orders</th>
                                            <th>Doctors Name</th>
                                            <th>Status</th>
                                            <th>Memo</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($allOrders as $allOrder)
                                        {!! Form::open(array('action'=>'AdminsController@update', 'class'=>'form-bordered', 'id'=>'update_form'.$allOrder->id)); !!}

                                        <tr>
                                            <td>{!! $allOrder->first_name !!} {!! $allOrder->last_name !!}
                                                {!! Form::hidden('order_id', $allOrder->id) !!}

                                            </td>
                                            <td>{!! $allOrder->order_name !!}</td>
                                            <td>{!! $allOrder->d_fname !!} {!! $allOrder->d_lname !!}</td>

                                            <td style="max-width: 200px">
                                                <select name="status" id="status{!! $allOrder->id !!}" class="form-control pull-right input-sm col-lg-5 col-md-5">
                                                    <option>Please Select</option>
                                                    @foreach($statusLists as $statusList)
                                                    <option value="{!! $statusList->id !!}">{!! $statusList->status !!}</option>
                                                    @endforeach
                                                    <option value="4">Order Completed</option>
                                                </select>
                                            </td>

                                            <td style="max-width: 100px">

                                                    <input type="text" name="memo" value="{{$allOrder->memo}}" class="form-control">

                                            </td>


                                            <td>
                                                <button class="btn btn-primary btn-xs" type="button" id="update_info{!! $allOrder->id !!}" data-id="{!! $allOrder->id !!}" data-firstname="{!! $allOrder->first_name !!}" data-lastname="{!! $allOrder->last_name !!}" data-patientid="{!! $allOrder->patient_id !!}" data-patientdbo="{!! date('m-d-Y', $allOrder->date_of_birth) !!}">Update</button>




                                                <a href="orderDetails/{!! $allOrder->id !!}" class="btn btn-primary btn-xs">View</a>
                                            </td>
                                        </tr>
                                        </form>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </section>
    </div>
</div>
<!-- Button trigger modal -->

<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center">Order Transfer Process</h4>
            </div>
            <div class="modal-body">

                {!! Form::open(array('url'=>'ordertransfer', 'class'=>'form-bordered', 'id'=>'order_transfer_form')); !!}
                <input type="hidden" id="t_order_id" name="t_order_id" />
                <input type="hidden" id="t_patient_id" name="t_patient_id" />
                <div class="form-group">
                    <label for="">Patient Name</label>
                    <input type="text" readonly disabled class="form-control" id="t_patient_name" name="t_patient_name">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Patient DOB</label>
                    <input type="text" readonly disabled class="form-control" id="t_patient_dob"name="t_patient_dob">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Pharmacy Name</label>
                    <input type="text" class="form-control" id="t_pharmacy_name" name="t_pharmacy_name">
                </div>
                <div class="form-group">
                    <label for="">Pharmacy Phone</label>
                    <input type="tel" maxlength="15" class="form-control" id="t_phone" name="t_phone">
                </div>
                <div class="form-group">
                    <label for="">Date of Transfer</label>
                    <input type="text" class="form-control" id="t_date" name="t_date" disabled readonly value="{{date('Y-m-d')}}" >
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Note</label>
                    <textarea name="t_note" id="t_note" class="form-control" rows="5"></textarea>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
                {!! Form::close() !!}
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal Icon -->
<!-- Modal Bootstrap -->


<div class="modal fade" id="modalCancel" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title" id="myModalLabel">Are you sure?</h4>
            </div>
            <div class="modal-body">
                <p>Are you really trying to cancel?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning successCancell">Confirm</button>
                <button type="button" class="btn btn-default closeCancell" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal Bootstrap -->


<div class="modal fade" id="modalComplete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title text-danger text-center" id="myModalLabel">Confirm Order Complete</h4>
            </div>
            <div class="modal-body">
                <p>Are you really trying to Complete this Order?</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-warning successComplete">Confirm</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- end: page -->
@stop
@section('javascript')



@foreach($allOrders as $allOrder)

$('#update_info{!! $allOrder->id !!}').click(function(){
var status = $("#status{!! $allOrder->id !!}").val();

    if(status == 4){
    $('#modalComplete').modal('show');

    $('.successComplete').click(function(){
    $('#update_form{!! $allOrder->id !!}').submit();
    });
}
if(status == 11){
    var conf = confirm('Are you sure to transfer this order ?');
    if(conf == true){
        var dataFirstName = $(this).attr('data-firstname');
        var dataLastName = $(this).attr('data-lastname');
        var dataPatientdbo = $(this).attr('data-patientdbo');
        var dataOrderID = $(this).attr('data-id');
        var dataPatientID = $(this).attr('data-patientid');

        $('#t_patient_name').val(dataFirstName+' '+dataLastName);
        $('#t_patient_id').val(dataPatientID);
        $('#t_patient_dob').val(dataPatientdbo);
        $('#t_order_id').val(dataOrderID);

        $('#myModal').modal('show');
    }
    else{return false;}
    }
    if(status == 12){

        $('#modalCancel').modal('show');

        $('.successCancell').click(function(){
        $('#update_form{!! $allOrder->id !!}').submit();
        });

    }
if(status != 4 && status != 11 && status != 12){
    $('#update_form{!! $allOrder->id !!}').submit();
}

});
@endforeach

$("#order_transfer_form").validate({
highlight: function( label ) {
$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
},
success: function( label ) {
$(label).closest('.form-group').removeClass('has-error');
label.remove();
},
rules: {
t_pharmacy_name: {
required: true
},
t_phone:  {
required: true
}

},
messages:{
t_pharmacy_name: {
required:"Pharmacy name required."
},
t_phone: {
required:"Please enter the Phone."
}

}
});

@stop