@extends('vascorx.layout.master-nobar')

@section('content')
<!-- start: page -->
<section class="body-sign">
    <div class="center-sign">
        <a href="" class="logo pull-left">
            <img src="{{asset('assets/images/logo.png')}}" height="35" alt="VascoRx" />
        </a>

        <div class="panel panel-sign">
            <div class="panel-title-sign mt-xl text-right">
                <h2 class="title text-uppercase text-bold m-none"><i class="fa fa-user mr-xs"></i> Sign In</h2>
            </div>
            <div class="panel-body">
                    
                @include('vascorx.common.error-message')

                {!! Form::open(['url' => '/auth/login','id'=>'userLogin']) !!}
                    <div class="form-group mb-lg">
                        <label>Username</label>
                        <div class="input-group input-group-icon">
                            <input name="username" type="text" class="form-control input-lg" tabindex="1" autofocus="" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-user"></i>
										</span>
									</span>
                        </div>  
                    </div>

                    <div class="form-group mb-lg">
                        <div class="clearfix">
                            <label class="pull-left">Password</label>
                            <a href="recover-password" class="pull-right">Lost Password?</a>
                        </div>
                        <div class="input-group input-group-icon">
                            <input name="password" type="password" class="form-control input-lg" tabindex="2" />
									<span class="input-group-addon">
										<span class="icon icon-lg">
											<i class="fa fa-lock"></i>
										</span>
									</span>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-8">
                            <div class="checkbox-custom checkbox-default">
                                <input id="RememberMe" name="rememberme" type="checkbox"/>
                                <label for="RememberMe">Remember Me</label>
                            </div>
                        </div>
                        <div class="col-sm-4 text-right">
                            <button type="submit" class="btn btn-primary hidden-xs" tabindex="3">Sign In</button>
                            <button type="submit" class="btn btn-primary btn-block btn-lg visible-xs mt-lg">Sign In</button>
                        </div>
                    </div>
    <!--
							<span class="mt-lg mb-lg line-thru text-center text-uppercase">
								<span>or</span>
							</span>

                    <div class="mb-xs text-center">
                        <a class="btn btn-facebook mb-md ml-xs mr-xs">Connect with <i class="fa fa-facebook"></i></a>
                        <a class="btn btn-twitter mb-md ml-xs mr-xs">Connect with <i class="fa fa-twitter"></i></a>
                    </div>
-->
<!--                    <p class="text-center">Don't have an account yet? <a href="signup">Sign Up!</a>-->
                    <p class="text-center">Don't have an account yet? <a class="btn signupReq alert-link">Sign Up!</a></p>
                <p class="text-center"> <a class="btn" style="color: black;" href="https://www.youtube.com/watch?v=d9rbhCjXO20" target="_blank">MD Portal Tutorial Video</a></p>

                {!! Form::close() !!}
            </div>
        </div>

        <p class="text-center text-muted mt-md mb-md">&copy; Copyright 2014. All Rights Reserved.</p>
    </div>

    <!-- Modal Bootstrap -->
    <div class="modal fade" id="modalSignUp" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                    <h4 class="modal-title text-center text-primary" id="myModalLabel">Doctor Sign-Up Request</h4>
                </div>
                <div class="modal-body">
                    <p>To set up an account with VascoRX please email: <a href="mailto:portal@vascorx.com">portal@vascorx.com</a></p>
                </div>
                <div class="modal-footer">

                    <button type="button" class="btn btn-danger" data-dismiss="modal">OK</button>
                </div>
            </div>
        </div>
    </div>
</section>



<!-- end: page -->
@stop

@section('javascript')

$("#userLogin").validate({
highlight: function( label ) {
$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
},
success: function( label ) {
$(label).closest('.form-group').removeClass('has-error');
label.remove();
},
rules: {

username: {
required: true
},
password: {
required: true,
rangelength: [1, 32]
}
},
messages:{
username: {
required:"Dont forget to input your User Name."
},
password: {
required:"Password field could not remain blank.",
rangelength:"Password length should be between 1 to 32"
}
}
});

$('.signupReq').click(function(){
    $('#modalSignUp').modal('show');

});

@stop