@extends('vascorx.layout.master')

@section('content')
<header class="page-header">
    <h2>Doctors List</h2>

</header>

@include('flash::message')
@include('vascorx.common.error-message')

<!-- start: page -->
<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-transparent">
            <div class="panel-body">
                <section class="panel panel-group">
                    <div id="accordion">
                        <div class="panel panel-accordion panel-accordion-first">

                            <div id="collapse1One" class="accordion-body collapse in">

                                <!-- -->
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped mb-none" id="datatable-default" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>User name</th>
                                            <th>Full name</th>
                                            <th>Contact name</th>
                                            <th>Email</th>
                                            <th>Phone</th>
                                            <th>Updated Date</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($Doctors as $Doctors)
                                        <tr class="gradeX">
                                            <td>{!! $Doctors->id !!}</td>
                                            <td>{!! $Doctors->username !!} </td>
                                            <td>{!! $Doctors->first_name !!} {!! $Doctors->last_name !!}</td>
                                            <td>{!! $Doctors->contact_name !!}</td>
                                            <td>{!! $Doctors->email !!}</td>
                                            <td>{!! $Doctors->phone_number !!}</td>
                                            <td>{!! $Doctors->updated_at !!}</td>
                                            <td class="actions">
                                                <a href="editdoctor/{!! $Doctors->id !!}" title="Accept">
                                                    <span class="btn-xs btn btn-success">
                                                    <i class="fa fa-pencil"></i> Edit
                                                    </span>
                                                </a>
                                                <a href="deletedoctor/{!! $Doctors->id !!}" class="on-default remove-row" title="Decline">
                                                    <span class="btn-xs btn btn-danger">
                                                    <i class="fa fa-close"></i> Delete
                                                    </span>
                                                </a>

                                            </td>
                                        </tr>
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </section>
    </div>
</div>
<!-- end: page -->
@stop

@section('javascript')

$('.showField').click(function(){
$('.field').toggle().removeClass('hide');
$('.patientSelect').toggleClass('hide');
$(this).html($(this).html() == 'Create New Patient' ? 'Cancel' : 'Create New Patient');
})


$('.showFile').click(function(){
$('.create-order').toggle().removeClass('hide');
$('.orderSelect').toggleClass('hide');
$(this).html($(this).html() == 'Create New Order' ? 'Cancel' : 'Create New Order');
})

@stop