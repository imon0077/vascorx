@extends('vascorx.layout.master')

@section('content')
<header class="page-header">
    <h2>Create New Patient</h2>
</header>

@include('flash::message')
@include('vascorx.common.error-message')

<!-- start: page -->
<div class="row">
<div class="col-xs-12">
<section class="panel">
<!--header class="panel-heading">
    <div class="panel-actions">
        <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
        <a href="#" class="panel-action panel-action-dismiss" data-panel-dismiss></a>
    </div>

    <h2 class="panel-title">Create New Patient</h2>
</header-->
<div class="panel-body">

    {!! Form::open(['url' => 'create-patient', 'files' => 'true', 'class' => 'form-horizontal form-bordered','id'=>'createPatient']) !!}

        <div class="form-group">
            <label class="col-md-3 control-label" for="inputDefault">First Name</label>
            <div class="col-md-6">
                <input class="form-control" name='first_name' id="first_name" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="inputDefault">Last Name</label>
            <div class="col-md-6">
                <input class="form-control" name='last_name' id="last_name" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Date of Birth</label>
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-4">
                        <input type="text" name="mm" class="form-control" placeholder="MM">
                    </div>
                    <div class="visible-xs mb-md"></div>
                    <div class="col-sm-4">
                        <input type="text" name="dd" class="form-control" placeholder="DD">
                    </div>
                    <div class="visible-xs mb-md"></div>
                    <div class="col-sm-4">
                        <input type="text" name="yyyy" class="form-control" placeholder="YYYY">
                    </div>
                </div>

            </div>
        </div>

        <!--<div class="form-group">
            <label class="col-md-3 control-label">Date of Birth</label>
            <div class="col-md-6">
                <div class="input-group">
                    <span class="input-group-addon">
                        <i class="fa fa-calendar"></i>
                    </span>
                    <input type="text" name='date_of_birth' data-plugin-datepicker class="form-control">
                </div>
            </div>
        </div>-->

<!--        @unless(empty($orderLists))-->
<!--        <div class="form-group orderSelect">-->
<!--            <label class="col-md-3 control-label">Select Order</label>-->
<!--            <div class="col-md-6">-->
<!--                <select data-plugin-selectTwo class="form-control populate" name='order_id'>-->
<!--                    <optgroup label="Orders">-->
<!--                        <option value=""> Select.. </option>-->
<!--                        @foreach($orderLists as $orderList)-->
<!--                            <option value="{{ $orderList->id }}"> {{ $orderList->order_name }}</option>-->
<!--                        @endforeach-->
<!--                    </optgroup>-->
<!--                </select>-->
<!--            </div>-->
<!--        </div>-->
<!--        @endunless-->

        <div class="form-group hide file">
            <label class="col-md-3 control-label">Upload Prescription Doc</label>
            <div class="col-md-6">
                <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                    <div class="input-append">
                        <div class="uneditable-input">
                            <i class="fa fa-file fileupload-exists"></i>
                            <span class="fileupload-preview"></span>
                        </div>
                        <span class="btn btn-default btn-file">
                            <span class="fileupload-exists">Change</span>
                            <span class="fileupload-new">Select file</span>
                            <input type="file" name="prescription_filename" id="prescription_filename">
                        </span>
                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="form-group hide file ">
            <label class="col-md-3 control-label">Upload Notes Doc</label>
            <div class="col-md-6">
                <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                    <div class="input-append">
                        <div class="uneditable-input">
                            <i class="fa fa-file fileupload-exists"></i>
                            <span class="fileupload-preview"></span>
                        </div>
                        <span class="btn btn-default btn-file">
                            <span class="fileupload-exists">Change</span>
                            <span class="fileupload-new">Select file</span>
                            <input type="file" name='note_filename' id="note_filename">
                        </span>
                        <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                    </div>

                </div>
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">&nbsp;</label>
            <div class="col-md-6">
                <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary save">Save Patient</button>
                <button type="button" class="mb-xs mt-xs mr-xs btn btn-warning showFile">Create New Order</button>
            </div>
         </div> 

     {!! Form::close() !!}
</section>
</div>
</div>

<!-- end: page -->
@stop

@section('javascript')

    $('.showFile').click(function(){
        $('.file').toggle().removeClass('hide');
        $('.orderSelect').toggleClass('hide');
        $(this).html($(this).html() == 'Create New Order' ? 'Cancel' : 'Create New Order');
    });

$("#createPatient").validate({
highlight: function( label ) {
$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
},
success: function( label ) {
$(label).closest('.form-group').removeClass('has-error');
label.remove();
},
rules: {
first_name: {
required: true
},
last_name:  {
required: true
},
mm: {
required: true,
number: true,
range: [1, 12],
rangelength: [1, 2]
},
dd: {
required: true,
number: true,
range: [1, 31],
rangelength: [1, 2]
},
yyyy: {
required: true,
number: true,
rangelength: [4, 4]
}

},
messages:{
first_name: {
required:"First name required."
},
last_name: {
required:"Last name required."
},
mm: {
required:"Month Field required.",
number: "Please input a valid number"
},
dd: {
required:"Date field required.",
number: "Please input a valid number"
},
yyyy: {
required:"Year field required.",
number: "Please input a valid number"
}

}
});

$('.save').click(function(){
    var has_class_p = $('#prescription_filename').closest('.form-group').hasClass('hide');
    var has_class_n = $('#note_filename').closest('.form-group').hasClass('hide');
    if (!has_class_p && !has_class_n){
        var uploadf = $('#prescription_filename').val();
        if(uploadf == "" || uploadf == null || uploadf == "undefine"   ){
            $('.error-upload-message').remove();
            $('#prescription_filename').closest('.form-group').addClass('has-error');
            $('#prescription_filename').closest('.input-append').append('<label class="error-upload-message error">The prescription filename field is required.</label>');
            return false;
        }else{
            $('#prescription_filename').closest('.form-group').removeClass('has-error');
            $('.error-upload-message').remove();
        }
        var uploadn = $('#note_filename').val();
        if(uploadn == "" || uploadn == null || uploadn == "undefine"   ){
            $('.error-note-message').remove();
            $('#note_filename').closest('.form-group').addClass('has-error');
            $('#note_filename').closest('.input-append').append('<label class="error-note-message error">The note filename field is required.</label>');
            return false;
        }else{
            $('#note_filename').closest('.form-group').removeClass('has-error');
            $('.error-note-message').remove();
        }
    }
});

$('#prescription_filename').click(function(){
    var uploadf = $('#prescription_filename').val();
    if(uploadf != "" || uploadf != null || uploadf != "undefine"   ){
        $('.error-upload-message').remove();
    }
});

$('#note_filename').click(function(){
    var uploadf = $('#note_filename').val();
    if(uploadf != "" || uploadf != null || uploadf != "undefine"   ){
        $('.error-note-message').remove();
    }
});

$('input[name="first_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});
$('input[name="last_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});

@stop