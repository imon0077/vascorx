@extends('vascorx.layout.master')

@section('content')
<header class="page-header">
    <h2>Edit Patient</h2>
</header>

@include('flash::message')
@include('vascorx.common.error-message')

<!-- start: page -->
<div class="row">
<div class="col-xs-12">
<section class="panel">

<div class="panel-body">

    {!! Form::open(['url' => 'editSave', 'class' => 'form-horizontal form-bordered','id'=>'editPatientSave']) !!}

        <div class="form-group">
            <label class="col-md-3 control-label" for="">First Name</label>
            <div class="col-md-6">
                <input class="form-control" name='id' value="{!! $patients->id !!}" id="" type="hidden">
                <input class="form-control" name='first_name' value="{!! $patients->first_name !!}" id="" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label" for="">Last Name</label>
            <div class="col-md-6">
                <input class="form-control" name='last_name' value="{!! $patients->last_name !!}" id="" type="text">
            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">Date of Birth</label>
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-4">
                        <input type="text" name="mm" value="{!! date('m', $patients->date_of_birth) !!}" class="form-control" placeholder="MM">
                    </div>
                    <div class="visible-xs mb-md"></div>
                    <div class="col-sm-4">
                        <input type="text" name="dd" value="{!! date('d', $patients->date_of_birth) !!}" class="form-control" placeholder="DD">
                    </div>
                    <div class="visible-xs mb-md"></div>
                    <div class="col-sm-4">
                        <input type="text" name="yyyy" value="{!! date('Y', $patients->date_of_birth) !!}" class="form-control" placeholder="YYYY">
                    </div>
                </div>

            </div>
        </div>

        <div class="form-group">
            <label class="col-md-3 control-label">&nbsp;</label>
            <div class="col-md-6">
                <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary">Update Patient</button>

            </div>
         </div> 

     {!! Form::close() !!}
</section>
</div>
</div>

<!-- end: page -->
@stop

@section('javascript')
$(document).ready(function(){


$("#editPatientSave").validate({
highlight: function( label ) {
$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
},
success: function( label ) {
$(label).closest('.form-group').removeClass('has-error');
label.remove();
},
rules: {

first_name:{
required: true
},
last_name:{
required: true
},
mm: {
required: true,
number: true,
range: [1, 12],
rangelength: [1, 2]
},
dd: {
required: true,
number: true,
range: [1, 31],
rangelength: [1, 2]
},
yyyy: {
required: true,
number: true,
max: 3000,
rangelength: [4, 4]
}
},
messages:{
first_name: {
required:"First name required."
},
last_name: {
required:"Last name required."
},
mm: {
required:"Month Field required.",
number: "Please input a valid number"
},
dd: {
required:"Date field required.",
number: "Please input a valid number"
},
yyyy: {
required:"Year field required.",
number: "Please input a valid number"
}
}
});

$('input[name="first_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});
$('input[name="last_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});


});
@stop
