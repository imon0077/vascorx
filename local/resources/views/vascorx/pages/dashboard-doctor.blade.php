@extends('vascorx.layout.master')

@section('content')
<header class="page-header">
    <h2>Dashboard</h2>

<!--    <div class="right-wrapper pull-right">-->
<!--        <ol class="breadcrumbs">-->
<!--            <li>-->
<!--                <a href="index">-->
<!--                    <i class="fa fa-home"></i>-->
<!--                </a>-->
<!--            </li>-->
<!--            <li><span>Dashboard</span></li>-->
<!--        </ol>-->
<!---->
<!--        <a class="sidebar-right-toggle" data-open="sidebar-right"><i class="fa fa-chevron-left"></i></a>-->
<!--    </div>-->
</header>

@include('flash::message')
@include('vascorx.common.error-message')

<!-- start: page -->


<div class="row">
    <div class="col-lg-6">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Create New Order</h2>
            </header>
            <div class="panel-body">

                {!! Form::open(['url' => 'create-order', 'files' => 'true', 'class' => 'form-horizontal form-bordered', 'id'=>'createOrder']) !!}

                <input type="hidden" name='doctor_id' value='{!! Auth::user()->getkey() !!}'>

                <div class="form-group file">
                    <label class="col-md-3 control-label">Upload Prescription</label>
                    <div class="col-md-9">
                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                            <div class="input-append">
                                <div class="uneditable-input" style="padding-right: 15px;">
                                    <i class="fa fa-file fileupload-exists"></i>
                                    <span class="fileupload-preview"></span>
                                </div>
                        <span class="btn btn-default btn-file">
                            <span class="fileupload-exists">Change</span>
                            <span class="fileupload-new">Select file</span>
                            <input type="file" name="prescription_filename" id="prescription_filename">
                        </span>
                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload" style="background-color: #FF3333"> X </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group file">
                    <label class="col-md-3 control-label">Upload Notes</label>
                    <div class="col-md-9">
                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                            <div class="input-append">
                                <div class="uneditable-input" style="padding-right: 15px;">
                                    <i class="fa fa-file fileupload-exists"></i>
                                    <span class="fileupload-preview"></span>
                                </div>
                        <span class="btn btn-default btn-file">
                            <span class="fileupload-exists">Change</span>
                            <span class="fileupload-new">Select file</span>
                            <input type="file" name='note_filename' id="note_filename">
                        </span>
                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload" style="background-color: #FF3333"> X </a>
                            </div>
                        </div>
                    </div>
                </div>

                @unless(empty($patientLists))
                <div class="form-group patientSelect">
                    <label class="col-md-3 control-label">Select Patient</label>
                    <div class="col-md-8">
                        <select data-plugin-selectTwo class="form-control populate" name='patient_id'>
                            <optgroup label="Patients">
                                <option value="">Select..</option>
                                @foreach($patientLists as $patientLists)
                                <option value="{{ $patientLists->id }}"> {{ $patientLists->first_name }} {{ $patientLists->last_name }} {!! date('m-d-Y', $patientLists->date_of_birth) !!}</option>
                                @endforeach
                            </optgroup>
                        </select>
                    </div>
                </div>
                @endunless


                <div class="form-group hide field">
                    <label class="col-md-3 control-label" for="inputDefault">First Name</label>
                    <div class="col-md-6">
                        <input class="form-control" name='first_name' id="first_name" type="text">
                    </div>
                </div>

                <div class="form-group hide field">
                    <label class="col-md-3 control-label" for="inputDefault">Last Name</label>
                    <div class="col-md-6">
                        <input class="form-control" name='last_name' id="last_name" type="text">
                    </div>
                </div>


                <div class="form-group hide field">
                    <label class="col-md-3 control-label">Date of Birth</label>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-4">
                                <input type="text" name="mm" class="form-control" placeholder="MM">
                            </div>
                            <div class="visible-xs mb-md"></div>
                            <div class="col-sm-4">
                                <input type="text" name="dd" class="form-control" placeholder="DD">
                            </div>
                            <div class="visible-xs mb-md"></div>
                            <div class="col-sm-4">
                                <input type="text" name="yyyy" class="form-control" placeholder="YYYY">
                            </div>
                        </div>

                    </div>
                </div>


                <div class="form-group">
                    <label class="col-md-3 control-label">&nbsp;</label>
                    <div class="col-md-9">
                        <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary saveco">Save Order</button>
                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-warning showField">Create New Patient</button>
                    </div>
                </div>

                {!! Form::close() !!}
        </section>
    </div>
    <div class="col-lg-6">
        <section class="panel">
            <header class="panel-heading">
                <h2 class="panel-title">Create New Patient</h2>
            </header>
            <div class="panel-body">

                {!! Form::open(['url' => 'create-patient', 'files' => 'true', 'class' => 'form-horizontal form-bordered', 'id'=>'createPatient']) !!}

                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputDefault">First Name</label>
                    <div class="col-md-6">
                        <input class="form-control" name='first_name' id="first_name" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputDefault">Last Name</label>
                    <div class="col-md-6">
                        <input class="form-control" name='last_name' id="last_name" type="text">
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Date of Birth</label>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-4">
                                <input type="text" name="mm" class="form-control" placeholder="MM">
                            </div>
                            <div class="visible-xs mb-md"></div>
                            <div class="col-sm-4">
                                <input type="text" name="dd" class="form-control" placeholder="DD">
                            </div>
                            <div class="visible-xs mb-md"></div>
                            <div class="col-sm-4">
                                <input type="text" name="yyyy" class="form-control" placeholder="YYYY">
                            </div>
                        </div>

                    </div>
                </div>

<!--                @unless(empty($orderLists))-->
<!--                <div class="form-group orderSelect">-->
<!--                    <label class="col-md-3 control-label">Select Order</label>-->
<!--                    <div class="col-md-6">-->
<!--                        <select data-plugin-selectTwo class="form-control populate" name='order_id'>-->
<!--                            <optgroup label="Orders">-->
<!--                                <option value=""> Select.. </option>-->
<!--                                @foreach($orderLists as $orderList)-->
<!--                                <option value="{{ $orderList->id }}"> {{ $orderList->order_name }}</option>-->
<!--                                @endforeach-->
<!--                            </optgroup>-->
<!--                        </select>-->
<!--                    </div>-->
<!--                </div>-->
<!--                @endunless-->

                <div class="form-group hide create-order file">
                    <label class="col-md-3 control-label">Upload Prescription</label>
                    <div class="col-md-9">
                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                            <div class="input-append">
                                <div class="uneditable-input" style="padding-right: 15px;">
                                    <i class="fa fa-file fileupload-exists"></i>
                                    <span class="fileupload-preview"></span>
                                </div>
                        <span class="btn btn-default btn-file">
                            <span class="fileupload-exists">Change</span>
                            <span class="fileupload-new">Select file</span>
                            <input type="file" name="prescription_filename" id="prescriptions_filename">
                        </span>
                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload" style="background-color: #FF3333"> X </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group hide create-order file">
                    <label class="col-md-3 control-label">Upload Notes</label>
                    <div class="col-md-9">
                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                            <div class="input-append">
                                <div class="uneditable-input" style="padding-right: 15px;">
                                    <i class="fa fa-file fileupload-exists"></i>
                                    <span class="fileupload-preview"></span>
                                </div>
                        <span class="btn btn-default btn-file">
                            <span class="fileupload-exists">Change</span>
                            <span class="fileupload-new">Select file</span>
                            <input type="file" name='note_filename' id="notes_filename">
                        </span>
                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload" style="background-color: #FF3333; border-color:  #FF3333;"> X </a>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">&nbsp;</label>
                    <div class="col-md-9">
                        <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary savecp">Save Patient</button>
                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-warning showFile">Create New Order</button>
                    </div>
                </div>

                {!! Form::close() !!}
        </section>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-transparent">
            <div class="panel-body">
                <section class="panel panel-group">
                    <div id="accordion">
                        <div class="panel panel-accordion panel-accordion-first">
                            <div class="panel-heading">
                                <h4 class="panel-title" id="notification_id">
                                    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapse1One">
                                        <i class="fa fa-check"></i> Doctor Notifications
                                    </a>
                                </h4>
                            </div>
                            <div id="collapse1One" class="accordion-body collapse in">
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped mb-none" id="datatable-editable">
                                        <thead>
                                        <tr>
                                            <th>Patient Name</th>
                                            <th>Order ID</th>
                                            <th>Order No.</th>
                                            <th>Order Date</th>

                                            <th>Return Date</th>
                                            <th>Status</th>
<!--                                            <th>Status</th>-->
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                         @foreach($allOrders as $allOrder)

                                        <tr>
                                            <td>{!! $allOrder->first_name !!} {!! $allOrder->last_name !!}</td>
                                            <td>{!! $allOrder->id !!}</td>
                                            <td><a href="orderDetails/{!! $allOrder->id !!}">{!! $allOrder->order_name !!}</a></td>
                                            <td>{!! $allOrder->created_at !!}</td>
                                            <td>{!! $allOrder->updated_at !!}</td>
                                            <td>{!! $allOrder->status !!}</td>
                                            <td><a class="btn panel-action-dismiss" onclick="clear_notification({!! $allOrder->id !!})"></a></td>
                                        </tr>

                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </section>
    </div>
</div>
<!-- end: page -->
@stop



@section('javascript')

$('.showField').click(function(){
$('.field').toggle().removeClass('hide');
$('.patientSelect').toggleClass('hide');
$(this).html($(this).html() == 'Create New Patient' ? 'Cancel' : 'Create New Patient');
})


$('.showFile').click(function(){
$('.create-order').toggle().removeClass('hide');
$('.orderSelect').toggleClass('hide');
$(this).html($(this).html() == 'Create New Order' ? 'Cancel' : 'Create New Order');
})

$("#createOrder").validate({
highlight: function( label ) {
$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
},
success: function( label ) {
$(label).closest('.form-group').removeClass('has-error');
label.remove();
},
rules: {
patient_id: {
required: true
},
mm: {
required: true,
number: true,
range: [1, 12],
rangelength: [1, 2]
},
dd: {
required: true,
number: true,
range: [1, 31],
rangelength: [1, 2]
},
yyyy: {
required: true,
number: true,
rangelength: [4, 4]
},
first_name:{
required: true
},
last_name:{
required: true
}
},
messages:{
patient_id: {
required:"Dont forget to select a patient; or create a new patient."
},
first_name: {
required:"First name required."
},
last_name: {
required:"Last name required."
},
mm: {
required:"Month Field required.",
number: "Please input a valid number"
},
dd: {
required:"Date field required.",
number: "Please input a valid number"
},
yyyy: {
required:"Year field required.",
number: "Please input a valid number"
}
}
});

$("#createPatient").validate({
highlight: function( label ) {
$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
},
success: function( label ) {
$(label).closest('.form-group').removeClass('has-error');
label.remove();
},
rules: {
first_name: {
required: true
},
last_name:  {
required: true
},
mm: {
required: true,
number: true,
range: [1, 12],
rangelength: [1, 2]
},
dd: {
required: true,
number: true,
range: [1, 31],
rangelength: [1, 2]
},
yyyy: {
required: true,
number: true,
rangelength: [4, 4]
}
},
messages:{
first_name: {
required:"First name required."
},
last_name: {
required:"Last name required."
},
mm: {
required:"Month Field required.",
number: "Please input a valid number"
},
dd: {
required:"Date field required.",
number: "Please input a valid number"
},
yyyy: {
required:"Year field required.",
number: "Please input a valid number"
}
}
});


$('.saveco').click(function(){
<!--var has_class_p = $('#prescription_filename').closest('.form-group').hasClass('hide');-->
<!--var has_class_n = $('#note_filename').closest('.form-group').hasClass('hide');-->
<!--if (!has_class_p && !has_class_n){-->
<!--var uploadf = $('#prescription_filename').val();-->
<!--if(uploadf == "" || uploadf == null || uploadf == "undefine"   ){-->
<!---->
<!--$('.error-upload-message').remove();-->
<!---->
<!--$('#prescription_filename').closest('.form-group').addClass('has-error');-->
<!--$('#prescription_filename').closest('.input-append').append('<label class="error-upload-message error">The prescription filename field is required.</label>');-->
<!--return false;-->
<!--}else{-->
<!--$('#prescription_filename').closest('.form-group').removeClass('has-error');-->
<!--$('.error-upload-message').remove();-->
<!--}-->
<!--var uploadn = $('#note_filename').val();-->
<!--if(uploadn == "" || uploadn == null || uploadn == "undefine"   ){-->
<!--$('.error-note-message').remove();-->
<!--$('#note_filename').closest('.form-group').addClass('has-error');-->
<!--$('#note_filename').closest('.input-append').append('<label class="error-note-message error">The note filename field is required.</label>');-->
<!--return false;-->
<!--}else{-->
<!--$('#note_filename').closest('.form-group').removeClass('has-error');-->
<!--$('.error-note-message').remove();-->
<!--}-->
<!--}-->
});

$('#prescription_filename').click(function(){
var uploadf = $('#prescription_filename').val();
if(uploadf != "" || uploadf != null || uploadf != "undefine"   ){
$('.error-upload-message').remove();
}
});

$('#note_filename').click(function(){
var uploadf = $('#note_filename').val();
if(uploadf != "" || uploadf != null || uploadf != "undefine"   ){
$('.error-note-message').remove();
}
});

$('.savecp').click(function(){
var has_class_p = $('#prescriptions_filename').closest('.form-group').hasClass('hide');
var has_class_n = $('#notes_filename').closest('.form-group').hasClass('hide');
if (!has_class_p && !has_class_n){
var uploadf = $('#prescriptions_filename').val();
if(uploadf == "" || uploadf == null || uploadf == "undefine"   ){

$('.error-upload-message').remove();
$('#prescriptions_filename').closest('.form-group').addClass('has-error');
$('#prescriptions_filename').closest('.input-append').append('<label class="error-upload-message error">The prescription filename field is required.</label>');
return false;
}else{
$('#prescriptions_filename').closest('.form-group').removeClass('has-error');
$('.error-upload-message').remove();
}
var uploadn = $('#notes_filename').val();
if(uploadn == "" || uploadn == null || uploadn == "undefine"   ){
$('.error-note-message').remove();
$('#notes_filename').closest('.form-group').addClass('has-error');
$('#notes_filename').closest('.input-append').append('<label class="error-note-message error">The note filename field is required.</label>');
return false;
}else{
$('#notes_filename').closest('.form-group').removeClass('has-error');
$('.error-note-message').remove();
}
}
});

$('#prescriptions_filename').click(function(){
var uploadf = $('#prescriptions_filename').val();
if(uploadf != "" || uploadf != null || uploadf != "undefine"   ){
$('.error-upload-message').remove();
}
});

$('#notes_filename').click(function(){
var uploadf = $('#notes_filename').val();
if(uploadf != "" || uploadf != null || uploadf != "undefine"   ){
$('.error-note-message').remove();
}
});

$('input[name="first_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});
$('input[name="last_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});

@stop