@extends('vascorx.layout.master')

@section('content')
<header class="page-header">
    <h2>Order Details of @foreach($orderDetails as $order) "<span class="text-danger">{!! ucwords($order->first_name) !!} {!! ucwords($order->last_name) !!}</span>"  @endforeach </h2>

</header>

<!-- start: page -->
<div class="row">
    <div class="col-xs-12">
        @foreach($orderDetails as $order)
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Order Details </h2>
            </header>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputDefault">Patient Name</label>
                    <div class="col-md-6">
                        : <label class="control-label">{!! ucwords($order->first_name) !!} {!! ucwords($order->last_name) !!}</label>

                    </div>
                </div>
                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Order No</label>
                    <div class="col-md-6">
                        : <label class="control-label">{!! $order->order_name !!}</label>
                    </div>
                </div>
                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Order Date</label>
                    <div class="col-md-6">
                        : <label class="control-label">{!! $order->created_at !!}</label>
                    </div>
                </div>

                @if($order->status != '')
                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Return Date</label>
                    <div class="col-md-6">
                        : <label class="control-label">{!! $order->updated_at !!}</label>
                    </div>
                </div>
                @endif

                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Status</label>
                    <div class="col-md-9">
                        : <label class="control-label">
                            @if(Auth::user()->hasRole('admin'))
                                @if( $order->status != 'Order Transferred' && $order->status != 'Order Completed'  && $order->status != 'Cancelled'  )
                                    {!! Form::open(array('url'=>'changeStatusFromAdmin', 'class'=>'form-bordered', 'id'=>'update_form')); !!}
                                    <input type="hidden" name="order_id" value="{{$order->id}}" />
                                    <select name="status_change" id="status" class="input-sm">
                                        <option value="0">Status Pending</option>
                                        @foreach($statusLists as $statusL)
                                        <option value="{{$statusL->id}}" @if($order->status == $statusL->status ) selected @endif >{{$statusL->status}}</option>
                                        @endforeach
                                        <option value="4" @if($order->status == 'Order Completed' ) selected @endif >Order Completed</option>
                                    </select>

                            {!! Form::button('Update', array('class' => 'btn btn-primary btn-xs','id'=>'update_info','data-id'=>$order->id, 'data-firstname'=>$order->first_name, 'data-lastname'=>$order->last_name, 'data-patientid'=>$order->patient_id,'data-patientdbo'=>date('m-d-Y', $order->date_of_birth) ) ) !!}

<!--                                    <button type="submit" class="btn btn-primary btn-xs"> Update</button>-->
                                    {!! Form::close() !!}
                                @else
                                    {!! $order->status !!}
                                @endif

                            @else
                                @if( $order->status == '' )
                                <span class="btn-warning text-sm btn-xs">Pending</span>
                                @else
                                {!! $order->status !!}
                                @endif


                            @endif
                            </label>
                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Prescription</label>

                    <div class="col-md-3">
                        : <?php echo substr($order->prescription_filename, 15); ?>
                    </div>

                    <div class="col-md-3">
                        <a href="<?php echo Config::get('app.url'); ?>local/storage/{!! $order->prescription_filename !!}" class="btn-primary btn-xs"> Download </a>
                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Note File</label>
                    <div class="col-md-3">
                        : <?php echo substr($order->note_filename, 15); ?>
                    </div>

                    <div class="col-md-3">
                           <a href="<?php echo Config::get('app.url'); ?>local/storage/{!! $order->note_filename !!}" class="btn-primary btn-xs"> Download </a>
                    </div>

                </div>


                @if($order->additional_filename1)
                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Additional file 1</label>
                    <div class="col-md-3">
                        : <?php echo substr($order->additional_filename1, 15); ?>
                    </div>

                    <div class="col-md-3">
                        <a href="<?php echo Config::get('app.url'); ?>local/storage/{!! $order->additional_filename1 !!}" class="btn-primary btn-xs"> Download </a>
                    </div>
                </div>
                @endif

                @if($order->additional_filename2)
                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Additional file 2</label>
                    <div class="col-md-3">
                        : <?php echo substr($order->additional_filename2, 15); ?>
                    </div>

                    <div class="col-md-3">
                        <a href="<?php echo Config::get('app.url'); ?>storage/{!! $order->additional_filename2 !!}" class="btn-primary btn-xs"> Download </a>
                    </div>
                </div>
                @endif

                @if($order->additional_filename3)
                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Additional file 3</label>
                    <div class="col-md-3">
                        : <?php echo substr($order->additional_filename3, 15); ?>
                    </div>

                    <div class="col-md-3">
                        <a href="<?php echo Config::get('app.url'); ?>local/storage/{!! $order->additional_filename3 !!}" class="btn-primary btn-xs"> Download </a>
                    </div>
                </div>
                @endif

                @if($order->memo)
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputDefault">Memo</label>

                    @if(Auth::user()->hasRole('admin'))
                    <div class="col-md-3">
                        {!! Form::open(array('url'=>'changeMemoFromAdmin', 'class'=>'form-bordered')); !!}
                        <input type="hidden" name="order_id" value="{{$order->id}}" />
                        <input type="text" class="input-sm form-control" name="memo" value="{!! $order->memo !!}">
                    </div>
                    <div class="col-md-3">
                        <button type="submit" class="btn btn-primary btn-xs"> Update</button>
                    </div>
                    @else
                    <div class="col-md-6">
                        : {!! $order->memo !!}
                    </div>
                    @endif

                </div>
                @endif

<!--                <div class="form-group">-->
<!--                    <label class="col-md-3 control-label" for="inputNote">Note</label>-->
<!--                    <div class="col-md-6" id="inputNote">-->
<!--                        : {!! $order->notes !!}-->
<!--                    </div>-->
<!--                </div>-->

              @if(Auth::user()->hasRole('admin'))
                <div class="form-group">
                    <label class="col-md-3 control-label" for="notes">Notes:</label>
                    <div class="col-md-3">
                     {!! Form::open(array('url'=>'NotesUpdate', 'class'=>'form-bordered')); !!}
                        <input type="hidden" name="order_id" value="{{$order->id}}" />
                        <textarea class="form-control" name='notes' id="notes" >{!! $order->notes !!}</textarea>

                    </div>
                    <div class="col-md-3"><br/>
                        <button type="submit" class="btn btn-primary btn-xs"> Update</button>
                    </div>
                    {!! Form::close() !!}
                </div>
              @else
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputNote">Note</label>
                    <div class="col-md-6" id="inputNote">
                        : {!! $order->notes !!}
                    </div>
                </div>
             @endif


            </div>
        </section>
        @endforeach
    </div>


    <!-- Transfer details -->
 @if($order->sid == 11)
    <div class="col-xs-12">
        @foreach($orderDetails as $order)
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Transfer Details</h2>
            </header>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputDefault">Pharmacy Name</label>
                    <div class="col-md-6">
                        : <label class="control-label">{!! $order->pharmacy_name !!} </label>

                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Pharmacy Phone</label>
                    <div class="col-md-6">
                        : <label class="control-label">
                            {!! $order->pharmacy_phone !!}
                        </label>
                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Note</label>
                    <div class="col-md-6">
                        : <label class="control-label">
                            {!! $order->note !!}
                        </label>
                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Transfer Date</label>
                    <div class="col-md-6">
                        : <label class="control-label">
                            {!! $order->transfer_date !!}
                        </label>
                    </div>
                </div>


            </div>
        </section>
        @endforeach
    </div>
@endif



    <!-- Patient details -->

    <div class="col-xs-12">
        @foreach($orderDetails as $order)
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Patient Details</h2>
            </header>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputDefault">Patient Name</label>
                    <div class="col-md-6">
                        : <label class="control-label">{!! ucwords($order->first_name) !!} {!! ucwords($order->last_name) !!}</label>

                    </div>
                </div>

                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Date of Birth</label>
                    <div class="col-md-6">
                        : <label class="control-label">
                            {!! date('d M, Y', $order->date_of_birth) !!}
                        </label>
                    </div>
                </div>


            </div>
        </section>
        @endforeach
        </div>

    <!-- Doctor details -->

    <div class="col-xs-12">
        @foreach($orderDetails as $order)
        <section class="panel">
            <header class="panel-heading">
                <div class="panel-actions">
                    <a href="#" class="panel-action panel-action-toggle" data-panel-toggle></a>
                </div>

                <h2 class="panel-title">Doctor Details</h2>
            </header>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputDefault">Doctor's Full Name</label>
                    <div class="col-md-6">
                        : <label class="control-label">{!! $order->d_fname !!} {!! $order->d_lname !!}</label>

                    </div>
                </div>




            </div>
        </section>
        @endforeach
    </div>

<!--    -->
    <div class="form-group  ">
        <label class="col-md-3 control-label" for="inputDefault"></label>

        <a href="{{ URL::previous() }}" class="mb-xs mt-xs mr-xs btn btn-warning saveco"> Back</a>
        @if(!Auth::user()->hasRole('admin'))
            @if( $order->status == 'Call to Patient insurance info needed' || $order->status == 'PA Info Request'  || $order->status == '' )
            <a href="{!!url()!!}/edit_order/{!! $order->id !!}" class="mb-xs mt-xs mr-xs btn btn-danger"> Edit Order</a>
            @endif
        @endif
    </div>
</div>
<!-- end: page -->
<!-- Button trigger modal -->

<div class="modal" id="myModal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center">Order Transfer Process</h4>
            </div>
            <div class="modal-body">

                {!! Form::open(array('url'=>'ordertransfer', 'class'=>'form-bordered', 'id'=>'order_transfer_form')); !!}
                <input type="hidden" id="t_order_id" name="t_order_id" />
                <input type="hidden" id="t_patient_id" name="t_patient_id" />
                <div class="form-group">
                    <label for="">Patient Name</label>
                    <input type="text" readonly disabled class="form-control" id="t_patient_name" name="t_patient_name">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Patient DOB</label>
                    <input type="text" readonly disabled class="form-control" id="t_patient_dob"name="t_patient_dob">
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Pharmacy Name</label>
                    <input type="text" class="form-control" id="t_pharmacy_name" name="t_pharmacy_name">
                </div>
                <div class="form-group">
                    <label for="">Pharmacy Phone</label>
                    <input type="tel" maxlength="15" class="form-control" id="t_phone" name="t_phone">
                </div>
                <div class="form-group">
                    <label for="">Date of Transfer</label>
                    <input type="date" class="form-control" id="t_date" name="t_date" disabled readonly value="{{date('m-d-Y')}}" >
                </div>
                <div class="form-group">
                    <label for="exampleInputPassword1">Note</label>
                    <textarea name="t_note" id="t_note" class="form-control" rows="5"></textarea>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Save changes</button>
                {!! Form::close() !!}
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- end: page -->
@stop
@section('javascript')
$('#update_info').click(function(){
var status = $("#status").val();
if(status == 4){
var conf = confirm('Are you sure the order is complete?');
if(conf == true){
$('#update_form').submit();
}
else{
return false;
}
}
if(status == 11){
var conf = confirm('Are you sure to transfer this order ?');
if(conf == true){
var dataFirstName = $(this).attr('data-firstname');
var dataLastName = $(this).attr('data-lastname');
var dataPatientdbo = $(this).attr('data-patientdbo');
var dataOrderID = $(this).attr('data-id');
var dataPatientID = $(this).attr('data-patientid');

$('#t_patient_name').val(dataFirstName+' '+dataLastName);
$('#t_patient_id').val(dataPatientID);
$('#t_patient_dob').val(dataPatientdbo);
$('#t_order_id').val(dataOrderID);

$('#myModal').modal('show');
}
}
if(status != 4 && status != 11){
$('#update_form').submit();
}

});


$("#order_transfer_form").validate({
highlight: function( label ) {
$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
},
success: function( label ) {
$(label).closest('.form-group').removeClass('has-error');
label.remove();
},
rules: {
t_pharmacy_name: {
required: true
},
t_phone:  {
required: true
}

},
messages:{
t_pharmacy_name: {
required:"Pharmacy name required."
},
t_phone: {
required:"Please enter the Phone."
}

}
});

@stop