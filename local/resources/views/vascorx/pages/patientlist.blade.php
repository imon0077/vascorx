@extends('vascorx.layout.master')

@section('content')
<header class="page-header">
    <h2>Patient list</h2>

</header>
@include('flash::message')
@include('vascorx.common.error-message')
<!-- start: page -->
<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-transparent">
            <div class="panel-body">
                <section class="panel panel-group">
                    <div id="accordion">
                        <div class="panel panel-accordion panel-accordion-first">

                            <div id="collapse1One" class="accordion-body collapse in">

                                <!-- -->
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped mb-none" id="datatable-default" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
                                        <thead>
                                        <tr>
                                            <th>Patient ID</th>
                                            <th>First name</th>
                                            <th>Last name</th>
                                            <th>Date of birth</th>
                                            <th>Created time</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($patientLists as $patientLists)

                                        <tr class="gradeX">
                                            <td>{!! $patientLists->id !!}</td>
                                            <td>{!! ucwords($patientLists->first_name) !!} </td>
                                            <td>{!! ucwords($patientLists->last_name) !!}</td>
                                            <td>{!! date('m-d-Y', $patientLists->date_of_birth) !!}</td>
                                            <td class="hidden-phone">{!! $patientLists->created_at !!}</td>
                                            <td class="actions">
                                                <a href="edit_patient/{!! $patientLists->id !!}" class="on-default btn edit-row" title="Edit"><i class="fa fa-pencil"></i></a>
                                                <button  data-href="del_patient/{!! $patientLists->id !!}" class="btn patientDelete" title="Delete"><i class="fa fa-trash-o"></i></button>
                                            </td>
                                        </tr>

                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </section>
    </div>
</div>

<!-- Modal Bootstrap -->


<div class="modal fade" id="modalDelete" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title text-warning text-center" id="myModalLabel">Confirm Patient Delete</h4>
            </div>
            <div class="modal-body">
                <p>Are you really trying to Delete?</p>
            </div>
            <div class="modal-footer">
                <a href="" class="btn btn-warning successDelete">Confirm</a>
                <button type="button" class="btn btn-default closeCancell" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<!-- end: page -->
@stop

@section('javascript')

$('.showField').click(function(){
$('.field').toggle().removeClass('hide');
$('.patientSelect').toggleClass('hide');
$(this).html($(this).html() == 'Create New Patient' ? 'Cancel' : 'Create New Patient');
})


$('.showFile').click(function(){
$('.create-order').toggle().removeClass('hide');
$('.orderSelect').toggleClass('hide');
$(this).html($(this).html() == 'Create New Order' ? 'Cancel' : 'Create New Order');
})


$('.patientDelete').click(function(){
var dataHref = $(this).attr('data-href');
$('.successDelete').attr('href',dataHref);
$('#modalDelete').modal('show');

});
@stop