@extends('vascorx.layout.master')

@section('content')
<header class="page-header">
    <h2>Create New Order</h2>
</header>

@include('flash::message')
@include('vascorx.common.error-message')

<!-- start: page -->
<div class="row">
<div class="col-xs-12">
<section class="panel">

<div class="panel-body">

    {!! Form::open(['url' => 'update-order', 'files' => 'true', 'class' => 'form-horizontal form-bordered']) !!}

        <input type="hidden" name='doctor_id' value='{!! Auth::user()->getkey() !!}'>
        <input type="hidden" name='order_id' value='{!! $orderLists->id !!}'>

        <div class="form-group field">
            <label class="col-md-3 control-label" for="inputDefault">Order ID</label>
            <div class="col-md-6">
                <input class="form-control" disabled  value="{!!$orderLists->id!!}" type="text">

            </div>
        </div>
        <div class="form-group field">
            <label class="col-md-3 control-label" for="inputDefault">Order Name</label>
            <div class="col-md-6">
                <input class="form-control" disabled value="{!!$orderLists->order_name!!}" type="text">

            </div>
        </div>


        @unless(empty($patientLists))
        <div class="form-group patientSelect">
            <label class="col-md-3 control-label">Select Patient</label>
            <div class="col-md-6">
                <select data-plugin-selectTwo class="form-control populate" name='patient_id'>
                    <optgroup label="Patients">
                        <option value="">Select..</option>
                        @foreach($patientLists as $patientLists)
                        <option value="{{ $patientLists->id }}"> {{ $patientLists->first_name }} {{ $patientLists->last_name }} {!! date('m-d-Y', $patientLists->date_of_birth) !!}</option>
                        @endforeach
                    </optgroup>
                </select>
            </div>
        </div>
        @endunless


        <div class="form-group hide field">
            <label class="col-md-3 control-label" for="inputDefault">First Name</label>
            <div class="col-md-6">
                <input class="form-control" name='first_name' id="inputDefault" type="text">
            </div>
        </div>

        <div class="form-group hide field">
            <label class="col-md-3 control-label" for="inputDefault">Last Name</label>
            <div class="col-md-6">
                <input class="form-control" name='last_name' id="inputDefault" type="text">
            </div>
        </div>

        <div class="form-group hide field">
            <label class="col-md-3 control-label">Date of Birth</label>
            <div class="col-sm-8">
                <div class="row">
                    <div class="col-sm-4">
                        <input type="text" placeholder="MM" class="form-control" name="mm">
                    </div>
                    <div class="visible-xs mb-md"></div>
                    <div class="col-sm-4">
                        <input type="text" placeholder="DD" class="form-control" name="dd">
                    </div>
                    <div class="visible-xs mb-md"></div>
                    <div class="col-sm-4">
                        <input type="text" placeholder="YYYY" class="form-control" name="yyyy">
                    </div>
                </div>

            </div>
        </div>
        <div class="form-group">
            <label class="col-md-3 control-label">&nbsp;</label>
            <div class="col-md-6">
                <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary">Save Order</button>
                <button type="button" class="mb-xs mt-xs mr-xs btn btn-warning showField">Create New Patient</button>
            </div>
         </div> 

     {!! Form::close() !!}
</section>
</div>
</div>

<!-- end: page -->
@stop

@section('javascript')

    $('.showField').click(function(){
        $('.field').toggle().removeClass('hide');
        $('.patientSelect').toggleClass('hide');
        $(this).html($(this).html() == 'Create New Patient' ? 'Cancel' : 'Create New Patient');
    })

@stop