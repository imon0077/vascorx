@extends('vascorx.layout.master')

@section('content')
<header class="page-header">
    <h2>Edit Doctor</h2>
</header>

@include('flash::message')
@include('vascorx.common.error-message')

<!-- start: page -->
<div class="row">
<div class="col-xs-12">
<section class="panel">

<div class="panel-body">

    {!! Form::open(['url' => 'editdoctorsave', 'class' => 'form-horizontal form-bordered','id'=>'editDoctorProfileSave']) !!}
    <input class="form-control" value="{!! $profiledetails->user_id !!}" name='id' id="inputDefault" type="hidden">
    <div class="form-group field">
        <label class="col-md-2 control-label" for="inputDefault">First Name</label>
        <div class="col-md-3">
            <input class="form-control" value="{!! $profiledetails->first_name !!}" name='first_name' id="" type="text">
        </div>

        <label class="col-md-3 control-label" for="inputDefault">Last Name</label>
        <div class="col-md-3">
            <input class="form-control" value="{!! $profiledetails->last_name !!}" name='last_name' id="" type="text">
        </div>
    </div>

    <div class="form-group field">

        <label class="col-md-2 control-label" for="inputDefault">Contact Name</label>
        <div class="col-md-3">
            <input class="form-control" value="{!! $profiledetails->contact_name !!}" name='contact_name' id="" type="text">
        </div>

        <label class="col-md-3 control-label" for="inputDefault">Phone Number</label>
        <div class="col-md-3">
            <input class="form-control" value="{!! $profiledetails->phone_number !!}" name='phn_number' id="" type="text">
        </div>
    </div>

    <div class="form-group field">
        <label class="col-md-2 control-label" for="inputDefault">Email Address</label>
        <div class="col-md-3">
            <input class="form-control" value="{!! $profiledetails->email !!}" name='email' id="" type="email">
        </div>

        <label class="col-md-3 control-label" for="inputDefault">FAX Number</label>
        <div class="col-md-3">
            <input class="form-control" value="{!! $profiledetails->fax_number !!}" name='fax_number' id="" type="text">
        </div>

    </div>

    <div class="form-group field">
        <label class="col-md-2 control-label" for="inputDefault">NPI Number</label>
        <div class="col-md-3">
            <input class="form-control" value="{!! $profiledetails->npi_number !!}" name='npi_number' id="npi_number" type="text">
        </div>

        <label class="col-md-3 control-label" for="inputDefault">DEA Number</label>
        <div class="col-md-3">
            <input class="form-control" value="{!! $profiledetails->dea_number !!}" name='dea_number' id="" type="text">
        </div>
    </div>


    <div class="form-group">
        <label class="col-md-3 control-label">&nbsp;</label>
        <div class="col-md-6">
            <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary">Save Changes</button>

        </div>
    </div>

    <!--  end  -->
     {!! Form::close() !!}
</section>
</div>
</div>

<!-- end: page -->
@stop

@section('javascript')
$(document).ready(function(){


$("#editDoctorProfileSave").validate({
highlight: function( label ) {
$(label).closest('.form-group').removeClass('has-success').addClass('has-error');
},
success: function( label ) {
$(label).closest('.form-group').removeClass('has-error');
label.remove();
},
rules: {
first_name: "required",
last_name: "required",
contact_name: {
required: true
},
phn_number: {
required: true
},
email: {
required: true,
email: true
},
npi_number: {

rangelength: [10, 20]
},
dea_number: {

rangelength: [1, 9]
}
},
messages:{
first_name: {
required:"First Name field could not remain blank"
},
last_name: {
required:"Last Name field could not remain blank"
},
contact_name: {
required:"Contact Name field could not remain blank"
},
email: {
required:"The email is required",
email:"Email does not seems to valid"
},
npi_number: {
rangelength:"The NPI Number should between 10 and 20 characters long."
},
dea_number: {
rangelength:"The DEA Number should between 1 and 9 characters long."
}
}
});

$('input[name="first_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});
$('input[name="last_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});

$('input[name="contact_name"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^a-z,A-Z,.\ ]/g,'') );
});
$('input[name="fax_number"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^0-9,.\ \-\+]/g,'') );
});
$('input[name="phn_number"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^0-9,.\ \-\+]/g,'') );
});
$('input[name="npi_number"]').bind('keyup blur', function(){
$(this).val( $(this).val().replace(/[^0-9,.\ \-\+]/g,'') );
});


});
@stop
