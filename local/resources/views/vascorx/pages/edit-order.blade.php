@extends('vascorx.layout.master')

@section('content')
<header class="page-header" xmlns="http://www.w3.org/1999/html">
    <h2>Edit Order: {!! $orders->order_name !!}</h2>
</header>

@include('flash::message')
@include('vascorx.common.error-message')

<!-- start: page -->
<div class="row">
    <div class="col-xs-12">
        <section class="panel">

            <div class="panel-body">

                {!! Form::open(['url' => 'edit_order_save', 'files' => 'true', 'class' => 'form-horizontal form-bordered']) !!}

                <input type="hidden" name='doctor_id' value='{!! Auth::user()->getkey() !!}'>
                <input type="hidden" name='id' value='{!! $orders->id !!}'>
                <input type="hidden" name='pid' value='{!! $orders->pid !!}'>
                <input type="hidden" name='sid' value='{!! $orders->status !!}'>
                <input type="hidden" name='order_number' value='{!! $orders->order_name !!}'>


                <div class="form-group file">
                    <label class="col-md-3 control-label">Prescription Doc <br/>(keep it blank if no changes)</label>
                    <div class="col-md-5">
                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                            <div class="input-append">
                                <div class="uneditable-input">
                                    <i class="fa fa-file fileupload-exists"></i>
                                    <span class="fileupload-preview"></span>
                                </div>
                        <span class="btn btn-default btn-file">
                            <span class="fileupload-exists">Change</span>
                            <span class="fileupload-new">Select file</span>
                            <input type="file" name="prescription_filename">
                        </span>
                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                            </div>
                        </div>
                    </div>


                    <div class="col-md-1 control-label">
                        <a href="<?php echo Config::get('app.url'); ?>local/storage/{!! $orders->prescription_filename !!}" class="btn-primary btn-xs"> Download </a>
                    </div>

                </div>

               <div class="form-group file">
                    <label class="col-md-3 control-label">Notes Doc<br/>(keep it blank if no changes)</label>
                     <div class="col-md-5">
                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                            <div class="input-append">
                                <div class="uneditable-input">
                                    <i class="fa fa-file fileupload-exists"></i>
                                    <span class="fileupload-preview"></span>
                                </div>
                        <span class="btn btn-default btn-file">
                            <span class="fileupload-exists">Change</span>
                            <span class="fileupload-new">Select file</span>
                            <input type="file" name='note_filename'>
                        </span>
                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                            </div>
                        </div>
                    </div>

                    <div class="col-md-1 control-label">
                        <a href="<?php echo Config::get('app.url'); ?>local/storage/{!! $orders->note_filename !!}" class="btn-primary btn-xs"> Download </a>
                    </div>

                </div>

<!---------------------------upload additional files   -->
                <div class="form-group hide field file">
                    <label class="col-md-3 control-label">Additional file 1</label>
                    <div class="col-md-5">
                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                            <div class="input-append">
                                <div class="uneditable-input">
                                    <i class="fa fa-file fileupload-exists"></i>
                                    <span class="fileupload-preview"></span>
                                </div>
                        <span class="btn btn-default btn-file">
                            <span class="fileupload-exists">Change</span>
                            <span class="fileupload-new">Select file</span>
                            <input type="file" name='additional_file1'>
                        </span>
                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                            </div>
                        </div>
                    </div>

                    @if($orders->additional_filename1)
                    <div class="col-md-1">
                        <a href="<?php echo Config::get('app.url'); ?>local/storage/{!! $orders->additional_filename1 !!}" class="btn-primary btn-xs"> Download </a>
                    </div>
                    @endif

                </div>

                <div class="form-group hide field file">
                    <label class="col-md-3 control-label">Additional file 2</label>
                    <div class="col-md-5">
                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                            <div class="input-append">
                                <div class="uneditable-input">
                                    <i class="fa fa-file fileupload-exists"></i>
                                    <span class="fileupload-preview"></span>
                                </div>
                        <span class="btn btn-default btn-file">
                            <span class="fileupload-exists">Change</span>
                            <span class="fileupload-new">Select file</span>
                            <input type="file" name='additional_file2'>
                        </span>
                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                            </div>
                        </div>
                    </div>

                    @if($orders->additional_filename2)
                    <div class="col-md-1">
                        <a href="<?php echo Config::get('app.url'); ?>local/storage/{!! $orders->additional_filename2 !!}" class="btn-primary btn-xs"> Download </a>
                    </div>
                    @endif

                </div>

                <div class="form-group hide field file">
                    <label class="col-md-3 control-label">Additional file 3</label>
                    <div class="col-md-5">
                        <div class="fileupload fileupload-new" data-provides="fileupload"><input type="hidden">
                            <div class="input-append">
                                <div class="uneditable-input">
                                    <i class="fa fa-file fileupload-exists"></i>
                                    <span class="fileupload-preview"></span>
                                </div>
                        <span class="btn btn-default btn-file">
                            <span class="fileupload-exists">Change</span>
                            <span class="fileupload-new">Select file</span>
                            <input type="file" name='additional_file3'>
                        </span>
                                <a href="#" class="btn btn-default fileupload-exists" data-dismiss="fileupload">Remove</a>
                            </div>
                        </div>
                    </div>
                    @if($orders->additional_filename3)
                    <div class="col-md-1">
                        <a href="<?php echo Config::get('app.url'); ?>local/storage/{!! $orders->additional_filename3 !!}" class="btn-primary btn-xs"> Download </a>
                    </div>
                    @endif

                </div>

<!--                <div class="form-group">-->
<!--                    <label class="col-md-3 control-label" for="notes">Notes:</label>-->
<!--                    <div class="col-md-3">-->
<!--                        <textarea class="form-control" name='notes' id="notes" @if($orders->status == 6)disabled='disabled' @endif >{!! $orders->notes !!}</textarea>-->
<!--                    </div>-->
<!--                </div>-->

<!-- ------------------ /upload additional files  -->


                <div class="form-group">
                    <label class="col-md-3 control-label" for="inputDefault">First Name</label>
                    <div class="col-md-3">
                        <input class="form-control" name='first_name' value="{!! $orders->first_name !!}" id="inputDefault" type="text" @if($orders->status == 6)disabled='disabled' @endif>
                    </div>
                    <label class="col-md-2 control-label" for="inputDefault">Last Name</label>
                    <div class="col-md-3">
                        <input class="form-control" name='last_name' value="{!! $orders->last_name !!}" id="inputDefault" type="text" @if($orders->status == 6)disabled='disabled' @endif>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-3 control-label">Date of Birth</label>
                    <div class="col-sm-8">
                        <div class="row">
                            <div class="col-sm-4">
                                <input type="text" name="mm" value="{!! date('m', $orders->date_of_birth) !!}" class="form-control" placeholder="MM" @if($orders->status == 6)disabled='disabled' @endif>
                            </div>
                            <div class="visible-xs mb-md"></div>
                            <div class="col-sm-4">
                                <input type="text" name="dd" value="{!! date('d', $orders->date_of_birth) !!}" class="form-control" placeholder="DD" @if($orders->status == 6)disabled='disabled' @endif>
                            </div>
                            <div class="visible-xs mb-md"></div>
                            <div class="col-sm-4">
                                <input type="text" name="yyyy" value="{!! date('Y', $orders->date_of_birth) !!}" class="form-control" placeholder="YYYY" @if($orders->status == 6)disabled='disabled' @endif>
                            </div>
                        </div>

                    </div>
                </div>


                <!--
                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Order No</label>
                    <div class="col-md-6">
                        : <label class="control-label">{!! $orders->order_name !!}</label>
                    </div>
                </div>
                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Order Date</label>
                    <div class="col-md-6">
                        : <label class="control-label">{!! $orders->created_at !!}</label>
                    </div>
                </div>

                @if($orders->status != '')
                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Return Date</label>
                    <div class="col-md-6">
                        : <label class="control-label">{!! $orders->updated_at !!}</label>
                    </div>
                </div>
                @endif

                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Status</label>
                    <div class="col-md-6">
                        : <label class="control-label">
                            @if( $orders->status == '' )
                            <span class="btn-warning text-sm btn-xs">Pending</span>
                            @else
                            {!! $orders->status !!}
                            @endif
                        </label>
                    </div>
                </div>
                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Prescription</label>
                    <div class="col-md-6">
                        : <a href="http://localhost/vasco/storage/{!! $orders->prescription_filename !!}" class="btn-primary btn-xs"> Download </a>
                    </div>
                </div>
                <div class="form-group  ">
                    <label class="col-md-3 control-label" for="inputDefault">Note File</label>
                    <div class="col-md-6">
                        : <a href="http://localhost/vasco/storage/{!! $orders->note_filename !!}" class="btn-primary btn-xs"> Download </a>
                    </div>
                </div>
                -->


                <div class="form-group">
                    <label class="col-md-3 control-label">&nbsp;</label>
                    <div class="col-md-6">
                        <button type="submit" class="mb-xs mt-xs mr-xs btn btn-primary">Edit Order</button>
                        @if($orders->status != 4 AND $orders->status != 11 AND $orders->status != 12)
                        <button type="button" class="mb-xs mt-xs mr-xs btn btn-warning showField">Upload Additional files</button>
                        @endif

                    </div>
                </div>

                {!! Form::close() !!}
        </section>
    </div>
</div>

<!-- end: page -->
@stop

@section('javascript')

$('.showField').click(function(){
$('.field').toggle().removeClass('hide');
$('.patientSelect').toggleClass('hide');
$(this).html($(this).html() == 'Upload Additional files' ? 'Cancel' : 'Upload Additional files');
})

@stop