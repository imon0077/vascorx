@extends('vascorx.layout.master')

@section('content')
<header class="page-header">
    <h2>New Doctors Signup request</h2>

</header>

@include('flash::message')
@include('vascorx.common.error-message')

<!-- start: page -->
<div class="row">
    <div class="col-lg-12">
        <section class="panel panel-transparent">
            <div class="panel-body">
                <section class="panel panel-group">
                    <div id="accordion">
                        <div class="panel panel-accordion panel-accordion-first">

                            <div id="collapse1One" class="accordion-body collapse in">

                                <!-- -->
                                <div class="panel-body">
                                    <table class="table table-bordered table-striped mb-none" id="datatable-default" data-swf-path="assets/vendor/jquery-datatables/extras/TableTools/swf/copy_csv_xls_pdf.swf">
                                        <thead>
                                        <tr>
                                            <th>ID</th>
                                            <th>User name</th>
                                            <th>Full name</th>
                                            <th>Created Date</th>
                                            <!--<th>Return Date</th>-->
                                            <th>Status</th>
                                            <th>Actions</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($newDoctors as $Doctors)
                                        {!! Form::open(array('action'=>'AdminsController@newDoctors', 'class'=>'form-bordered')); !!}
                                        <tr class="gradeX">
                                            <td>{!! $Doctors->id !!}</td>
                                            <td>{!! $Doctors->username !!} </td>
                                            <td>{!! $Doctors->first_name !!} {!! $Doctors->last_name !!}</td>
                                            <td>{!! $Doctors->created_at !!}</td>
                                           <!-- <td>{!! $Doctors->updated_at !!}</td>-->
                                            <td class="center hidden-phone">
                                                <span class="btn-xs @if($Doctors->status == 0) btn-warning @elseif($Doctors->status == 1) btn-success @else btn-danger @endif">
                                                    @if($Doctors->status == 0)Pending @elseif($Doctors->status == 1) Accepted @else Declined @endif
                                                </span>
                                            </td>

                                            <td class="actions">
                                                @if($Doctors->status == 0)
                                                <a href="accept/{!! $Doctors->id !!}" title="Accept">
                                                    <span class="btn-xs btn btn-success">
                                                    <i class="fa fa-check"></i> Accept
                                                    </span>
                                                </a>
                                                <a href="decline/{!! $Doctors->id !!}" class="on-default remove-row" title="Decline">
                                                    <span class="btn-xs btn btn-danger">
                                                    <i class="fa fa-close"></i> Decline
                                                    </span>
                                                </a>
                                                @elseif($Doctors->status == 1)
                                                <a href="decline/{!! $Doctors->id !!}" class="on-default remove-row" title="Decline">
                                                    <span class="btn-xs btn btn-danger">
                                                    <i class="fa fa-close"></i> Decline
                                                    </span>
                                                </a>
                                                <!--<a href="decline/{!! $Doctors->id !!}" class="on-default remove-row" title="Decline"><i class="fa fa-close"></i></a>-->

                                                @else
                                                <a href="accept/{!! $Doctors->id !!}" title="Accept">
                                                    <span class="btn-xs btn btn-success">
                                                    <i class="fa fa-check"></i> Accept
                                                    </span>
                                                </a>
                                                <!--<a href="accept/{!! $Doctors->id !!}" title="Accept"><i class="fa fa-check"></i></a>-->

                                                @endif

                                            </td>
                                        </tr>
                                        {!! Form::close() !!}
                                        @endforeach
                                        </tbody>
                                    </table>
                                </div>
                                <!-- -->

                            </div>
                        </div>
                    </div>
                </section>

            </div>
        </section>
    </div>
</div>
<!-- end: page -->
@stop

@section('javascript')

$('.showField').click(function(){
$('.field').toggle().removeClass('hide');
$('.patientSelect').toggleClass('hide');
$(this).html($(this).html() == 'Create New Patient' ? 'Cancel' : 'Create New Patient');
})


$('.showFile').click(function(){
$('.create-order').toggle().removeClass('hide');
$('.orderSelect').toggleClass('hide');
$(this).html($(this).html() == 'Create New Order' ? 'Cancel' : 'Create New Order');
})

@stop