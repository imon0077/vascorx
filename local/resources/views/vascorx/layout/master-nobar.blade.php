<!doctype html>
<html class="fixed">
@include('vascorx.common.head')
<body>

    @yield('content')

    @include('vascorx.common.js')

	<script type="text/javascript">
	$(document).ready(function(){
   		@yield('javascript')
   	})
   	</script>
</body>
</html>