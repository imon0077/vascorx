<!doctype html>
<html class="fixed">
@include('vascorx.common.head')
<body>
<section class="body">

<!-- start: header -->
@include('vascorx.common.header')
<!-- end: header -->

<div class="inner-wrapper">
<!-- start: sidebar -->
    @include('vascorx.common.sidebar')
<!-- end: sidebar -->

<section role="main" class="content-body">

    @yield('content')

</section>
</div>
    @include('vascorx.common.right-sidebar')
</section>

@include('vascorx.common.js')

<script type="text/javascript">
$(document).ready(function(){
		@yield('javascript')
})
</script>

</body>
</html>