function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for(var i=0;i < ca.length;i++) {
        var c = ca[i];
        while (c.charAt(0)==' ') c = c.substring(1,c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
    }
    return null;
}

$('.sidebar-toggle').click(function(){
    var me = readCookie('currentView');
    document.cookie = (me == 'max' || me == null) ? 'currentView=min': 'currentView=max';
    //console.log(me);
});

$(document).ready(function(){
    var me = readCookie('currentView');
    if(me == 'min'){
        document.cookie = 'currentView=max';
        $('.sidebar-toggle').click();
    }
});